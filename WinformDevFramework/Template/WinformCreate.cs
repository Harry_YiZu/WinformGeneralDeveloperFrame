﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Commons.Collections;
using NVelocity;
using NVelocity.App;
using NVelocity.Runtime;

namespace WinformDevFramework
{
    public class WinformCreate
    {
        private static readonly string templateDir = AppDomain.CurrentDomain.BaseDirectory + @"\Template\";
        public bool Create(string tableName, string NameSpace, string outdir, List<ControlInfo> EntityList)
        {

            VelocityEngine velocityEngine = new VelocityEngine();
            ExtendedProperties props = new ExtendedProperties();
            props.AddProperty(RuntimeConstants.RESOURCE_LOADER, @"file");
            props.AddProperty(RuntimeConstants.FILE_RESOURCE_LOADER_PATH, templateDir);
            props.AddProperty(RuntimeConstants.INPUT_ENCODING, "utf-8");
            props.AddProperty(RuntimeConstants.OUTPUT_ENCODING, "utf-8");
            //模板的缓存设置
            props.AddProperty(RuntimeConstants.FILE_RESOURCE_LOADER_CACHE, true);              //是否缓存
            props.AddProperty("file.resource.loader.modificationCheckInterval", (Int64)30);    //缓存时间(秒)
            velocityEngine.Init(props);
            //为模板变量赋值
            VelocityContext context = new VelocityContext();
            context.Put("tableName", tableName);
            context.Put("NameSpace", NameSpace);
            context.Put("outdir", outdir);
            context.Put("EntityList", EntityList);
            //从文件中读取模板
            NVelocity.Template template = velocityEngine.GetTemplate(@"\Form.vm");
            NVelocity.Template template2 = velocityEngine.GetTemplate(@"\FormDesigner.vm");

            if (!Directory.Exists(outdir + "\\From"))
            {
                Directory.CreateDirectory(outdir + "\\From");
            }
            //合并模板
            using (StreamWriter writer = new StreamWriter(outdir + $"\\From\\Frm{tableName.Substring(0, 1).ToUpper()}{tableName.Substring(1)}.Designer.cs", false))
            {
                template2.Merge(context, writer);
            }
            using (StreamWriter writer = new StreamWriter(outdir + $"\\From\\Frm{tableName.Substring(0, 1).ToUpper()}{tableName.Substring(1)}.cs", false))
            {
                template.Merge(context, writer);
            }
            return true;
        }
    }
}
