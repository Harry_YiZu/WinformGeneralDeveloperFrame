using SqlSugar;
using WinformDevFramework.Core.Winform;
using WinformDevFramework.IServices;
using WinformDevFramework;
using WinformDevFramework.IServices.System;
using WinformDevFramework.Services;

namespace WinformDevFramework
{
    public partial class Frmw_OtherOutWarehouse : BaseForm1
    {
        private Iw_OtherOutWarehouseServices _w_OtherOutWarehouseServices;
        private Iw_OtherOutWarehouseDetailServices _w_OtherOutWarehouseDetailServices;
        private Iw_Warehouseervices _warehoursesServices;
        private IW_GoodsServices _wGoodsServices;
        private IsysDicDataServices _sysDicDataServices;
        private ISysUserServices _sysUserServices;
        public Frmw_OtherOutWarehouse(Iw_OtherOutWarehouseServices w_OtherOutWarehouseServices, Iw_OtherOutWarehouseDetailServices w_OtherOutWarehouseDetailServices, Iw_Warehouseervices warehoursesServices, IW_GoodsServices wGoodsServices, IsysDicDataServices sysDicDataServices,ISysUserServices sysUserServices)
        {
            _w_OtherOutWarehouseServices = w_OtherOutWarehouseServices;
            InitializeComponent();
            _w_OtherOutWarehouseDetailServices = w_OtherOutWarehouseDetailServices;
            _wGoodsServices = wGoodsServices;
            _warehoursesServices = warehoursesServices;
            _sysDicDataServices = sysDicDataServices;
            _sysUserServices = sysUserServices;
        }

        public override void Init()
        {
            base.Init();
            //查询数据
            this.dataGridViewList.DataSource = GetData();

            // 设置隐藏列
            this.dataGridViewList.Columns["TypeCode"].HeaderText = "业务类别";
            this.dataGridViewList.Columns["InvoicesDate"].HeaderText = "单据日期";
            this.dataGridViewList.Columns["InvoicesImage"].Visible = false;
            this.dataGridViewList.Columns["Remark"].HeaderText = "备注";
            this.dataGridViewList.Columns["Status"].Visible = false;
            this.dataGridViewList.Columns["MakeUserID"].Visible = false;
            this.dataGridViewList.Columns["OtherOutWarehouseCode"].HeaderText = "出库单号";

            //设置明细 仓库
            Warehouse.DataSource = _warehoursesServices.Query();
            Warehouse.DisplayMember = "WarehouseName";
            Warehouse.ValueMember = "WarehouseCode";

            GoodsName.DataSource = _wGoodsServices.Query();
            GoodsName.DisplayMember = "GoodsName";
            GoodsName.ValueMember = "GoodsCode";

            MakeUser.DataSource = _sysUserServices.Query();//值查找销售部门
            MakeUser.ValueMember = "ID";
            MakeUser.DisplayMember = "Fullname";

            TypeCode.DataSource = _sysDicDataServices.QueryListByClause(p => p.DicTypeID == 2003);
            TypeCode.DisplayMember = "DicData";
            TypeCode.ValueMember = "ID";
            SetToolButtonStatus(formStatus);
            SetToolsButton();
        }

        public override void TabControlSelectingFunction(object? sender, TabControlCancelEventArgs e)
        {
            base.TabControlSelectingFunction(sender, e);
        }

        public override void DataGridViewListDoubleClick(object? sender, DataGridViewCellEventArgs e)
        {
            base.DataGridViewListDoubleClick(sender, e);
            var g = (DataGridView)sender;
            var model = g.CurrentRow.DataBoundItem as w_OtherOutWarehouse;

            //给详情页设置数据
            this.txtID.Text = model.ID.ToString();
            this.TypeCode.SelectedValue = model.TypeCode.ToInt32();
            this.InvoicesDate.Text = model.InvoicesDate.ToString();
            this.InvoicesImage.Text = model.InvoicesImage;
            this.Remark.Text = model.Remark;
            this.Status.Text = model.Status;
            this.MakeUser.SelectedValue = model.MakeUserID;
            this.OtherOutWarehouseCode.Text = model.OtherOutWarehouseCode;
            SetDataToDetail(GetDetailData(OtherOutWarehouseCode.Text));
            SetToolButtonStatus(formStatus);
        }

        public override void AddFunction(object sender, EventArgs e)
        {
            base.AddFunction(sender, e);
            SetToolButtonStatus(formStatus);
            dataGridViewDetail.Rows.Clear();
            MakeUser.SelectedValue = AppInfo.User.ID;
            SetDataGridViewReadonlyStatus();
        }

        public override void EditFunction(object sender, EventArgs e)
        {
            base.EditFunction(sender, e);
            SetToolButtonStatus(formStatus);
            SetDataGridViewReadonlyStatus();

        }

        public override void SaveFunction(object sender, EventArgs e)
        {
            base.SaveFunction(sender, e);
            {
                //新增
                if (string.IsNullOrEmpty(txtID.Text))
                {
                    w_OtherOutWarehouse model = new w_OtherOutWarehouse();
                    // TODO获取界面的数据
                    model.TypeCode = this.TypeCode.SelectedValue.ToString();
                    model.InvoicesDate = this.InvoicesDate.Value.ToDateTime();
                    model.InvoicesImage = this.InvoicesImage.Text;
                    model.Remark = this.Remark.Text;
                    model.Status = this.Status.Text;
                    model.MakeUserID = this.MakeUser.SelectedValue.ToInt32();
                    if (string.IsNullOrEmpty(this.OtherOutWarehouseCode.Text))
                    {
                        model.OtherOutWarehouseCode = $"QTCK{DateTime.Now.ToString("yyyyMMddHHmmss")}";
                    }
                    else
                    {
                        model.OtherOutWarehouseCode = this.OtherOutWarehouseCode.Text;
                    }
                    #region 明细表

                    List<w_OtherOutWarehouseDetail> dataDetails = new List<w_OtherOutWarehouseDetail>();
                    foreach (DataGridViewRow row in dataGridViewDetail.Rows)
                    {
                        if (!row.IsNewRow)
                        {
                            w_OtherOutWarehouseDetail detail = new w_OtherOutWarehouseDetail();
                            detail.Number = row.Cells["Number"].FormattedValue.ToDecimal();
                            detail.GoodsName = row.Cells["GoodsCode"].FormattedValue.ToString();
                            detail.WarehouseCode = row.Cells["Warehouse"].Value.ToString();
                            detail.WarehouseName = row.Cells["Warehouse"].FormattedValue.ToString();
                            detail.GoodsCode = row.Cells["GoodsCode"].Value.ToString();
                            detail.GoodsSpec = row.Cells["GoodsSpec"].FormattedValue.ToString();
                            detail.GoodsUnit = row.Cells["GoodsUnit"].FormattedValue.ToString();
                            detail.Remark = row.Cells["RemarkD"].FormattedValue.ToString();
                            dataDetails.Add(detail);
                        }
                    }

                    #endregion
                    int id = _w_OtherOutWarehouseServices.AddOtherOutWarehouseInfo(model, dataDetails);
                    if (id > 0)
                    {
                        MessageBox.Show("保存成功！", "提示");
                        txtID.Text = id.ToString();
                        OtherOutWarehouseCode.Text = model.OtherOutWarehouseCode;
                        SetDataToDetail(GetDetailData(model.OtherOutWarehouseCode));
                    }
                }
                // 修改
                else
                {
                    w_OtherOutWarehouse model = _w_OtherOutWarehouseServices.QueryById(int.Parse(txtID.Text));
                    // TODO获取界面的数据
                    model.TypeCode = this.TypeCode.SelectedValue.ToString();
                    model.InvoicesDate = this.InvoicesDate.Value.ToDateTime();
                    model.InvoicesImage = this.InvoicesImage.Text;
                    model.Remark = this.Remark.Text;
                    model.Status = this.Status.Text;
                    model.MakeUserID = this.MakeUser.SelectedValue.ToInt32();
                    model.OtherOutWarehouseCode = this.OtherOutWarehouseCode.Text;
                    #region 明细表

                    List<w_OtherOutWarehouseDetail> dataDetails = new List<w_OtherOutWarehouseDetail>();
                    foreach (DataGridViewRow row in dataGridViewDetail.Rows)
                    {
                        if (!row.IsNewRow)
                        {
                            w_OtherOutWarehouseDetail detail = new w_OtherOutWarehouseDetail();
                            detail.Number = row.Cells["Number"].FormattedValue.ToDecimal();
                            detail.GoodsName = row.Cells["GoodsCode"].FormattedValue.ToString();
                            detail.GoodsCode = row.Cells["GoodsCode"].Value.ToString();
                            detail.GoodsSpec = row.Cells["GoodsSpec"].FormattedValue.ToString();
                            detail.GoodsUnit = row.Cells["GoodsUnit"].FormattedValue.ToString();
                            detail.WarehouseCode = row.Cells["Warehouse"].Value.ToString();
                            detail.WarehouseName = row.Cells["Warehouse"].FormattedValue.ToString();
                            detail.OtherOutWarehouseDetailCode = row.Cells["OtherOutWarehouseDetailCode"].FormattedValue.ToString();

                            detail.OtherOutWarehouseCode = row.Cells["OtherOutWarehouseCodeD"].FormattedValue.ToString();

                            detail.Remark = row.Cells["RemarkD"].FormattedValue.ToString();
                            dataDetails.Add(detail);
                        }
                    }

                    #endregion
                    if (_w_OtherOutWarehouseServices.UpdateOtherOutWarehouseInfo(model, dataDetails))
                    {
                        MessageBox.Show("保存成功！", "提示");
                        SetDataToDetail(GetDetailData(model.OtherOutWarehouseCode));
                    }
                }
                SetToolButtonStatus(formStatus);
                //列表重新赋值
                this.dataGridViewList.DataSource = GetData();
            }
        }

        public override void CanelFunction(object sender, EventArgs e)
        {
            bool isAdd = formStatus == FormStatus.Add;
            base.CanelFunction(sender, e);
            SetToolButtonStatus(formStatus);
            btnEdit.Enabled = !isAdd;
            btnDel.Enabled = !isAdd;
            SetDataToDetail(GetDetailData(OtherOutWarehouseCode.Text));
        }

        public override void DelFunction(object sender, EventArgs e)
        {
            base.DelFunction(sender, e);
            var result = MessageBox.Show("是否删除？", "确认", MessageBoxButtons.YesNoCancel);
            if (result == DialogResult.Yes)
            {
                if (_w_OtherOutWarehouseServices.DeleteOtherOutWarehouseInfo(new w_OtherOutWarehouse()
                {
                    ID = txtID.Text.ToInt32(),
                    OtherOutWarehouseCode = OtherOutWarehouseCode.Text
                }))
                {
                    formStatus = FormStatus.Del;
                    SetToolButtonStatus(formStatus);
                    //列表重新赋值
                    dataGridViewDetail.Rows.Clear();
                    this.dataGridViewList.DataSource = GetData();
                }

            }
        }

        public override void SetToolButtonStatus(FormStatus status)
        {
            base.SetToolButtonStatus(status);
            switch (status)
            {
                case FormStatus.Add:
                    {
                        btnAdd.Enabled = false;
                        btnEdit.Enabled = false;
                        btnSave.Enabled = true;
                        btnDel.Enabled = false;
                        btnCanel.Enabled = true;
                        dataGridViewDetail.ReadOnly = false;
                        SetControlStatus(this.groupBox1, true);
                        ClearControlsText(this.groupBox1);
                        break;
                    }
                case FormStatus.Edit:
                    {
                        btnAdd.Enabled = false;
                        btnEdit.Enabled = false;
                        btnSave.Enabled = true;
                        btnDel.Enabled = false;
                        btnCanel.Enabled = true;
                        dataGridViewDetail.ReadOnly = false;
                        SetControlStatus(this.groupBox1, true);
                        break;
                    }
                case FormStatus.View:
                    {
                        btnAdd.Enabled = true;
                        btnEdit.Enabled = true;
                        btnSave.Enabled = false;
                        btnDel.Enabled = true;
                        btnCanel.Enabled = false;
                        dataGridViewDetail.ReadOnly = true;
                        SetControlStatus(this.groupBox1, false);
                        break;
                    }
                case FormStatus.Canel:
                    {
                        btnAdd.Enabled = true;
                        btnEdit.Enabled = true;
                        btnSave.Enabled = false;
                        btnDel.Enabled = false;
                        btnCanel.Enabled = false;
                        dataGridViewDetail.ReadOnly = true;
                        SetControlStatus(this.groupBox1, false);
                        break;
                    }
                case FormStatus.First:
                    {
                        btnAdd.Enabled = true;
                        btnEdit.Enabled = false;
                        btnSave.Enabled = false;
                        btnDel.Enabled = false;
                        btnCanel.Enabled = false;
                        dataGridViewDetail.ReadOnly = true;
                        SetControlStatus(this.groupBox1, false);
                        break;
                    }
                case FormStatus.Save:
                    {
                        btnAdd.Enabled = true;
                        btnEdit.Enabled = true;
                        btnSave.Enabled = false;
                        btnDel.Enabled = true;
                        btnCanel.Enabled = false;
                        dataGridViewDetail.ReadOnly = true;
                        SetControlStatus(this.groupBox1, false);
                        break;
                    }
                case FormStatus.Del:
                    {
                        btnAdd.Enabled = true;
                        btnEdit.Enabled = false;
                        btnSave.Enabled = false;
                        btnDel.Enabled = false;
                        btnCanel.Enabled = false;
                        dataGridViewDetail.ReadOnly = true;
                        SetControlStatus(this.groupBox1, false);
                        ClearControlsText(this.groupBox1);
                        break;
                    }
            }
        }

        public override bool ValidateData()
        {
            return true;
        }
        /// <summary>
        /// 获取数据
        /// </summary>
        /// <returns></returns>
        private List<w_OtherOutWarehouse> GetData()
        {
            List<w_OtherOutWarehouse> data = new List<w_OtherOutWarehouse>();
            data = _w_OtherOutWarehouseServices.Query();
            return data;
        }
        /// <summary>
        /// 明细表 设置数据 --主表带出
        /// </summary>
        /// <param name="details"></param>
        private void SetDataToDetail(List<w_OtherOutWarehouseDetail> details)
        {
            dataGridViewDetail.Rows.Clear();
            details.ForEach(p =>
            {
                dataGridViewDetail.Rows.AddCopy(dataGridViewDetail.Rows.Count - 1);
                DataGridViewRow row = dataGridViewDetail.Rows[dataGridViewDetail.Rows.Count - 2];
                row.Cells["GoodsName"].Value = p.GoodsCode;
                row.Cells["GoodsCode"].Value = p.GoodsCode;
                row.Cells["GoodsSpec"].Value = p.GoodsSpec;
                row.Cells["GoodsUnit"].Value = p.GoodsUnit;
                row.Cells["Number"].Value = p.Number;
                row.Cells["RemarkD"].Value = p.Remark;
                row.Cells["Warehouse"].Value = p.WarehouseCode;
                row.Cells["OtherOutWarehouseDetailCode"].Value = p.OtherOutWarehouseDetailCode;
                row.Cells["OtherOutWarehouseCodeD"].Value = p.OtherOutWarehouseCode;
            });
        }
        /// <summary>
        /// 获取明细数据
        /// </summary>
        /// <returns></returns>
        private List<w_OtherOutWarehouseDetail> GetDetailData(string code)
        {
            List<w_OtherOutWarehouseDetail> data = new List<w_OtherOutWarehouseDetail>();
            data = _w_OtherOutWarehouseDetailServices.QueryListByClause(p => p.OtherOutWarehouseCode == code);
            return data;
        }
        private void SetDataGridViewReadonlyStatus()
        {
            dataGridViewDetail.ReadOnly = false;
            string[] cols = { "Number", "Warehouse", "RemarkD", "GoodsName" };
            foreach (DataGridViewColumn column in dataGridViewDetail.Columns)
            {
                if (!cols.Contains(column.Name))
                {
                    column.ReadOnly = true;
                }
            }
        }

        private void dataGridViewDetail_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
            {
                if (dataGridViewDetail.Columns[e.ColumnIndex].Name == "GoodsName" && !string.IsNullOrEmpty(dataGridViewDetail.Rows[e.RowIndex].Cells[e.ColumnIndex].FormattedValue.ToString()))
                {

                    dataGridViewDetail.Rows[e.RowIndex].Cells["GoodsCode"].Value = dataGridViewDetail.Rows[e.RowIndex].Cells["GoodsName"].Value;
                    var code = dataGridViewDetail.Rows[e.RowIndex].Cells["GoodsName"].Value.ToString();
                    var goods = _wGoodsServices.QueryByClause(p =>
                        p.GoodsCode == code);
                    if (goods != null)
                    {
                        dataGridViewDetail.Rows[e.RowIndex].Cells["GoodsUnit"].Value = goods.GoodsUnit;
                        dataGridViewDetail.Rows[e.RowIndex].Cells["GoodsSpec"].Value = goods.GoodsSpec;
                    }
                }
            }
        }
    }
}
