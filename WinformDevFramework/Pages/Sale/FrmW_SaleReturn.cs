using SqlSugar;
using WinformDevFramework.Core.Winform;
using WinformDevFramework.IServices;
using WinformDevFramework;
using WinformDevFramework.IServices.System;
using static System.Runtime.CompilerServices.RuntimeHelpers;
using WinformDevFramework.Services;

namespace WinformDevFramework
{
    public partial class Frmw_SaleReturn : BaseForm1
    {
        private Iw_SaleReturnServices _w_SaleReturnServices;
        private Iw_SaleReturnDetailServices _w_SaleReturnDetailServices;
        private IW_GoodsServices _w_GoodsServices;
        private Iw_SaleDetailServices _w_SaleDetailServices;
        private Iw_SaleServices _w_SaleServices;
        private Iw_SaleReturnInWarehouseServices _w_SaleReturnInWarehouseServices;
        private ISysUserServices _w_SysUserServices;
        public Frmw_SaleReturn(Iw_SaleReturnServices w_SaleReturnServices, Iw_SaleReturnDetailServices w_SaleReturnDetailServices, IW_GoodsServices w_GoodsServices, Iw_SaleDetailServices w_SaleDetailServices, Iw_SaleServices w_SaleServices, Iw_SaleReturnInWarehouseServices w_SaleReturnInWarehouseServices, ISysUserServices sysUserServices)
        {
            _w_SaleReturnServices = w_SaleReturnServices;
            InitializeComponent();
            _w_SaleReturnDetailServices = w_SaleReturnDetailServices;
            _w_GoodsServices = w_GoodsServices;
            _w_SaleDetailServices = w_SaleDetailServices;
            _w_SaleServices = w_SaleServices;
            _w_SaleReturnInWarehouseServices = w_SaleReturnInWarehouseServices;
            _w_SysUserServices = sysUserServices;
        }

        public override void Init()
        {
            base.Init();
            //查询数据
            this.dataGridViewList.DataSource = GetData();

            // 设置隐藏列
            this.dataGridViewList.Columns["SaleReturnCode"].HeaderText = "退货单号";
            this.dataGridViewList.Columns["CustomerCode"].HeaderText = "客户编码";
            this.dataGridViewList.Columns["CustomerName"].HeaderText = "客户名称";
            this.dataGridViewList.Columns["InvoicesDate"].HeaderText = "单据日期";
            this.dataGridViewList.Columns["MakeUserID"].Visible = false;
            this.dataGridViewList.Columns["InvoicesFile"].Visible = false;
            this.dataGridViewList.Columns["SaleCode"].HeaderText = "销售单号";
            this.dataGridViewList.Columns["Remark"].HeaderText = "备注";
            this.dataGridViewList.Columns["Status"].Visible = false;
            this.dataGridViewList.Columns["ReviewUserID"].Visible = false;
            this.dataGridViewList.Columns["TotalPrice"].HeaderText = "总额";
            this.dataGridViewList.Columns["TotalPrice1"].HeaderText = "已结金额";
            this.dataGridViewList.Columns["TotalPrice2"].HeaderText = "未结金额";

            //设置明细表 combobox 数据源
            GoodsName.DataSource = _w_GoodsServices.Query();
            GoodsName.ValueMember = "GoodsCode";
            GoodsName.DisplayMember = "GoodsName";

            MakeUser.DataSource = _w_SysUserServices.Query();//值查找销售部门
            MakeUser.ValueMember = "ID";
            MakeUser.DisplayMember = "Fullname";
            SetToolButtonStatus(formStatus);
            SetToolsButton();
        }

        public override void TabControlSelectingFunction(object? sender, TabControlCancelEventArgs e)
        {
            base.TabControlSelectingFunction(sender, e);
        }

        public override void DataGridViewListDoubleClick(object? sender, DataGridViewCellEventArgs e)
        {
            base.DataGridViewListDoubleClick(sender, e);
            var g = (DataGridView)sender;
            var model = g.CurrentRow.DataBoundItem as w_SaleReturn;

            //给详情页设置数据
            this.txtID.Text = model.ID.ToString();
            this.SaleReturnCode.Text = model.SaleReturnCode;
            this.CustomerCode.Text = model.CustomerCode;
            this.CustomerName.Text = model.CustomerName;
            this.InvoicesDate.Text = model.InvoicesDate.ToString();
            this.MakeUser.SelectedValue = model.MakeUserID;
            this.InvoicesFile.Text = model.InvoicesFile;
            this.SaleCode.Text = model.SaleCode;
            this.Remark.Text = model.Remark;
            this.Status.Text = model.Status;
            this.ReviewUserID.Text = model.ReviewUserID.ToString();
            TotalPrice.Text = model.TotalPrice.ToString();
            TotalPrice1.Text = model.TotalPrice1.ToString();
            TotalPrice2.Text = model.TotalPrice2.ToString();

            SetDataToDetail(GetDetailData(model.SaleReturnCode));
            SetToolButtonStatus(formStatus);
        }

        public override void AddFunction(object sender, EventArgs e)
        {
            base.AddFunction(sender, e);
            SetToolButtonStatus(formStatus);
            SetDataGridViewReadonlyStatus();
            MakeUser.SelectedValue = AppInfo.User.ID;
            dataGridViewDetail.Rows.Clear();
        }

        public override void EditFunction(object sender, EventArgs e)
        {
            if (CheckData(SaleReturnCode.Text))
            {
                base.EditFunction(sender, e);
                SetToolButtonStatus(formStatus);
                SetDataGridViewReadonlyStatus();
            }
        }

        public override void SaveFunction(object sender, EventArgs e)
        {
            base.SaveFunction(sender, e);
            {
                //新增
                if (string.IsNullOrEmpty(txtID.Text))
                {
                    w_SaleReturn model = new w_SaleReturn();
                    // TODO获取界面的数据
                    if (string.IsNullOrEmpty(this.SaleReturnCode.Text))
                    {
                        model.SaleReturnCode = $"XSTH{DateTime.Now.ToString("yyyyMMddHHmmss")}";
                    }
                    else
                    {
                        model.SaleReturnCode = this.SaleReturnCode.Text;
                    }

                    model.CustomerCode = this.CustomerCode.Text;
                    model.CustomerName = this.CustomerName.Text;
                    model.InvoicesDate = this.InvoicesDate.Text.ToDateTime();
                    model.MakeUserID = this.MakeUser.SelectedValue.ToInt32();
                    model.InvoicesFile = this.InvoicesFile.Text;
                    model.SaleCode = this.SaleCode.Text;
                    model.Remark = this.Remark.Text;
                    model.Status = this.Status.Text;
                    model.ReviewUserID = this.ReviewUserID.Text.ToInt32();
                    model.TotalPrice = TotalPrice.Text.ToDecimal();
                    model.TotalPrice1 = TotalPrice1.Text.ToDecimal();
                    model.TotalPrice2 = TotalPrice2.Text.ToDecimal();

                    //明细表
                    List<w_SaleReturnDetail> details = new List<w_SaleReturnDetail>();
                    foreach (DataGridViewRow row in dataGridViewDetail.Rows)
                    {
                        if (!row.IsNewRow)
                        {
                            w_SaleReturnDetail detail = new w_SaleReturnDetail();
                            detail.Number = row.Cells["Number"].FormattedValue.ToDecimal();
                            detail.GoodsName = row.Cells["GoodsName"].FormattedValue.ToString();
                            detail.GoodsCode = row.Cells["GoodsName"].Value.ToString();
                            detail.GoodsUnit = row.Cells["GoodsUnit"].FormattedValue.ToString();
                            detail.GoodsSpec = row.Cells["GoodsSpec"].FormattedValue.ToString();
                            detail.UnitPrice = row.Cells["UnitPrice"].FormattedValue.ToDecimal();
                            detail.DiscountPrice = row.Cells["DiscountPrice"].FormattedValue.ToDecimal();
                            detail.TotalPrice = row.Cells["TotalPriceD"].FormattedValue.ToDecimal();
                            detail.TaxRate = row.Cells["TaxRate"].FormattedValue.ToDecimal();
                            detail.TaxUnitPrice = row.Cells["TaxUnitPrice"].FormattedValue.ToDecimal();
                            detail.TaxPrice = row.Cells["TaxPrice"].FormattedValue.ToDecimal();
                            detail.TotalTaxPrice = row.Cells["TotalTaxPrice"].FormattedValue.ToDecimal();
                            detail.Remark = row.Cells["RemarkD"].FormattedValue.ToString();
                            details.Add(detail);
                        }
                    }
                    var id = _w_SaleReturnServices.AddSaleReturnInfo(model, details);
                    txtID.Text = id.ToString();
                    if (id > 0)
                    {
                        MessageBox.Show("保存成功！", "提示");
                        SaleReturnCode.Text = model.SaleReturnCode;
                        SetDataToDetail(GetDetailData(model.SaleReturnCode));
                    }
                }
                // 修改
                else
                {
                    w_SaleReturn model = _w_SaleReturnServices.QueryById(int.Parse(txtID.Text));
                    // TODO获取界面的数据
                    model.SaleReturnCode = this.SaleReturnCode.Text;
                    model.CustomerCode = this.CustomerCode.Text;
                    model.CustomerName = this.CustomerName.Text;
                    model.InvoicesDate = this.InvoicesDate.Value;
                    model.MakeUserID = this.MakeUser.SelectedValue.ToInt32();
                    model.InvoicesFile = this.InvoicesFile.Text;
                    model.SaleCode = this.SaleCode.Text;
                    model.Remark = this.Remark.Text;
                    model.Status = this.Status.Text;
                    model.ReviewUserID = this.ReviewUserID.Text.ToInt32();
                    model.TotalPrice = TotalPrice.Text.ToDecimal();
                    model.TotalPrice1 = TotalPrice1.Text.ToDecimal();
                    model.TotalPrice2 = TotalPrice2.Text.ToDecimal();
                    //明细表
                    List<w_SaleReturnDetail> details = new List<w_SaleReturnDetail>();
                    foreach (DataGridViewRow row in dataGridViewDetail.Rows)
                    {
                        if (!row.IsNewRow)
                        {
                            w_SaleReturnDetail detail = new w_SaleReturnDetail();
                            detail.SaleCode = row.Cells["SaleCodeD"].FormattedValue.ToString();
                            detail.SaleReturnCode = row.Cells["SaleReturnCodeD"].FormattedValue.ToString();
                            detail.SaleReturnDetailCode = row.Cells["SaleReturnDetailCode"].FormattedValue.ToString();

                            detail.Number = row.Cells["Number"].FormattedValue.ToDecimal();
                            detail.GoodsName = row.Cells["GoodsName"].FormattedValue.ToString();
                            detail.GoodsCode = row.Cells["GoodsName"].Value.ToString();
                            detail.GoodsUnit = row.Cells["GoodsUnit"].FormattedValue.ToString();
                            detail.GoodsSpec = row.Cells["GoodsSpec"].FormattedValue.ToString();
                            detail.UnitPrice = row.Cells["UnitPrice"].FormattedValue.ToDecimal();
                            detail.DiscountPrice = row.Cells["DiscountPrice"].FormattedValue.ToDecimal();
                            detail.TotalPrice = row.Cells["TotalPriceD"].FormattedValue.ToDecimal();
                            detail.TaxRate = row.Cells["TaxRate"].FormattedValue.ToDecimal();
                            detail.TaxUnitPrice = row.Cells["TaxUnitPrice"].FormattedValue.ToDecimal();
                            detail.TaxPrice = row.Cells["TaxPrice"].FormattedValue.ToDecimal();
                            detail.TotalTaxPrice = row.Cells["TotalTaxPrice"].FormattedValue.ToDecimal();
                            detail.Remark = row.Cells["RemarkD"].FormattedValue.ToString();
                            details.Add(detail);
                        }
                    }
                    if (_w_SaleReturnServices.UpdateSaleReturnInfo(model, details))
                    {
                        MessageBox.Show("保存成功！", "提示");
                        SetDataToDetail(GetDetailData(model.SaleReturnCode));
                    }
                }
                SetToolButtonStatus(formStatus);
                //列表重新赋值
                this.dataGridViewList.DataSource = GetData();
            }
        }

        public override void CanelFunction(object sender, EventArgs e)
        {
            bool isAdd = formStatus == FormStatus.Add;
            base.CanelFunction(sender, e);
            SetToolButtonStatus(formStatus);
            btnEdit.Enabled = !isAdd;
            btnDel.Enabled = !isAdd;
            SetDataToDetail(GetDetailData(SaleReturnCode.Text));
        }

        public override void DelFunction(object sender, EventArgs e)
        {
            if (CheckData(SaleReturnCode.Text))
            {
                base.DelFunction(sender, e);
                var result = MessageBox.Show("是否删除？", "确认", MessageBoxButtons.YesNoCancel);
                if (result == DialogResult.Yes)
                {
                    _w_SaleReturnServices.DeleteById(Int32.Parse(txtID.Text));
                    _w_SaleReturnDetailServices.Delete(p => p.SaleReturnCode == SaleReturnCode.Text);
                    formStatus = FormStatus.Del;
                    SetToolButtonStatus(formStatus);
                    //列表重新赋值
                    dataGridViewDetail.Rows.Clear();
                    this.dataGridViewList.DataSource = GetData();
                }
            }
        }

        public override void SetToolButtonStatus(FormStatus status)
        {
            base.SetToolButtonStatus(status);
            switch (status)
            {
                case FormStatus.Add:
                    {
                        btnAdd.Enabled = false;
                        btnEdit.Enabled = false;
                        btnSave.Enabled = true;
                        btnDel.Enabled = false;
                        btnCanel.Enabled = true;
                        dataGridViewDetail.ReadOnly = false;
                        SetControlStatus(this.groupBox1, true);
                        ClearControlsText(this.groupBox1);
                        break;
                    }
                case FormStatus.Edit:
                    {
                        btnAdd.Enabled = false;
                        btnEdit.Enabled = false;
                        btnSave.Enabled = true;
                        btnDel.Enabled = false;
                        btnCanel.Enabled = true;
                        dataGridViewDetail.ReadOnly = false;
                        SetControlStatus(this.groupBox1, true);
                        break;
                    }
                case FormStatus.View:
                    {
                        btnAdd.Enabled = true;
                        btnEdit.Enabled = true;
                        btnSave.Enabled = false;
                        btnDel.Enabled = true;
                        btnCanel.Enabled = false;
                        dataGridViewDetail.ReadOnly = true;
                        SetControlStatus(this.groupBox1, false);
                        break;
                    }
                case FormStatus.Canel:
                    {
                        btnAdd.Enabled = true;
                        btnEdit.Enabled = true;
                        btnSave.Enabled = false;
                        btnDel.Enabled = false;
                        btnCanel.Enabled = false;
                        dataGridViewDetail.ReadOnly = true;
                        SetControlStatus(this.groupBox1, false);
                        break;
                    }
                case FormStatus.First:
                    {
                        btnAdd.Enabled = true;
                        btnEdit.Enabled = false;
                        btnSave.Enabled = false;
                        btnDel.Enabled = false;
                        btnCanel.Enabled = false;
                        dataGridViewDetail.ReadOnly = true;
                        SetControlStatus(this.groupBox1, false);
                        break;
                    }
                case FormStatus.Save:
                    {
                        btnAdd.Enabled = true;
                        btnEdit.Enabled = true;
                        btnSave.Enabled = false;
                        btnDel.Enabled = true;
                        btnCanel.Enabled = false;
                        dataGridViewDetail.ReadOnly = true;
                        SetControlStatus(this.groupBox1, false);
                        break;
                    }
                case FormStatus.Del:
                    {
                        btnAdd.Enabled = true;
                        btnEdit.Enabled = false;
                        btnSave.Enabled = false;
                        btnDel.Enabled = false;
                        btnCanel.Enabled = false;
                        dataGridViewDetail.ReadOnly = true;
                        SetControlStatus(this.groupBox1, false);
                        ClearControlsText(this.groupBox1);
                        break;
                    }
            }
        }

        public override bool ValidateData()
        {
            return true;
        }
        /// <summary>
        /// 获取数据
        /// </summary>
        /// <returns></returns>
        private List<w_SaleReturn> GetData()
        {
            List<w_SaleReturn> data = new List<w_SaleReturn>();
            data = _w_SaleReturnServices.Query();
            return data;
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void SaleCode_KeyPress(object sender, KeyPressEventArgs e)
        {

            if (e.KeyChar == 13)
            {
                if (!string.IsNullOrEmpty(SaleCode.Text.Trim()))
                {
                    var data = _w_SaleServices.QueryByClause(p => p.SaleCode.Equals(SaleCode.Text.Trim()));
                    if (data != null)
                    {
                        CustomerName.Text = data.CustomerName;
                        CustomerCode.Text = data.CustomerCode;

                        List<w_SaleDetail> details = new List<w_SaleDetail>();
                        details = _w_SaleDetailServices.QueryListByClause(p => p.SaleCode == data.SaleCode);
                        SetDataToDetail(details);
                    }
                    else
                    {
                        //GoodsName.DataSource=new List<w_SaleReturnDetail>();;
                        MessageBox.Show("销售单号不存在！");
                    }
                }
            }
        }
        /// <summary>
        /// 明细表 设置数据 --销售单号带出
        /// </summary>
        /// <param name="details"></param>
        private void SetDataToDetail(List<w_SaleDetail> details)
        {
            dataGridViewDetail.Rows.Clear();
            details.ForEach(p =>
            {
                dataGridViewDetail.Rows.AddCopy(dataGridViewDetail.Rows.Count - 1);
                DataGridViewRow row = dataGridViewDetail.Rows[dataGridViewDetail.Rows.Count - 2];
                row.Cells["SaleCodeD"].Value = p.SaleCode;
                //row.Cells["SaleDetailCode"].Value = p.SaleDetailCode;
                row.Cells["GoodsName"].Value = p.GoodsCode;
                row.Cells["GoodsCode"].Value = p.GoodsCode;
                row.Cells["GoodsUnit"].Value = p.GoodsUnit;
                row.Cells["GoodsSpec"].Value = p.GoodsSpec;
                row.Cells["Number"].Value = 0;

                row.Cells["UnitPrice"].Value = p.UnitPrice;
                row.Cells["DiscountPrice"].Value = 0;
                row.Cells["TotalPriceD"].Value = 0;
                row.Cells["TaxRate"].Value = p.TaxRate;
                row.Cells["TaxUnitPrice"].Value = 0;
                row.Cells["TaxPrice"].Value = 0;
                row.Cells["TotalTaxPrice"].Value = 0;
                row.Cells["RemarkD"].Value = p.Remark;
                row.Cells["TotalTaxPrice"].Value = 0;
                //row.Cells["BuyID"].Value = p.BuyID;
                //row.Cells["BuyCodeD"].Value = p.BuyCode;
                //row.Cells["BuyDetailCode"].Value = p.BuyDetailCode;
            });
        }
        /// <summary>
        /// 明细表 设置数据 --主表带出
        /// </summary>
        /// <param name="details"></param>
        private void SetDataToDetail(List<w_SaleReturnDetail> details)
        {
            dataGridViewDetail.Rows.Clear();
            details.ForEach(p =>
            {
                dataGridViewDetail.Rows.AddCopy(dataGridViewDetail.Rows.Count - 1);
                DataGridViewRow row = dataGridViewDetail.Rows[dataGridViewDetail.Rows.Count - 2];
                row.Cells["SaleCodeD"].Value = p.SaleCode;
                row.Cells["GoodsName"].Value = p.GoodsCode;
                row.Cells["GoodsCode"].Value = p.GoodsCode;
                row.Cells["Number"].Value = p.Number;
                row.Cells["GoodsUnit"].Value = p.GoodsUnit;
                row.Cells["GoodsSpec"].Value = p.GoodsSpec;
                row.Cells["UnitPrice"].Value = p.UnitPrice;
                row.Cells["DiscountPrice"].Value = p.DiscountPrice;
                row.Cells["TotalPriceD"].Value = p.TotalPrice;
                row.Cells["TaxRate"].Value = p.TaxRate;
                row.Cells["TaxUnitPrice"].Value = p.TaxUnitPrice;
                row.Cells["TaxPrice"].Value = p.TaxPrice;
                row.Cells["TotalTaxPrice"].Value = p.TotalTaxPrice;
                row.Cells["RemarkD"].Value = p.Remark;
                row.Cells["SaleReturnCodeD"].Value = p.SaleReturnCode;
                row.Cells["SaleReturnDetailCode"].Value = p.SaleReturnDetailCode;
            });
        }
        private void SetDetailGoods(string code)
        {
            GoodsName.DataSource =
                _w_SaleDetailServices.QueryListByClause(p => p.SaleCode == code);
            GoodsName.DisplayMember = "GoodsCode";
            GoodsName.ValueMember = "GoodsName";
        }

        private void dataGridViewDetail_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
            {
                if (dataGridViewDetail.Columns[e.ColumnIndex].Name == "GoodsName" && dataGridViewDetail.Rows[e.RowIndex].Cells["GoodsName"].Value != null)
                {
                    var model = _w_GoodsServices.QueryByClause(p => p.GoodsCode == dataGridViewDetail.Rows[e.RowIndex].Cells["GoodsName"].Value);
                    dataGridViewDetail.Rows[e.RowIndex].Cells["GoodsCode"].Value = dataGridViewDetail.Rows[e.RowIndex].Cells["GoodsName"].Value;
                    dataGridViewDetail.Rows[e.RowIndex].Cells["GoodsSpec"].Value = model.GoodsSpec;
                    dataGridViewDetail.Rows[e.RowIndex].Cells["GoodsUnit"].Value = model.GoodsUnit;
                    dataGridViewDetail.Rows[e.RowIndex].Cells["Number"].Value = 0;
                    dataGridViewDetail.Rows[e.RowIndex].Cells["UnitPrice"].Value = 0;
                    dataGridViewDetail.Rows[e.RowIndex].Cells["TotalPriceD"].Value = 0;
                    dataGridViewDetail.Rows[e.RowIndex].Cells["TaxRate"].Value = 0;
                    dataGridViewDetail.Rows[e.RowIndex].Cells["DiscountPrice"].Value = 0;

                    dataGridViewDetail.Rows[e.RowIndex].Cells["TaxUnitPrice"].Value = 0;
                    dataGridViewDetail.Rows[e.RowIndex].Cells["TaxPrice"].Value = 0;
                    dataGridViewDetail.Rows[e.RowIndex].Cells["TotalTaxPrice"].Value = 0;

                }

                //数量
                if (dataGridViewDetail.Columns[e.ColumnIndex].Name == "Number" || dataGridViewDetail.Columns[e.ColumnIndex].Name == "UnitPrice" || dataGridViewDetail.Columns[e.ColumnIndex].Name == "DiscountPrice" || dataGridViewDetail.Columns[e.ColumnIndex].Name == "TaxRate")
                {
                    decimal value = dataGridViewDetail.Rows[e.RowIndex].Cells["Number"].Value == null ? 0 : dataGridViewDetail.Rows[e.RowIndex].Cells["Number"].Value.ToDecimal();
                    //购货单价
                    decimal unitPrice = dataGridViewDetail.Rows[e.RowIndex].Cells["UnitPrice"].Value == null ? 0 : dataGridViewDetail.Rows[e.RowIndex].Cells["UnitPrice"].Value.ToDecimal();
                    //折扣金额
                    decimal discountPrice = dataGridViewDetail.Rows[e.RowIndex].Cells["DiscountPrice"].Value == null ? 0 : dataGridViewDetail.Rows[e.RowIndex].Cells["DiscountPrice"].Value.ToDecimal();
                    //税率
                    decimal taxRate = dataGridViewDetail.Rows[e.RowIndex].Cells["TaxRate"].Value == null
                        ? 0
                        : dataGridViewDetail.Rows[e.RowIndex].Cells["TaxRate"].Value.ToDecimal() / 100;

                    decimal jine = value * unitPrice - discountPrice;
                    //金额
                    dataGridViewDetail.Rows[e.RowIndex].Cells["TotalPriceD"].Value = jine;

                    decimal danjia = unitPrice * (1 + taxRate);

                    //含税单价
                    dataGridViewDetail.Rows[e.RowIndex].Cells["TaxUnitPrice"].Value = danjia;

                    //税额
                    decimal shuie = unitPrice * (taxRate) * value - discountPrice * taxRate;
                    dataGridViewDetail.Rows[e.RowIndex].Cells["TaxPrice"].Value = shuie;

                    //含税金额
                    dataGridViewDetail.Rows[e.RowIndex].Cells["TotalTaxPrice"].Value =
                        shuie + jine;
                    SetFormTotalData();
                }
            }
        }
        private void SetFormTotalData()
        {
            decimal total = 0;
            foreach (DataGridViewRow row in dataGridViewDetail.Rows)
            {
                if (!row.IsNewRow)
                {
                    total += row.Cells["TotalTaxPrice"].Value.ToDecimal();
                }
            }
            TotalPrice.Text = total.ToString();
        }
        /// <summary>
        /// 获取明细数据
        /// </summary>
        /// <returns></returns>
        private List<w_SaleReturnDetail> GetDetailData(string code)
        {
            List<w_SaleReturnDetail> data = new List<w_SaleReturnDetail>();
            data = _w_SaleReturnDetailServices.QueryListByClause(p => p.SaleReturnCode == code);
            return data;
        }
        private void SetDataGridViewReadonlyStatus()
        {
            dataGridViewDetail.ReadOnly = false;
            string[] cols = { "GoodsName", "Number", "RemarkD", "UnitPrice", "TaxRate", "DiscountPrice" };
            foreach (DataGridViewColumn column in dataGridViewDetail.Columns)
            {
                if (!cols.Contains(column.Name))
                {
                    column.ReadOnly = true;
                }
            }
        }
        /// <summary>
        /// 数据校验
        /// </summary>
        /// <returns></returns>
        private bool CheckData(string code)
        {
            //是否有入库记录
            if (_w_SaleReturnInWarehouseServices.Exists(p => p.SaleReturnCode == code))
            {
                MessageBox.Show("该退货单有入库记录，不能操作！", "提示");
                return false;
            }
            return true;
        }
    }
}
