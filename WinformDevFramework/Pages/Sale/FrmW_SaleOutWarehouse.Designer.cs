namespace WinformDevFramework
{
    partial class Frmw_SaleOutWarehouse
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblSaleCode = new System.Windows.Forms.Label();
            this.SaleCode = new System.Windows.Forms.TextBox();
            this.lblSaleOutWarehouseCode = new System.Windows.Forms.Label();
            this.SaleOutWarehouseCode = new System.Windows.Forms.TextBox();
            this.lblCustomerCode = new System.Windows.Forms.Label();
            this.CustomerCode = new System.Windows.Forms.TextBox();
            this.lblCustomerName = new System.Windows.Forms.Label();
            this.CustomerName = new System.Windows.Forms.TextBox();
            this.lblInvicesDate = new System.Windows.Forms.Label();
            this.InvicesDate = new System.Windows.Forms.DateTimePicker();
            this.lblInvicesFile = new System.Windows.Forms.Label();
            this.InvicesFile = new System.Windows.Forms.TextBox();
            this.lblMakeUserID = new System.Windows.Forms.Label();
            this.lblReviewUserID = new System.Windows.Forms.Label();
            this.ReviewUserID = new System.Windows.Forms.NumericUpDown();
            this.lblStatus = new System.Windows.Forms.Label();
            this.Status = new System.Windows.Forms.TextBox();
            this.lblRemark = new System.Windows.Forms.Label();
            this.Remark = new System.Windows.Forms.TextBox();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.dataGridViewDetail = new System.Windows.Forms.DataGridView();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SaleCodeD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SaleDetailCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SaleOutWarehouseCodeD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.WarehouseName = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.WarehouseCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GoodsCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GoodsName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GoodsSpec = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GoodsUnit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Number = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RemarkD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SaleOutWarehouseDetailCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MakeUser = new System.Windows.Forms.ComboBox();
            this.palTools.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabDataEdit.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.flowLayoutPanelTools.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ReviewUserID)).BeginInit();
            this.tabControl2.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewDetail)).BeginInit();
            this.SuspendLayout();
            // 
            // palTools
            // 
            this.palTools.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            // 
            // tabControl1
            // 
            this.tabControl1.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.tabControl1.Size = new System.Drawing.Size(800, 535);
            // 
            // tabList
            // 
            this.tabList.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.tabList.Padding = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.tabList.Size = new System.Drawing.Size(792, 505);
            // 
            // tabDataEdit
            // 
            this.tabDataEdit.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.tabDataEdit.Padding = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.tabDataEdit.Size = new System.Drawing.Size(423, 214);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.MakeUser);
            this.groupBox1.Controls.Add(this.tabControl2);
            this.groupBox1.Controls.Add(this.Remark);
            this.groupBox1.Controls.Add(this.lblRemark);
            this.groupBox1.Controls.Add(this.Status);
            this.groupBox1.Controls.Add(this.lblStatus);
            this.groupBox1.Controls.Add(this.ReviewUserID);
            this.groupBox1.Controls.Add(this.lblReviewUserID);
            this.groupBox1.Controls.Add(this.lblMakeUserID);
            this.groupBox1.Controls.Add(this.InvicesFile);
            this.groupBox1.Controls.Add(this.lblInvicesFile);
            this.groupBox1.Controls.Add(this.InvicesDate);
            this.groupBox1.Controls.Add(this.lblInvicesDate);
            this.groupBox1.Controls.Add(this.CustomerName);
            this.groupBox1.Controls.Add(this.lblCustomerName);
            this.groupBox1.Controls.Add(this.CustomerCode);
            this.groupBox1.Controls.Add(this.lblCustomerCode);
            this.groupBox1.Controls.Add(this.SaleOutWarehouseCode);
            this.groupBox1.Controls.Add(this.lblSaleOutWarehouseCode);
            this.groupBox1.Controls.Add(this.SaleCode);
            this.groupBox1.Controls.Add(this.lblSaleCode);
            this.groupBox1.Location = new System.Drawing.Point(6, 5);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.groupBox1.Padding = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.groupBox1.Size = new System.Drawing.Size(411, 204);
            this.groupBox1.Controls.SetChildIndex(this.lblSaleCode, 0);
            this.groupBox1.Controls.SetChildIndex(this.SaleCode, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblSaleOutWarehouseCode, 0);
            this.groupBox1.Controls.SetChildIndex(this.SaleOutWarehouseCode, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblCustomerCode, 0);
            this.groupBox1.Controls.SetChildIndex(this.CustomerCode, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblCustomerName, 0);
            this.groupBox1.Controls.SetChildIndex(this.CustomerName, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblInvicesDate, 0);
            this.groupBox1.Controls.SetChildIndex(this.InvicesDate, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblInvicesFile, 0);
            this.groupBox1.Controls.SetChildIndex(this.InvicesFile, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblMakeUserID, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblReviewUserID, 0);
            this.groupBox1.Controls.SetChildIndex(this.ReviewUserID, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblStatus, 0);
            this.groupBox1.Controls.SetChildIndex(this.Status, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblRemark, 0);
            this.groupBox1.Controls.SetChildIndex(this.Remark, 0);
            this.groupBox1.Controls.SetChildIndex(this.txtID, 0);
            this.groupBox1.Controls.SetChildIndex(this.tabControl2, 0);
            this.groupBox1.Controls.SetChildIndex(this.MakeUser, 0);
            // 
            // flowLayoutPanelTools
            // 
            this.flowLayoutPanelTools.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            // 
            // txtID
            // 
            this.txtID.Location = new System.Drawing.Point(199, 31);
            this.txtID.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.txtID.Size = new System.Drawing.Size(56, 23);
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(2, 2);
            this.btnAdd.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnAdd.Size = new System.Drawing.Size(53, 25);
            // 
            // btnEdit
            // 
            this.btnEdit.Location = new System.Drawing.Point(59, 2);
            this.btnEdit.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnEdit.Size = new System.Drawing.Size(53, 25);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(116, 2);
            this.btnSave.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnSave.Size = new System.Drawing.Size(53, 25);
            // 
            // btnCanel
            // 
            this.btnCanel.Location = new System.Drawing.Point(173, 2);
            this.btnCanel.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnCanel.Size = new System.Drawing.Size(53, 25);
            // 
            // btnDel
            // 
            this.btnDel.Location = new System.Drawing.Point(230, 2);
            this.btnDel.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnDel.Size = new System.Drawing.Size(53, 25);
            // 
            // btnResetPW
            // 
            this.btnResetPW.Location = new System.Drawing.Point(287, 2);
            this.btnResetPW.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnResetPW.Size = new System.Drawing.Size(53, 25);
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(401, 2);
            this.btnClose.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnClose.Size = new System.Drawing.Size(53, 25);
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(344, 2);
            this.btnSearch.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnSearch.Size = new System.Drawing.Size(53, 25);
            // 
            // lblSaleCode
            // 
            this.lblSaleCode.Location = new System.Drawing.Point(230, 10);
            this.lblSaleCode.Name = "lblSaleCode";
            this.lblSaleCode.Size = new System.Drawing.Size(85, 23);
            this.lblSaleCode.TabIndex = 2;
            this.lblSaleCode.Text = "销售订单";
            this.lblSaleCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // SaleCode
            // 
            this.SaleCode.Location = new System.Drawing.Point(320, 10);
            this.SaleCode.Name = "SaleCode";
            this.SaleCode.Size = new System.Drawing.Size(130, 23);
            this.SaleCode.TabIndex = 1;
            // 
            // lblSaleOutWarehouseCode
            // 
            this.lblSaleOutWarehouseCode.Location = new System.Drawing.Point(10, 10);
            this.lblSaleOutWarehouseCode.Name = "lblSaleOutWarehouseCode";
            this.lblSaleOutWarehouseCode.Size = new System.Drawing.Size(85, 23);
            this.lblSaleOutWarehouseCode.TabIndex = 4;
            this.lblSaleOutWarehouseCode.Text = "销售出库单号";
            this.lblSaleOutWarehouseCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // SaleOutWarehouseCode
            // 
            this.SaleOutWarehouseCode.Location = new System.Drawing.Point(100, 10);
            this.SaleOutWarehouseCode.Name = "SaleOutWarehouseCode";
            this.SaleOutWarehouseCode.PlaceholderText = "为空自动生成";
            this.SaleOutWarehouseCode.Size = new System.Drawing.Size(130, 23);
            this.SaleOutWarehouseCode.TabIndex = 3;
            // 
            // lblCustomerCode
            // 
            this.lblCustomerCode.Location = new System.Drawing.Point(230, 45);
            this.lblCustomerCode.Name = "lblCustomerCode";
            this.lblCustomerCode.Size = new System.Drawing.Size(85, 23);
            this.lblCustomerCode.TabIndex = 6;
            this.lblCustomerCode.Text = "客户编码";
            this.lblCustomerCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // CustomerCode
            // 
            this.CustomerCode.Location = new System.Drawing.Point(320, 45);
            this.CustomerCode.Name = "CustomerCode";
            this.CustomerCode.Size = new System.Drawing.Size(130, 23);
            this.CustomerCode.TabIndex = 5;
            // 
            // lblCustomerName
            // 
            this.lblCustomerName.Location = new System.Drawing.Point(10, 45);
            this.lblCustomerName.Name = "lblCustomerName";
            this.lblCustomerName.Size = new System.Drawing.Size(85, 23);
            this.lblCustomerName.TabIndex = 8;
            this.lblCustomerName.Text = "客户名称";
            this.lblCustomerName.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // CustomerName
            // 
            this.CustomerName.Location = new System.Drawing.Point(100, 45);
            this.CustomerName.Name = "CustomerName";
            this.CustomerName.Size = new System.Drawing.Size(130, 23);
            this.CustomerName.TabIndex = 7;
            // 
            // lblInvicesDate
            // 
            this.lblInvicesDate.Location = new System.Drawing.Point(450, 10);
            this.lblInvicesDate.Name = "lblInvicesDate";
            this.lblInvicesDate.Size = new System.Drawing.Size(85, 23);
            this.lblInvicesDate.TabIndex = 10;
            this.lblInvicesDate.Text = "单据日期";
            this.lblInvicesDate.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // InvicesDate
            // 
            this.InvicesDate.Location = new System.Drawing.Point(540, 10);
            this.InvicesDate.Name = "InvicesDate";
            this.InvicesDate.Size = new System.Drawing.Size(130, 23);
            this.InvicesDate.TabIndex = 9;
            // 
            // lblInvicesFile
            // 
            this.lblInvicesFile.Location = new System.Drawing.Point(450, 45);
            this.lblInvicesFile.Name = "lblInvicesFile";
            this.lblInvicesFile.Size = new System.Drawing.Size(85, 23);
            this.lblInvicesFile.TabIndex = 12;
            this.lblInvicesFile.Text = "单据附件";
            this.lblInvicesFile.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblInvicesFile.Visible = false;
            // 
            // InvicesFile
            // 
            this.InvicesFile.Location = new System.Drawing.Point(540, 45);
            this.InvicesFile.Name = "InvicesFile";
            this.InvicesFile.Size = new System.Drawing.Size(130, 23);
            this.InvicesFile.TabIndex = 11;
            this.InvicesFile.Visible = false;
            // 
            // lblMakeUserID
            // 
            this.lblMakeUserID.Location = new System.Drawing.Point(10, 80);
            this.lblMakeUserID.Name = "lblMakeUserID";
            this.lblMakeUserID.Size = new System.Drawing.Size(85, 23);
            this.lblMakeUserID.TabIndex = 14;
            this.lblMakeUserID.Text = "制单人";
            this.lblMakeUserID.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblReviewUserID
            // 
            this.lblReviewUserID.Location = new System.Drawing.Point(230, 80);
            this.lblReviewUserID.Name = "lblReviewUserID";
            this.lblReviewUserID.Size = new System.Drawing.Size(85, 23);
            this.lblReviewUserID.TabIndex = 16;
            this.lblReviewUserID.Text = "审核人";
            this.lblReviewUserID.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblReviewUserID.Visible = false;
            // 
            // ReviewUserID
            // 
            this.ReviewUserID.Location = new System.Drawing.Point(320, 80);
            this.ReviewUserID.Name = "ReviewUserID";
            this.ReviewUserID.Size = new System.Drawing.Size(130, 23);
            this.ReviewUserID.TabIndex = 15;
            this.ReviewUserID.Visible = false;
            // 
            // lblStatus
            // 
            this.lblStatus.Location = new System.Drawing.Point(450, 80);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(85, 23);
            this.lblStatus.TabIndex = 18;
            this.lblStatus.Text = "单据状态";
            this.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblStatus.Visible = false;
            // 
            // Status
            // 
            this.Status.Location = new System.Drawing.Point(540, 80);
            this.Status.Name = "Status";
            this.Status.Size = new System.Drawing.Size(130, 23);
            this.Status.TabIndex = 17;
            this.Status.Visible = false;
            // 
            // lblRemark
            // 
            this.lblRemark.Location = new System.Drawing.Point(10, 118);
            this.lblRemark.Name = "lblRemark";
            this.lblRemark.Size = new System.Drawing.Size(85, 23);
            this.lblRemark.TabIndex = 20;
            this.lblRemark.Text = "备注";
            this.lblRemark.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Remark
            // 
            this.Remark.Location = new System.Drawing.Point(100, 118);
            this.Remark.Name = "Remark";
            this.Remark.Size = new System.Drawing.Size(130, 23);
            this.Remark.TabIndex = 19;
            // 
            // tabControl2
            // 
            this.tabControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl2.Controls.Add(this.tabPage1);
            this.tabControl2.Location = new System.Drawing.Point(2, 144);
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(399, 52);
            this.tabControl2.TabIndex = 21;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.dataGridViewDetail);
            this.tabPage1.Location = new System.Drawing.Point(4, 26);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.tabPage1.Size = new System.Drawing.Size(391, 22);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "出库明细";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // dataGridViewDetail
            // 
            this.dataGridViewDetail.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewDetail.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dataGridViewDetail.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridViewDetail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewDetail.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ID,
            this.SaleCodeD,
            this.SaleDetailCode,
            this.SaleOutWarehouseCodeD,
            this.WarehouseName,
            this.WarehouseCode,
            this.GoodsCode,
            this.GoodsName,
            this.GoodsSpec,
            this.GoodsUnit,
            this.Number,
            this.RemarkD,
            this.SaleOutWarehouseDetailCode});
            this.dataGridViewDetail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewDetail.GridColor = System.Drawing.SystemColors.Control;
            this.dataGridViewDetail.Location = new System.Drawing.Point(3, 3);
            this.dataGridViewDetail.Name = "dataGridViewDetail";
            this.dataGridViewDetail.RowHeadersVisible = false;
            this.dataGridViewDetail.RowHeadersWidth = 72;
            this.dataGridViewDetail.RowTemplate.Height = 25;
            this.dataGridViewDetail.Size = new System.Drawing.Size(385, 16);
            this.dataGridViewDetail.TabIndex = 1;
            // 
            // ID
            // 
            this.ID.DataPropertyName = "ID";
            this.ID.HeaderText = "ID";
            this.ID.MinimumWidth = 9;
            this.ID.Name = "ID";
            this.ID.ReadOnly = true;
            this.ID.Visible = false;
            // 
            // SaleCodeD
            // 
            this.SaleCodeD.DataPropertyName = "SaleCode";
            this.SaleCodeD.HeaderText = "销售单号";
            this.SaleCodeD.MinimumWidth = 9;
            this.SaleCodeD.Name = "SaleCodeD";
            this.SaleCodeD.ReadOnly = true;
            this.SaleCodeD.Visible = false;
            // 
            // SaleDetailCode
            // 
            this.SaleDetailCode.DataPropertyName = "SaleDetailCode";
            this.SaleDetailCode.HeaderText = "销售明细单号";
            this.SaleDetailCode.MinimumWidth = 9;
            this.SaleDetailCode.Name = "SaleDetailCode";
            this.SaleDetailCode.ReadOnly = true;
            this.SaleDetailCode.Visible = false;
            // 
            // SaleOutWarehouseCodeD
            // 
            this.SaleOutWarehouseCodeD.DataPropertyName = "SaleOutWarehouseCode";
            this.SaleOutWarehouseCodeD.HeaderText = "销售出库单号";
            this.SaleOutWarehouseCodeD.MinimumWidth = 9;
            this.SaleOutWarehouseCodeD.Name = "SaleOutWarehouseCodeD";
            this.SaleOutWarehouseCodeD.ReadOnly = true;
            this.SaleOutWarehouseCodeD.Visible = false;
            // 
            // WarehouseName
            // 
            this.WarehouseName.HeaderText = "仓库";
            this.WarehouseName.MinimumWidth = 9;
            this.WarehouseName.Name = "WarehouseName";
            // 
            // WarehouseCode
            // 
            this.WarehouseCode.HeaderText = "仓库编码";
            this.WarehouseCode.MinimumWidth = 9;
            this.WarehouseCode.Name = "WarehouseCode";
            this.WarehouseCode.ReadOnly = true;
            // 
            // GoodsCode
            // 
            this.GoodsCode.DataPropertyName = "GoodsCode";
            this.GoodsCode.HeaderText = "商品编码";
            this.GoodsCode.MinimumWidth = 9;
            this.GoodsCode.Name = "GoodsCode";
            this.GoodsCode.ReadOnly = true;
            this.GoodsCode.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.GoodsCode.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // GoodsName
            // 
            this.GoodsName.DataPropertyName = "GoodsName";
            this.GoodsName.HeaderText = "商品名称";
            this.GoodsName.MinimumWidth = 9;
            this.GoodsName.Name = "GoodsName";
            this.GoodsName.ReadOnly = true;
            // 
            // GoodsSpec
            // 
            this.GoodsSpec.DataPropertyName = "GoodsSpec";
            this.GoodsSpec.HeaderText = "商品规格";
            this.GoodsSpec.MinimumWidth = 9;
            this.GoodsSpec.Name = "GoodsSpec";
            this.GoodsSpec.ReadOnly = true;
            // 
            // GoodsUnit
            // 
            this.GoodsUnit.DataPropertyName = "GoodsUnit";
            this.GoodsUnit.HeaderText = "计量单位";
            this.GoodsUnit.MinimumWidth = 9;
            this.GoodsUnit.Name = "GoodsUnit";
            this.GoodsUnit.ReadOnly = true;
            // 
            // Number
            // 
            this.Number.DataPropertyName = "Number";
            this.Number.HeaderText = "数量";
            this.Number.MinimumWidth = 9;
            this.Number.Name = "Number";
            // 
            // RemarkD
            // 
            this.RemarkD.DataPropertyName = "Remark";
            this.RemarkD.HeaderText = "备注";
            this.RemarkD.MinimumWidth = 9;
            this.RemarkD.Name = "RemarkD";
            // 
            // SaleOutWarehouseDetailCode
            // 
            this.SaleOutWarehouseDetailCode.DataPropertyName = "SaleOutWarehouseDetailCode";
            this.SaleOutWarehouseDetailCode.HeaderText = "销售出库明细单号";
            this.SaleOutWarehouseDetailCode.MinimumWidth = 9;
            this.SaleOutWarehouseDetailCode.Name = "SaleOutWarehouseDetailCode";
            this.SaleOutWarehouseDetailCode.ReadOnly = true;
            // 
            // MakeUser
            // 
            this.MakeUser.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.MakeUser.FormattingEnabled = true;
            this.MakeUser.Location = new System.Drawing.Point(100, 81);
            this.MakeUser.Name = "MakeUser";
            this.MakeUser.Size = new System.Drawing.Size(130, 25);
            this.MakeUser.TabIndex = 24;
            // 
            // Frmw_SaleOutWarehouse
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 566);
            this.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.Name = "Frmw_SaleOutWarehouse";
            this.Text = "Frmw_SaleOutWarehouse";
            this.palTools.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabDataEdit.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.flowLayoutPanelTools.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ReviewUserID)).EndInit();
            this.tabControl2.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewDetail)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private TextBox SaleCode;
        private Label lblSaleCode;
        private TextBox SaleOutWarehouseCode;
        private Label lblSaleOutWarehouseCode;
        private TextBox CustomerCode;
        private Label lblCustomerCode;
        private TextBox CustomerName;
        private Label lblCustomerName;
        private DateTimePicker InvicesDate;
        private Label lblInvicesDate;
        private TextBox InvicesFile;
        private Label lblInvicesFile;
        private Label lblMakeUserID;
        private NumericUpDown ReviewUserID;
        private Label lblReviewUserID;
        private TextBox Status;
        private Label lblStatus;
        private TextBox Remark;
        private Label lblRemark;
        private TabControl tabControl2;
        private TabPage tabPage1;
        private DataGridView dataGridViewDetail;
        private DataGridViewTextBoxColumn ID;
        private DataGridViewTextBoxColumn SaleCodeD;
        private DataGridViewTextBoxColumn SaleDetailCode;
        private DataGridViewTextBoxColumn SaleOutWarehouseCodeD;
        private DataGridViewComboBoxColumn WarehouseName;
        private DataGridViewTextBoxColumn WarehouseCode;
        private DataGridViewTextBoxColumn GoodsCode;
        private DataGridViewTextBoxColumn GoodsName;
        private DataGridViewTextBoxColumn GoodsSpec;
        private DataGridViewTextBoxColumn GoodsUnit;
        private DataGridViewTextBoxColumn Number;
        private DataGridViewTextBoxColumn RemarkD;
        private DataGridViewTextBoxColumn SaleOutWarehouseDetailCode;
        private ComboBox MakeUser;
    }
}