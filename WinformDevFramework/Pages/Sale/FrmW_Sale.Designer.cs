namespace WinformDevFramework
{
    partial class Frmw_Sale
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblCustomerCode = new System.Windows.Forms.Label();
            this.CustomerCode = new System.Windows.Forms.TextBox();
            this.lblInvoicesDate = new System.Windows.Forms.Label();
            this.InvoicesDate = new System.Windows.Forms.DateTimePicker();
            this.lblMakeUserID = new System.Windows.Forms.Label();
            this.lblDeliveryDate = new System.Windows.Forms.Label();
            this.DeliveryDate = new System.Windows.Forms.DateTimePicker();
            this.lblSaleCode = new System.Windows.Forms.Label();
            this.SaleCode = new System.Windows.Forms.TextBox();
            this.lblInvoicesFile = new System.Windows.Forms.Label();
            this.InvoicesFile = new System.Windows.Forms.TextBox();
            this.lblRemark = new System.Windows.Forms.Label();
            this.Remark = new System.Windows.Forms.TextBox();
            this.lblTotalPrice = new System.Windows.Forms.Label();
            this.TotalPrice = new System.Windows.Forms.TextBox();
            this.lblTotalPrice1 = new System.Windows.Forms.Label();
            this.TotalPrice1 = new System.Windows.Forms.TextBox();
            this.lblTotalPrice2 = new System.Windows.Forms.Label();
            this.TotalPrice2 = new System.Windows.Forms.TextBox();
            this.lblCustomerName = new System.Windows.Forms.Label();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.dataGridViewDetail = new System.Windows.Forms.DataGridView();
            this.GoodsName = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.GoodsCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GoodsSpec = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GoodsUnit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Number = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UnitPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TotalPriceD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TaxRate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DiscountPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TaxUnitPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TaxPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TotalTaxPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RemarkD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SaleCodeD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SaleDetailCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CustomerName = new System.Windows.Forms.ComboBox();
            this.MakeUser = new System.Windows.Forms.ComboBox();
            this.palTools.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabDataEdit.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.flowLayoutPanelTools.SuspendLayout();
            this.tabControl2.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewDetail)).BeginInit();
            this.SuspendLayout();
            // 
            // palTools
            // 
            this.palTools.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            // 
            // tabControl1
            // 
            this.tabControl1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            // 
            // tabList
            // 
            this.tabList.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.tabList.Padding = new System.Windows.Forms.Padding(6, 5, 6, 5);
            // 
            // tabDataEdit
            // 
            this.tabDataEdit.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.tabDataEdit.Padding = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.tabDataEdit.Size = new System.Drawing.Size(423, 214);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.MakeUser);
            this.groupBox1.Controls.Add(this.CustomerName);
            this.groupBox1.Controls.Add(this.tabControl2);
            this.groupBox1.Controls.Add(this.lblCustomerName);
            this.groupBox1.Controls.Add(this.TotalPrice2);
            this.groupBox1.Controls.Add(this.lblTotalPrice2);
            this.groupBox1.Controls.Add(this.TotalPrice1);
            this.groupBox1.Controls.Add(this.lblTotalPrice1);
            this.groupBox1.Controls.Add(this.TotalPrice);
            this.groupBox1.Controls.Add(this.lblTotalPrice);
            this.groupBox1.Controls.Add(this.Remark);
            this.groupBox1.Controls.Add(this.lblRemark);
            this.groupBox1.Controls.Add(this.InvoicesFile);
            this.groupBox1.Controls.Add(this.lblInvoicesFile);
            this.groupBox1.Controls.Add(this.SaleCode);
            this.groupBox1.Controls.Add(this.lblSaleCode);
            this.groupBox1.Controls.Add(this.DeliveryDate);
            this.groupBox1.Controls.Add(this.lblDeliveryDate);
            this.groupBox1.Controls.Add(this.lblMakeUserID);
            this.groupBox1.Controls.Add(this.InvoicesDate);
            this.groupBox1.Controls.Add(this.lblInvoicesDate);
            this.groupBox1.Controls.Add(this.CustomerCode);
            this.groupBox1.Controls.Add(this.lblCustomerCode);
            this.groupBox1.Location = new System.Drawing.Point(6, 5);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox1.Size = new System.Drawing.Size(411, 204);
            this.groupBox1.Controls.SetChildIndex(this.lblCustomerCode, 0);
            this.groupBox1.Controls.SetChildIndex(this.CustomerCode, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblInvoicesDate, 0);
            this.groupBox1.Controls.SetChildIndex(this.InvoicesDate, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblMakeUserID, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblDeliveryDate, 0);
            this.groupBox1.Controls.SetChildIndex(this.DeliveryDate, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblSaleCode, 0);
            this.groupBox1.Controls.SetChildIndex(this.SaleCode, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblInvoicesFile, 0);
            this.groupBox1.Controls.SetChildIndex(this.InvoicesFile, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblRemark, 0);
            this.groupBox1.Controls.SetChildIndex(this.Remark, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblTotalPrice, 0);
            this.groupBox1.Controls.SetChildIndex(this.TotalPrice, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblTotalPrice1, 0);
            this.groupBox1.Controls.SetChildIndex(this.TotalPrice1, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblTotalPrice2, 0);
            this.groupBox1.Controls.SetChildIndex(this.TotalPrice2, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblCustomerName, 0);
            this.groupBox1.Controls.SetChildIndex(this.txtID, 0);
            this.groupBox1.Controls.SetChildIndex(this.tabControl2, 0);
            this.groupBox1.Controls.SetChildIndex(this.CustomerName, 0);
            this.groupBox1.Controls.SetChildIndex(this.MakeUser, 0);
            // 
            // flowLayoutPanelTools
            // 
            this.flowLayoutPanelTools.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            // 
            // txtID
            // 
            this.txtID.Location = new System.Drawing.Point(199, 31);
            this.txtID.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.txtID.Size = new System.Drawing.Size(56, 23);
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(2, 2);
            this.btnAdd.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            // 
            // btnEdit
            // 
            this.btnEdit.Location = new System.Drawing.Point(61, 2);
            this.btnEdit.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(120, 2);
            this.btnSave.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            // 
            // btnCanel
            // 
            this.btnCanel.Location = new System.Drawing.Point(179, 2);
            this.btnCanel.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            // 
            // btnDel
            // 
            this.btnDel.Location = new System.Drawing.Point(238, 2);
            this.btnDel.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            // 
            // btnResetPW
            // 
            this.btnResetPW.Location = new System.Drawing.Point(297, 2);
            this.btnResetPW.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnResetPW.Size = new System.Drawing.Size(55, 25);
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(415, 2);
            this.btnClose.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(356, 2);
            this.btnSearch.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            // 
            // lblCustomerCode
            // 
            this.lblCustomerCode.Location = new System.Drawing.Point(450, 12);
            this.lblCustomerCode.Name = "lblCustomerCode";
            this.lblCustomerCode.Size = new System.Drawing.Size(85, 23);
            this.lblCustomerCode.TabIndex = 2;
            this.lblCustomerCode.Text = "客户编码";
            this.lblCustomerCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // CustomerCode
            // 
            this.CustomerCode.Location = new System.Drawing.Point(540, 12);
            this.CustomerCode.Name = "CustomerCode";
            this.CustomerCode.Size = new System.Drawing.Size(130, 23);
            this.CustomerCode.TabIndex = 1;
            // 
            // lblInvoicesDate
            // 
            this.lblInvoicesDate.Location = new System.Drawing.Point(10, 45);
            this.lblInvoicesDate.Name = "lblInvoicesDate";
            this.lblInvoicesDate.Size = new System.Drawing.Size(85, 23);
            this.lblInvoicesDate.TabIndex = 4;
            this.lblInvoicesDate.Text = "单据日期";
            this.lblInvoicesDate.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // InvoicesDate
            // 
            this.InvoicesDate.Location = new System.Drawing.Point(100, 45);
            this.InvoicesDate.Name = "InvoicesDate";
            this.InvoicesDate.Size = new System.Drawing.Size(130, 23);
            this.InvoicesDate.TabIndex = 3;
            // 
            // lblMakeUserID
            // 
            this.lblMakeUserID.Location = new System.Drawing.Point(450, 45);
            this.lblMakeUserID.Name = "lblMakeUserID";
            this.lblMakeUserID.Size = new System.Drawing.Size(85, 23);
            this.lblMakeUserID.TabIndex = 6;
            this.lblMakeUserID.Text = "制单人";
            this.lblMakeUserID.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblDeliveryDate
            // 
            this.lblDeliveryDate.Location = new System.Drawing.Point(230, 45);
            this.lblDeliveryDate.Name = "lblDeliveryDate";
            this.lblDeliveryDate.Size = new System.Drawing.Size(85, 23);
            this.lblDeliveryDate.TabIndex = 8;
            this.lblDeliveryDate.Text = "交货日期";
            this.lblDeliveryDate.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DeliveryDate
            // 
            this.DeliveryDate.Location = new System.Drawing.Point(320, 45);
            this.DeliveryDate.Name = "DeliveryDate";
            this.DeliveryDate.Size = new System.Drawing.Size(130, 23);
            this.DeliveryDate.TabIndex = 7;
            // 
            // lblSaleCode
            // 
            this.lblSaleCode.Location = new System.Drawing.Point(10, 12);
            this.lblSaleCode.Name = "lblSaleCode";
            this.lblSaleCode.Size = new System.Drawing.Size(85, 23);
            this.lblSaleCode.TabIndex = 10;
            this.lblSaleCode.Text = "单据编号";
            this.lblSaleCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // SaleCode
            // 
            this.SaleCode.Location = new System.Drawing.Point(100, 12);
            this.SaleCode.Name = "SaleCode";
            this.SaleCode.PlaceholderText = "为空自动生成";
            this.SaleCode.Size = new System.Drawing.Size(130, 23);
            this.SaleCode.TabIndex = 9;
            // 
            // lblInvoicesFile
            // 
            this.lblInvoicesFile.Location = new System.Drawing.Point(10, 112);
            this.lblInvoicesFile.Name = "lblInvoicesFile";
            this.lblInvoicesFile.Size = new System.Drawing.Size(85, 23);
            this.lblInvoicesFile.TabIndex = 12;
            this.lblInvoicesFile.Text = "单据附件";
            this.lblInvoicesFile.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // InvoicesFile
            // 
            this.InvoicesFile.Location = new System.Drawing.Point(100, 112);
            this.InvoicesFile.Name = "InvoicesFile";
            this.InvoicesFile.Size = new System.Drawing.Size(130, 23);
            this.InvoicesFile.TabIndex = 11;
            // 
            // lblRemark
            // 
            this.lblRemark.Location = new System.Drawing.Point(230, 112);
            this.lblRemark.Name = "lblRemark";
            this.lblRemark.Size = new System.Drawing.Size(85, 23);
            this.lblRemark.TabIndex = 14;
            this.lblRemark.Text = "备注";
            this.lblRemark.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Remark
            // 
            this.Remark.Location = new System.Drawing.Point(320, 112);
            this.Remark.Name = "Remark";
            this.Remark.Size = new System.Drawing.Size(130, 23);
            this.Remark.TabIndex = 13;
            // 
            // lblTotalPrice
            // 
            this.lblTotalPrice.Location = new System.Drawing.Point(10, 78);
            this.lblTotalPrice.Name = "lblTotalPrice";
            this.lblTotalPrice.Size = new System.Drawing.Size(85, 23);
            this.lblTotalPrice.TabIndex = 16;
            this.lblTotalPrice.Text = "总金额";
            this.lblTotalPrice.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TotalPrice
            // 
            this.TotalPrice.Location = new System.Drawing.Point(100, 78);
            this.TotalPrice.Name = "TotalPrice";
            this.TotalPrice.Size = new System.Drawing.Size(130, 23);
            this.TotalPrice.TabIndex = 15;
            // 
            // lblTotalPrice1
            // 
            this.lblTotalPrice1.Location = new System.Drawing.Point(230, 78);
            this.lblTotalPrice1.Name = "lblTotalPrice1";
            this.lblTotalPrice1.Size = new System.Drawing.Size(85, 23);
            this.lblTotalPrice1.TabIndex = 18;
            this.lblTotalPrice1.Text = "已结金额";
            this.lblTotalPrice1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TotalPrice1
            // 
            this.TotalPrice1.Location = new System.Drawing.Point(320, 78);
            this.TotalPrice1.Name = "TotalPrice1";
            this.TotalPrice1.Size = new System.Drawing.Size(130, 23);
            this.TotalPrice1.TabIndex = 17;
            // 
            // lblTotalPrice2
            // 
            this.lblTotalPrice2.Location = new System.Drawing.Point(450, 78);
            this.lblTotalPrice2.Name = "lblTotalPrice2";
            this.lblTotalPrice2.Size = new System.Drawing.Size(85, 23);
            this.lblTotalPrice2.TabIndex = 20;
            this.lblTotalPrice2.Text = "未结金额";
            this.lblTotalPrice2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TotalPrice2
            // 
            this.TotalPrice2.Location = new System.Drawing.Point(540, 78);
            this.TotalPrice2.Name = "TotalPrice2";
            this.TotalPrice2.Size = new System.Drawing.Size(130, 23);
            this.TotalPrice2.TabIndex = 19;
            // 
            // lblCustomerName
            // 
            this.lblCustomerName.Location = new System.Drawing.Point(230, 12);
            this.lblCustomerName.Name = "lblCustomerName";
            this.lblCustomerName.Size = new System.Drawing.Size(85, 23);
            this.lblCustomerName.TabIndex = 22;
            this.lblCustomerName.Text = "客户名称";
            this.lblCustomerName.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabControl2
            // 
            this.tabControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl2.Controls.Add(this.tabPage1);
            this.tabControl2.Location = new System.Drawing.Point(3, 141);
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(404, 54);
            this.tabControl2.TabIndex = 23;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.dataGridViewDetail);
            this.tabPage1.Location = new System.Drawing.Point(4, 26);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.tabPage1.Size = new System.Drawing.Size(396, 24);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "销售明细";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // dataGridViewDetail
            // 
            this.dataGridViewDetail.AllowUserToOrderColumns = true;
            this.dataGridViewDetail.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGridViewDetail.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridViewDetail.ColumnHeadersHeight = 40;
            this.dataGridViewDetail.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.GoodsName,
            this.GoodsCode,
            this.GoodsSpec,
            this.GoodsUnit,
            this.Number,
            this.UnitPrice,
            this.TotalPriceD,
            this.TaxRate,
            this.DiscountPrice,
            this.TaxUnitPrice,
            this.TaxPrice,
            this.TotalTaxPrice,
            this.RemarkD,
            this.SaleCodeD,
            this.SaleDetailCode});
            this.dataGridViewDetail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewDetail.EnableHeadersVisualStyles = false;
            this.dataGridViewDetail.GridColor = System.Drawing.SystemColors.ControlLight;
            this.dataGridViewDetail.Location = new System.Drawing.Point(3, 3);
            this.dataGridViewDetail.Name = "dataGridViewDetail";
            this.dataGridViewDetail.RowHeadersVisible = false;
            this.dataGridViewDetail.RowHeadersWidth = 72;
            this.dataGridViewDetail.RowTemplate.Height = 25;
            this.dataGridViewDetail.Size = new System.Drawing.Size(390, 18);
            this.dataGridViewDetail.TabIndex = 0;
            // 
            // GoodsName
            // 
            this.GoodsName.HeaderText = "商品";
            this.GoodsName.MinimumWidth = 9;
            this.GoodsName.Name = "GoodsName";
            this.GoodsName.Width = 39;
            // 
            // GoodsCode
            // 
            this.GoodsCode.DataPropertyName = "GoodsCode";
            this.GoodsCode.HeaderText = "商品编码";
            this.GoodsCode.MinimumWidth = 9;
            this.GoodsCode.Name = "GoodsCode";
            this.GoodsCode.ReadOnly = true;
            this.GoodsCode.Width = 64;
            // 
            // GoodsSpec
            // 
            this.GoodsSpec.DataPropertyName = "GoodsSpec";
            this.GoodsSpec.HeaderText = "商品规格";
            this.GoodsSpec.MinimumWidth = 9;
            this.GoodsSpec.Name = "GoodsSpec";
            this.GoodsSpec.ReadOnly = true;
            this.GoodsSpec.Width = 64;
            // 
            // GoodsUnit
            // 
            this.GoodsUnit.DataPropertyName = "GoodsUnit";
            this.GoodsUnit.HeaderText = "计量单位";
            this.GoodsUnit.MinimumWidth = 9;
            this.GoodsUnit.Name = "GoodsUnit";
            this.GoodsUnit.ReadOnly = true;
            this.GoodsUnit.Width = 64;
            // 
            // Number
            // 
            this.Number.DataPropertyName = "Number";
            this.Number.HeaderText = "数量";
            this.Number.MinimumWidth = 9;
            this.Number.Name = "Number";
            this.Number.Width = 53;
            // 
            // UnitPrice
            // 
            this.UnitPrice.DataPropertyName = "UnitPrice";
            this.UnitPrice.HeaderText = "单价";
            this.UnitPrice.MinimumWidth = 9;
            this.UnitPrice.Name = "UnitPrice";
            this.UnitPrice.Width = 53;
            // 
            // TotalPriceD
            // 
            this.TotalPriceD.DataPropertyName = "TotalPrice";
            this.TotalPriceD.HeaderText = "金额";
            this.TotalPriceD.MinimumWidth = 9;
            this.TotalPriceD.Name = "TotalPriceD";
            this.TotalPriceD.ReadOnly = true;
            this.TotalPriceD.Width = 53;
            // 
            // TaxRate
            // 
            this.TaxRate.DataPropertyName = "TaxRate";
            this.TaxRate.HeaderText = "税率";
            this.TaxRate.MinimumWidth = 9;
            this.TaxRate.Name = "TaxRate";
            this.TaxRate.Width = 53;
            // 
            // DiscountPrice
            // 
            this.DiscountPrice.DataPropertyName = "DiscountPrice";
            this.DiscountPrice.HeaderText = "折扣金额";
            this.DiscountPrice.MinimumWidth = 9;
            this.DiscountPrice.Name = "DiscountPrice";
            this.DiscountPrice.Width = 64;
            // 
            // TaxUnitPrice
            // 
            this.TaxUnitPrice.DataPropertyName = "TaxUnitPrice";
            this.TaxUnitPrice.HeaderText = "含税单价";
            this.TaxUnitPrice.MinimumWidth = 9;
            this.TaxUnitPrice.Name = "TaxUnitPrice";
            this.TaxUnitPrice.ReadOnly = true;
            this.TaxUnitPrice.Width = 64;
            // 
            // TaxPrice
            // 
            this.TaxPrice.DataPropertyName = "TaxPrice";
            this.TaxPrice.HeaderText = "税额";
            this.TaxPrice.MinimumWidth = 9;
            this.TaxPrice.Name = "TaxPrice";
            this.TaxPrice.ReadOnly = true;
            this.TaxPrice.Width = 53;
            // 
            // TotalTaxPrice
            // 
            this.TotalTaxPrice.DataPropertyName = "TotalTaxPrice";
            this.TotalTaxPrice.HeaderText = "含税总价";
            this.TotalTaxPrice.MinimumWidth = 9;
            this.TotalTaxPrice.Name = "TotalTaxPrice";
            this.TotalTaxPrice.ReadOnly = true;
            this.TotalTaxPrice.Width = 64;
            // 
            // RemarkD
            // 
            this.RemarkD.DataPropertyName = "Remark";
            this.RemarkD.HeaderText = "备注";
            this.RemarkD.MinimumWidth = 9;
            this.RemarkD.Name = "RemarkD";
            this.RemarkD.Width = 53;
            // 
            // SaleCodeD
            // 
            this.SaleCodeD.DataPropertyName = "SaleCode";
            this.SaleCodeD.HeaderText = "销售单号";
            this.SaleCodeD.MinimumWidth = 9;
            this.SaleCodeD.Name = "SaleCodeD";
            this.SaleCodeD.ReadOnly = true;
            this.SaleCodeD.Width = 64;
            // 
            // SaleDetailCode
            // 
            this.SaleDetailCode.DataPropertyName = "SaleDetailCode";
            this.SaleDetailCode.HeaderText = "明细单号";
            this.SaleDetailCode.MinimumWidth = 9;
            this.SaleDetailCode.Name = "SaleDetailCode";
            this.SaleDetailCode.ReadOnly = true;
            this.SaleDetailCode.Width = 64;
            // 
            // CustomerName
            // 
            this.CustomerName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CustomerName.FormattingEnabled = true;
            this.CustomerName.Location = new System.Drawing.Point(320, 10);
            this.CustomerName.Name = "CustomerName";
            this.CustomerName.Size = new System.Drawing.Size(130, 25);
            this.CustomerName.TabIndex = 24;
            // 
            // MakeUser
            // 
            this.MakeUser.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.MakeUser.FormattingEnabled = true;
            this.MakeUser.Location = new System.Drawing.Point(540, 44);
            this.MakeUser.Name = "MakeUser";
            this.MakeUser.Size = new System.Drawing.Size(130, 25);
            this.MakeUser.TabIndex = 25;
            // 
            // Frmw_Sale
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.Name = "Frmw_Sale";
            this.Text = "Frmw_Sale";
            this.palTools.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabDataEdit.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.flowLayoutPanelTools.ResumeLayout(false);
            this.tabControl2.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewDetail)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private TextBox CustomerCode;
        private Label lblCustomerCode;
        private DateTimePicker InvoicesDate;
        private Label lblInvoicesDate;
        private Label lblMakeUserID;
        private DateTimePicker DeliveryDate;
        private Label lblDeliveryDate;
        private TextBox SaleCode;
        private Label lblSaleCode;
        private TextBox InvoicesFile;
        private Label lblInvoicesFile;
        private TextBox Remark;
        private Label lblRemark;
        private TextBox TotalPrice;
        private Label lblTotalPrice;
        private TextBox TotalPrice1;
        private Label lblTotalPrice1;
        private TextBox TotalPrice2;
        private Label lblTotalPrice2;
        private Label lblCustomerName;
        private TabControl tabControl2;
        private TabPage tabPage1;
        private DataGridView dataGridViewDetail;
        private ComboBox CustomerName;
        private DataGridViewComboBoxColumn GoodsName;
        private DataGridViewTextBoxColumn GoodsCode;
        private DataGridViewTextBoxColumn GoodsSpec;
        private DataGridViewTextBoxColumn GoodsUnit;
        private DataGridViewTextBoxColumn Number;
        private DataGridViewTextBoxColumn UnitPrice;
        private DataGridViewTextBoxColumn TotalPriceD;
        private DataGridViewTextBoxColumn TaxRate;
        private DataGridViewTextBoxColumn DiscountPrice;
        private DataGridViewTextBoxColumn TaxUnitPrice;
        private DataGridViewTextBoxColumn TaxPrice;
        private DataGridViewTextBoxColumn TotalTaxPrice;
        private DataGridViewTextBoxColumn RemarkD;
        private DataGridViewTextBoxColumn SaleCodeD;
        private DataGridViewTextBoxColumn SaleDetailCode;
        private ComboBox MakeUser;
    }
}