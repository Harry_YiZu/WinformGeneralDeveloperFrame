namespace WinformDevFramework
{
    partial class Frmw_BuyReturn
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frmw_BuyReturn));
            this.lblBuyReturnCode = new System.Windows.Forms.Label();
            this.BuyReturnCode = new System.Windows.Forms.TextBox();
            this.lblBuyCode = new System.Windows.Forms.Label();
            this.lblSupplierCode = new System.Windows.Forms.Label();
            this.SupplierName = new System.Windows.Forms.TextBox();
            this.lblInvoicesDate = new System.Windows.Forms.Label();
            this.InvoicesDate = new System.Windows.Forms.DateTimePicker();
            this.lblMakeUserName = new System.Windows.Forms.Label();
            this.MakeUserName = new System.Windows.Forms.TextBox();
            this.lblReviewUserName = new System.Windows.Forms.Label();
            this.ReviewUserName = new System.Windows.Forms.TextBox();
            this.lblInvoicesImage = new System.Windows.Forms.Label();
            this.lblRemark = new System.Windows.Forms.Label();
            this.BuyCode = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.InvoicesImage = new System.Windows.Forms.PictureBox();
            this.Remark = new System.Windows.Forms.RichTextBox();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.dataGridViewDetail = new System.Windows.Forms.DataGridView();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BuyReturnCodeD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BuyReturnDetailCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BuyCodeD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GoodsCode = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.GoodsName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GoodsSpec = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GoodsUnit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ReturnNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UnitPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DiscountPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TotalPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TaxRate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TaxUnitPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TaxPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TotalTaxPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RemarkD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label1 = new System.Windows.Forms.Label();
            this.ToltalPrice = new System.Windows.Forms.Label();
            this.Status = new System.Windows.Forms.Label();
            this.SupplierCode = new System.Windows.Forms.Label();
            this.palTools.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabDataEdit.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.flowLayoutPanelTools.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.InvoicesImage)).BeginInit();
            this.tabControl2.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewDetail)).BeginInit();
            this.SuspendLayout();
            // 
            // palTools
            // 
            this.palTools.Size = new System.Drawing.Size(935, 31);
            // 
            // tabControl1
            // 
            this.tabControl1.Size = new System.Drawing.Size(935, 419);
            // 
            // tabList
            // 
            this.tabList.Size = new System.Drawing.Size(927, 389);
            // 
            // tabDataEdit
            // 
            this.tabDataEdit.Size = new System.Drawing.Size(927, 389);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.SupplierCode);
            this.groupBox1.Controls.Add(this.Status);
            this.groupBox1.Controls.Add(this.ToltalPrice);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.tabControl2);
            this.groupBox1.Controls.Add(this.Remark);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.InvoicesImage);
            this.groupBox1.Controls.Add(this.BuyCode);
            this.groupBox1.Controls.Add(this.lblRemark);
            this.groupBox1.Controls.Add(this.lblInvoicesImage);
            this.groupBox1.Controls.Add(this.ReviewUserName);
            this.groupBox1.Controls.Add(this.lblReviewUserName);
            this.groupBox1.Controls.Add(this.MakeUserName);
            this.groupBox1.Controls.Add(this.lblMakeUserName);
            this.groupBox1.Controls.Add(this.InvoicesDate);
            this.groupBox1.Controls.Add(this.lblInvoicesDate);
            this.groupBox1.Controls.Add(this.SupplierName);
            this.groupBox1.Controls.Add(this.lblSupplierCode);
            this.groupBox1.Controls.Add(this.lblBuyCode);
            this.groupBox1.Controls.Add(this.BuyReturnCode);
            this.groupBox1.Controls.Add(this.lblBuyReturnCode);
            this.groupBox1.Size = new System.Drawing.Size(921, 383);
            this.groupBox1.Controls.SetChildIndex(this.lblBuyReturnCode, 0);
            this.groupBox1.Controls.SetChildIndex(this.BuyReturnCode, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblBuyCode, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblSupplierCode, 0);
            this.groupBox1.Controls.SetChildIndex(this.SupplierName, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblInvoicesDate, 0);
            this.groupBox1.Controls.SetChildIndex(this.InvoicesDate, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblMakeUserName, 0);
            this.groupBox1.Controls.SetChildIndex(this.MakeUserName, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblReviewUserName, 0);
            this.groupBox1.Controls.SetChildIndex(this.ReviewUserName, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblInvoicesImage, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblRemark, 0);
            this.groupBox1.Controls.SetChildIndex(this.txtID, 0);
            this.groupBox1.Controls.SetChildIndex(this.BuyCode, 0);
            this.groupBox1.Controls.SetChildIndex(this.InvoicesImage, 0);
            this.groupBox1.Controls.SetChildIndex(this.button1, 0);
            this.groupBox1.Controls.SetChildIndex(this.Remark, 0);
            this.groupBox1.Controls.SetChildIndex(this.tabControl2, 0);
            this.groupBox1.Controls.SetChildIndex(this.label1, 0);
            this.groupBox1.Controls.SetChildIndex(this.ToltalPrice, 0);
            this.groupBox1.Controls.SetChildIndex(this.Status, 0);
            this.groupBox1.Controls.SetChildIndex(this.SupplierCode, 0);
            // 
            // flowLayoutPanelTools
            // 
            this.flowLayoutPanelTools.Size = new System.Drawing.Size(931, 27);
            // 
            // lblBuyReturnCode
            // 
            this.lblBuyReturnCode.Location = new System.Drawing.Point(10, 10);
            this.lblBuyReturnCode.Name = "lblBuyReturnCode";
            this.lblBuyReturnCode.Size = new System.Drawing.Size(85, 23);
            this.lblBuyReturnCode.TabIndex = 2;
            this.lblBuyReturnCode.Text = "退货单号";
            this.lblBuyReturnCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BuyReturnCode
            // 
            this.BuyReturnCode.Location = new System.Drawing.Point(100, 10);
            this.BuyReturnCode.Name = "BuyReturnCode";
            this.BuyReturnCode.PlaceholderText = "为空自动生成";
            this.BuyReturnCode.Size = new System.Drawing.Size(130, 23);
            this.BuyReturnCode.TabIndex = 1;
            // 
            // lblBuyCode
            // 
            this.lblBuyCode.Location = new System.Drawing.Point(230, 10);
            this.lblBuyCode.Name = "lblBuyCode";
            this.lblBuyCode.Size = new System.Drawing.Size(85, 23);
            this.lblBuyCode.TabIndex = 4;
            this.lblBuyCode.Text = "关联的采购单号";
            this.lblBuyCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblSupplierCode
            // 
            this.lblSupplierCode.Location = new System.Drawing.Point(450, 10);
            this.lblSupplierCode.Name = "lblSupplierCode";
            this.lblSupplierCode.Size = new System.Drawing.Size(85, 23);
            this.lblSupplierCode.TabIndex = 6;
            this.lblSupplierCode.Text = "供应商";
            this.lblSupplierCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // SupplierName
            // 
            this.SupplierName.Location = new System.Drawing.Point(540, 10);
            this.SupplierName.Name = "SupplierName";
            this.SupplierName.Size = new System.Drawing.Size(130, 23);
            this.SupplierName.TabIndex = 5;
            // 
            // lblInvoicesDate
            // 
            this.lblInvoicesDate.Location = new System.Drawing.Point(10, 45);
            this.lblInvoicesDate.Name = "lblInvoicesDate";
            this.lblInvoicesDate.Size = new System.Drawing.Size(85, 23);
            this.lblInvoicesDate.TabIndex = 8;
            this.lblInvoicesDate.Text = "单据日期";
            this.lblInvoicesDate.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // InvoicesDate
            // 
            this.InvoicesDate.Location = new System.Drawing.Point(100, 45);
            this.InvoicesDate.Name = "InvoicesDate";
            this.InvoicesDate.Size = new System.Drawing.Size(130, 23);
            this.InvoicesDate.TabIndex = 7;
            // 
            // lblMakeUserName
            // 
            this.lblMakeUserName.Location = new System.Drawing.Point(230, 45);
            this.lblMakeUserName.Name = "lblMakeUserName";
            this.lblMakeUserName.Size = new System.Drawing.Size(85, 23);
            this.lblMakeUserName.TabIndex = 10;
            this.lblMakeUserName.Text = "制单人";
            this.lblMakeUserName.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MakeUserName
            // 
            this.MakeUserName.Location = new System.Drawing.Point(320, 45);
            this.MakeUserName.Name = "MakeUserName";
            this.MakeUserName.Size = new System.Drawing.Size(130, 23);
            this.MakeUserName.TabIndex = 9;
            // 
            // lblReviewUserName
            // 
            this.lblReviewUserName.Location = new System.Drawing.Point(450, 45);
            this.lblReviewUserName.Name = "lblReviewUserName";
            this.lblReviewUserName.Size = new System.Drawing.Size(85, 23);
            this.lblReviewUserName.TabIndex = 12;
            this.lblReviewUserName.Text = "审核人";
            this.lblReviewUserName.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ReviewUserName
            // 
            this.ReviewUserName.Location = new System.Drawing.Point(540, 45);
            this.ReviewUserName.Name = "ReviewUserName";
            this.ReviewUserName.Size = new System.Drawing.Size(130, 23);
            this.ReviewUserName.TabIndex = 11;
            // 
            // lblInvoicesImage
            // 
            this.lblInvoicesImage.Location = new System.Drawing.Point(10, 80);
            this.lblInvoicesImage.Name = "lblInvoicesImage";
            this.lblInvoicesImage.Size = new System.Drawing.Size(85, 23);
            this.lblInvoicesImage.TabIndex = 14;
            this.lblInvoicesImage.Text = "单据附件";
            this.lblInvoicesImage.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblInvoicesImage.Visible = false;
            // 
            // lblRemark
            // 
            this.lblRemark.Location = new System.Drawing.Point(9, 80);
            this.lblRemark.Name = "lblRemark";
            this.lblRemark.Size = new System.Drawing.Size(85, 23);
            this.lblRemark.TabIndex = 18;
            this.lblRemark.Text = "备注";
            this.lblRemark.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BuyCode
            // 
            this.BuyCode.Location = new System.Drawing.Point(321, 10);
            this.BuyCode.Name = "BuyCode";
            this.BuyCode.Size = new System.Drawing.Size(130, 23);
            this.BuyCode.TabIndex = 20;
            this.BuyCode.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.BuyCode_KeyPress);
            // 
            // button1
            // 
            this.button1.Image = ((System.Drawing.Image)(resources.GetObject("button1.Image")));
            this.button1.Location = new System.Drawing.Point(197, 80);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(26, 23);
            this.button1.TabIndex = 53;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // InvoicesImage
            // 
            this.InvoicesImage.Location = new System.Drawing.Point(100, 80);
            this.InvoicesImage.Name = "InvoicesImage";
            this.InvoicesImage.Size = new System.Drawing.Size(91, 60);
            this.InvoicesImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.InvoicesImage.TabIndex = 52;
            this.InvoicesImage.TabStop = false;
            this.InvoicesImage.Visible = false;
            this.InvoicesImage.Click += new System.EventHandler(this.InvoicesImage_Click);
            this.InvoicesImage.DoubleClick += new System.EventHandler(this.InvoicesImage_DoubleClick);
            // 
            // Remark
            // 
            this.Remark.Location = new System.Drawing.Point(100, 80);
            this.Remark.Name = "Remark";
            this.Remark.Size = new System.Drawing.Size(348, 60);
            this.Remark.TabIndex = 54;
            this.Remark.Text = "";
            // 
            // tabControl2
            // 
            this.tabControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl2.Controls.Add(this.tabPage1);
            this.tabControl2.Location = new System.Drawing.Point(0, 146);
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(924, 240);
            this.tabControl2.TabIndex = 55;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.dataGridViewDetail);
            this.tabPage1.Location = new System.Drawing.Point(4, 26);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(916, 210);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "退货明细";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // dataGridViewDetail
            // 
            this.dataGridViewDetail.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGridViewDetail.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridViewDetail.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridViewDetail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewDetail.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ID,
            this.BuyReturnCodeD,
            this.BuyReturnDetailCode,
            this.BuyCodeD,
            this.GoodsCode,
            this.GoodsName,
            this.GoodsSpec,
            this.GoodsUnit,
            this.ReturnNumber,
            this.UnitPrice,
            this.DiscountPrice,
            this.TotalPrice,
            this.TaxRate,
            this.TaxUnitPrice,
            this.TaxPrice,
            this.TotalTaxPrice,
            this.RemarkD});
            this.dataGridViewDetail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewDetail.GridColor = System.Drawing.SystemColors.Control;
            this.dataGridViewDetail.Location = new System.Drawing.Point(3, 3);
            this.dataGridViewDetail.Name = "dataGridViewDetail";
            this.dataGridViewDetail.RowHeadersVisible = false;
            this.dataGridViewDetail.RowTemplate.Height = 25;
            this.dataGridViewDetail.Size = new System.Drawing.Size(910, 204);
            this.dataGridViewDetail.TabIndex = 0;
            this.dataGridViewDetail.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewDetail_CellEndEdit);
            // 
            // ID
            // 
            this.ID.DataPropertyName = "ID";
            this.ID.HeaderText = "ID";
            this.ID.Name = "ID";
            this.ID.ReadOnly = true;
            this.ID.Visible = false;
            this.ID.Width = 46;
            // 
            // BuyReturnCodeD
            // 
            this.BuyReturnCodeD.DataPropertyName = "BuyReturnCode";
            this.BuyReturnCodeD.HeaderText = "采购退货单号";
            this.BuyReturnCodeD.Name = "BuyReturnCodeD";
            this.BuyReturnCodeD.ReadOnly = true;
            this.BuyReturnCodeD.Visible = false;
            this.BuyReturnCodeD.Width = 105;
            // 
            // BuyReturnDetailCode
            // 
            this.BuyReturnDetailCode.DataPropertyName = "BuyReturnDetailCode";
            this.BuyReturnDetailCode.HeaderText = "采购退货明细单号";
            this.BuyReturnDetailCode.Name = "BuyReturnDetailCode";
            this.BuyReturnDetailCode.ReadOnly = true;
            this.BuyReturnDetailCode.Visible = false;
            this.BuyReturnDetailCode.Width = 129;
            // 
            // BuyCodeD
            // 
            this.BuyCodeD.DataPropertyName = "BuyCode";
            this.BuyCodeD.HeaderText = "采购单号";
            this.BuyCodeD.Name = "BuyCodeD";
            this.BuyCodeD.ReadOnly = true;
            this.BuyCodeD.Visible = false;
            this.BuyCodeD.Width = 81;
            // 
            // GoodsCode
            // 
            this.GoodsCode.DataPropertyName = "GoodsCode";
            this.GoodsCode.HeaderText = "商品编码";
            this.GoodsCode.Name = "GoodsCode";
            this.GoodsCode.ReadOnly = true;
            this.GoodsCode.Width = 62;
            // 
            // GoodsName
            // 
            this.GoodsName.DataPropertyName = "GoodsName";
            this.GoodsName.HeaderText = "商品名称";
            this.GoodsName.Name = "GoodsName";
            this.GoodsName.ReadOnly = true;
            this.GoodsName.Width = 81;
            // 
            // GoodsSpec
            // 
            this.GoodsSpec.DataPropertyName = "GoodsSpec";
            this.GoodsSpec.HeaderText = "商品规格";
            this.GoodsSpec.Name = "GoodsSpec";
            this.GoodsSpec.ReadOnly = true;
            this.GoodsSpec.Width = 81;
            // 
            // GoodsUnit
            // 
            this.GoodsUnit.DataPropertyName = "GoodsUnit";
            this.GoodsUnit.HeaderText = "计量单位";
            this.GoodsUnit.Name = "GoodsUnit";
            this.GoodsUnit.ReadOnly = true;
            this.GoodsUnit.Width = 81;
            // 
            // ReturnNumber
            // 
            this.ReturnNumber.DataPropertyName = "ReturnNumber";
            this.ReturnNumber.HeaderText = "退货数量";
            this.ReturnNumber.Name = "ReturnNumber";
            this.ReturnNumber.Width = 81;
            // 
            // UnitPrice
            // 
            this.UnitPrice.DataPropertyName = "UnitPrice";
            this.UnitPrice.HeaderText = "退货单价";
            this.UnitPrice.Name = "UnitPrice";
            this.UnitPrice.Width = 81;
            // 
            // DiscountPrice
            // 
            this.DiscountPrice.DataPropertyName = "DiscountPrice";
            this.DiscountPrice.HeaderText = "折扣价格";
            this.DiscountPrice.Name = "DiscountPrice";
            this.DiscountPrice.Width = 81;
            // 
            // TotalPrice
            // 
            this.TotalPrice.DataPropertyName = "TotalPrice";
            this.TotalPrice.HeaderText = "退货金额";
            this.TotalPrice.Name = "TotalPrice";
            this.TotalPrice.ReadOnly = true;
            this.TotalPrice.Width = 81;
            // 
            // TaxRate
            // 
            this.TaxRate.DataPropertyName = "TaxRate";
            this.TaxRate.HeaderText = "税率(%)";
            this.TaxRate.Name = "TaxRate";
            this.TaxRate.Width = 76;
            // 
            // TaxUnitPrice
            // 
            this.TaxUnitPrice.DataPropertyName = "TaxUnitPrice";
            this.TaxUnitPrice.HeaderText = "含税单价";
            this.TaxUnitPrice.Name = "TaxUnitPrice";
            this.TaxUnitPrice.ReadOnly = true;
            this.TaxUnitPrice.Width = 81;
            // 
            // TaxPrice
            // 
            this.TaxPrice.DataPropertyName = "TaxPrice";
            this.TaxPrice.HeaderText = "税额";
            this.TaxPrice.Name = "TaxPrice";
            this.TaxPrice.ReadOnly = true;
            this.TaxPrice.Width = 57;
            // 
            // TotalTaxPrice
            // 
            this.TotalTaxPrice.DataPropertyName = "TotalTaxPrice";
            this.TotalTaxPrice.HeaderText = "含税金额";
            this.TotalTaxPrice.Name = "TotalTaxPrice";
            this.TotalTaxPrice.ReadOnly = true;
            this.TotalTaxPrice.Width = 81;
            // 
            // RemarkD
            // 
            this.RemarkD.DataPropertyName = "Remark";
            this.RemarkD.HeaderText = "备注";
            this.RemarkD.Name = "RemarkD";
            this.RemarkD.Width = 57;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(705, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(32, 17);
            this.label1.TabIndex = 56;
            this.label1.Text = "总额";
            // 
            // ToltalPrice
            // 
            this.ToltalPrice.AutoSize = true;
            this.ToltalPrice.Location = new System.Drawing.Point(754, 13);
            this.ToltalPrice.Name = "ToltalPrice";
            this.ToltalPrice.Size = new System.Drawing.Size(32, 17);
            this.ToltalPrice.TabIndex = 57;
            this.ToltalPrice.Text = "0.00";
            // 
            // Status
            // 
            this.Status.AutoSize = true;
            this.Status.Location = new System.Drawing.Point(705, 50);
            this.Status.Name = "Status";
            this.Status.Size = new System.Drawing.Size(43, 17);
            this.Status.TabIndex = 58;
            this.Status.Text = "label2";
            this.Status.Visible = false;
            // 
            // SupplierCode
            // 
            this.SupplierCode.AutoSize = true;
            this.SupplierCode.Location = new System.Drawing.Point(706, 79);
            this.SupplierCode.Name = "SupplierCode";
            this.SupplierCode.Size = new System.Drawing.Size(43, 17);
            this.SupplierCode.TabIndex = 59;
            this.SupplierCode.Text = "label2";
            this.SupplierCode.Visible = false;
            // 
            // Frmw_BuyReturn
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(935, 450);
            this.Name = "Frmw_BuyReturn";
            this.Text = "Frmw_BuyReturn";
            this.palTools.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabDataEdit.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.flowLayoutPanelTools.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.InvoicesImage)).EndInit();
            this.tabControl2.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewDetail)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
            private TextBox BuyReturnCode;
            private Label lblBuyReturnCode;
            private Label lblBuyCode;
            private TextBox SupplierName;
            private Label lblSupplierCode;
            private DateTimePicker InvoicesDate;
            private Label lblInvoicesDate;
            private TextBox MakeUserName;
            private Label lblMakeUserName;
            private TextBox ReviewUserName;
            private Label lblReviewUserName;
            private Label lblInvoicesImage;
            private Label lblStatus;
            private Label lblRemark;
        private TextBox BuyCode;
        private Button button1;
        private PictureBox InvoicesImage;
        private RichTextBox Remark;
        private TabControl tabControl2;
        private TabPage tabPage1;
        private DataGridView dataGridViewDetail;
        private DataGridViewTextBoxColumn ID;
        private DataGridViewTextBoxColumn BuyReturnCodeD;
        private DataGridViewTextBoxColumn BuyReturnDetailCode;
        private DataGridViewTextBoxColumn BuyCodeD;
        private DataGridViewComboBoxColumn GoodsCode;
        private DataGridViewTextBoxColumn GoodsName;
        private DataGridViewTextBoxColumn GoodsSpec;
        private DataGridViewTextBoxColumn GoodsUnit;
        private DataGridViewTextBoxColumn ReturnNumber;
        private DataGridViewTextBoxColumn UnitPrice;
        private DataGridViewTextBoxColumn DiscountPrice;
        private DataGridViewTextBoxColumn TotalPrice;
        private DataGridViewTextBoxColumn TaxRate;
        private DataGridViewTextBoxColumn TaxUnitPrice;
        private DataGridViewTextBoxColumn TaxPrice;
        private DataGridViewTextBoxColumn TotalTaxPrice;
        private DataGridViewTextBoxColumn RemarkD;
        private Label ToltalPrice;
        private Label label1;
        private Label Status;
        private Label SupplierCode;
    }
}