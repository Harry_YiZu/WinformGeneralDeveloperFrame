namespace WinformDevFramework
{
    partial class Frmw_Buy
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frmw_Buy));
            this.lblBuyCode = new System.Windows.Forms.Label();
            this.BuyCode = new System.Windows.Forms.TextBox();
            this.lblSupplierName = new System.Windows.Forms.Label();
            this.lblInvoicesDate = new System.Windows.Forms.Label();
            this.InvoicesDate = new System.Windows.Forms.DateTimePicker();
            this.lblMakeUser = new System.Windows.Forms.Label();
            this.lblDeliveryDate = new System.Windows.Forms.Label();
            this.DeliveryDate = new System.Windows.Forms.DateTimePicker();
            this.lblInvoicesImage = new System.Windows.Forms.Label();
            this.lblRemark = new System.Windows.Forms.Label();
            this.SupplierName = new System.Windows.Forms.ComboBox();
            this.MakeUser = new System.Windows.Forms.ComboBox();
            this.TotalPayable = new System.Windows.Forms.LinkLabel();
            this.yijie = new System.Windows.Forms.LinkLabel();
            this.label18 = new System.Windows.Forms.Label();
            this.yijie1 = new System.Windows.Forms.Label();
            this.totalprice = new System.Windows.Forms.Label();
            this.weijie = new System.Windows.Forms.Label();
            this.InvoicesImage = new System.Windows.Forms.PictureBox();
            this.button1 = new System.Windows.Forms.Button();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.tabPageGouhuo = new System.Windows.Forms.TabPage();
            this.dataGridViewBuyDetail = new System.Windows.Forms.DataGridView();
            this.BuyID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BuyCodeD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BuyDetailCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GoodsName = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.GoodsCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GoodsUnit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GoodsSpec = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BuyNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UnitPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DiscountPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TotalPriceD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TaxRate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TaxUnitPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TaxPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TotalTaxPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RemarkD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItemDel = new System.Windows.Forms.ToolStripMenuItem();
            this.SettlementAccount = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.Remark = new System.Windows.Forms.RichTextBox();
            this.palTools.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabDataEdit.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.flowLayoutPanelTools.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.InvoicesImage)).BeginInit();
            this.tabControl2.SuspendLayout();
            this.tabPageGouhuo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewBuyDetail)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // palTools
            // 
            this.palTools.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.palTools.Size = new System.Drawing.Size(778, 31);
            // 
            // tabControl1
            // 
            this.tabControl1.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.tabControl1.Size = new System.Drawing.Size(778, 429);
            // 
            // tabList
            // 
            this.tabList.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.tabList.Padding = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.tabList.Size = new System.Drawing.Size(770, 399);
            // 
            // tabDataEdit
            // 
            this.tabDataEdit.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.tabDataEdit.Padding = new System.Windows.Forms.Padding(6, 5, 6, 5);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.Remark);
            this.groupBox1.Controls.Add(this.SettlementAccount);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.tabControl2);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.InvoicesImage);
            this.groupBox1.Controls.Add(this.weijie);
            this.groupBox1.Controls.Add(this.TotalPayable);
            this.groupBox1.Controls.Add(this.yijie);
            this.groupBox1.Controls.Add(this.label18);
            this.groupBox1.Controls.Add(this.yijie1);
            this.groupBox1.Controls.Add(this.totalprice);
            this.groupBox1.Controls.Add(this.MakeUser);
            this.groupBox1.Controls.Add(this.SupplierName);
            this.groupBox1.Controls.Add(this.lblRemark);
            this.groupBox1.Controls.Add(this.lblInvoicesImage);
            this.groupBox1.Controls.Add(this.DeliveryDate);
            this.groupBox1.Controls.Add(this.lblDeliveryDate);
            this.groupBox1.Controls.Add(this.lblMakeUser);
            this.groupBox1.Controls.Add(this.InvoicesDate);
            this.groupBox1.Controls.Add(this.lblInvoicesDate);
            this.groupBox1.Controls.Add(this.lblSupplierName);
            this.groupBox1.Controls.Add(this.BuyCode);
            this.groupBox1.Controls.Add(this.lblBuyCode);
            this.groupBox1.Location = new System.Drawing.Point(6, 5);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.groupBox1.Padding = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.groupBox1.Size = new System.Drawing.Size(780, 379);
            this.groupBox1.Controls.SetChildIndex(this.lblBuyCode, 0);
            this.groupBox1.Controls.SetChildIndex(this.BuyCode, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblSupplierName, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblInvoicesDate, 0);
            this.groupBox1.Controls.SetChildIndex(this.InvoicesDate, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblMakeUser, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblDeliveryDate, 0);
            this.groupBox1.Controls.SetChildIndex(this.DeliveryDate, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblInvoicesImage, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblRemark, 0);
            this.groupBox1.Controls.SetChildIndex(this.SupplierName, 0);
            this.groupBox1.Controls.SetChildIndex(this.MakeUser, 0);
            this.groupBox1.Controls.SetChildIndex(this.totalprice, 0);
            this.groupBox1.Controls.SetChildIndex(this.yijie1, 0);
            this.groupBox1.Controls.SetChildIndex(this.label18, 0);
            this.groupBox1.Controls.SetChildIndex(this.yijie, 0);
            this.groupBox1.Controls.SetChildIndex(this.TotalPayable, 0);
            this.groupBox1.Controls.SetChildIndex(this.weijie, 0);
            this.groupBox1.Controls.SetChildIndex(this.txtID, 0);
            this.groupBox1.Controls.SetChildIndex(this.InvoicesImage, 0);
            this.groupBox1.Controls.SetChildIndex(this.button1, 0);
            this.groupBox1.Controls.SetChildIndex(this.tabControl2, 0);
            this.groupBox1.Controls.SetChildIndex(this.label1, 0);
            this.groupBox1.Controls.SetChildIndex(this.SettlementAccount, 0);
            this.groupBox1.Controls.SetChildIndex(this.Remark, 0);
            // 
            // flowLayoutPanelTools
            // 
            this.flowLayoutPanelTools.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.flowLayoutPanelTools.Size = new System.Drawing.Size(774, 27);
            // 
            // txtID
            // 
            this.txtID.Location = new System.Drawing.Point(199, 31);
            this.txtID.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.txtID.Size = new System.Drawing.Size(56, 23);
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(2, 2);
            this.btnAdd.Margin = new System.Windows.Forms.Padding(2);
            this.btnAdd.Size = new System.Drawing.Size(53, 23);
            // 
            // btnEdit
            // 
            this.btnEdit.Location = new System.Drawing.Point(59, 2);
            this.btnEdit.Margin = new System.Windows.Forms.Padding(2);
            this.btnEdit.Size = new System.Drawing.Size(53, 23);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(116, 2);
            this.btnSave.Margin = new System.Windows.Forms.Padding(2);
            this.btnSave.Size = new System.Drawing.Size(53, 23);
            // 
            // btnCanel
            // 
            this.btnCanel.Location = new System.Drawing.Point(173, 2);
            this.btnCanel.Margin = new System.Windows.Forms.Padding(2);
            this.btnCanel.Size = new System.Drawing.Size(53, 23);
            // 
            // btnDel
            // 
            this.btnDel.Location = new System.Drawing.Point(230, 2);
            this.btnDel.Margin = new System.Windows.Forms.Padding(2);
            this.btnDel.Size = new System.Drawing.Size(53, 23);
            // 
            // btnResetPW
            // 
            this.btnResetPW.Location = new System.Drawing.Point(287, 2);
            this.btnResetPW.Margin = new System.Windows.Forms.Padding(2);
            this.btnResetPW.Size = new System.Drawing.Size(53, 25);
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(401, 2);
            this.btnClose.Margin = new System.Windows.Forms.Padding(2);
            this.btnClose.Size = new System.Drawing.Size(53, 23);
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(344, 2);
            this.btnSearch.Margin = new System.Windows.Forms.Padding(2);
            this.btnSearch.Size = new System.Drawing.Size(53, 23);
            // 
            // lblBuyCode
            // 
            this.lblBuyCode.Location = new System.Drawing.Point(10, 10);
            this.lblBuyCode.Name = "lblBuyCode";
            this.lblBuyCode.Size = new System.Drawing.Size(85, 23);
            this.lblBuyCode.TabIndex = 2;
            this.lblBuyCode.Text = "采购单号";
            this.lblBuyCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BuyCode
            // 
            this.BuyCode.Location = new System.Drawing.Point(100, 10);
            this.BuyCode.Name = "BuyCode";
            this.BuyCode.PlaceholderText = "为空自动生成";
            this.BuyCode.Size = new System.Drawing.Size(130, 23);
            this.BuyCode.TabIndex = 1;
            // 
            // lblSupplierName
            // 
            this.lblSupplierName.Location = new System.Drawing.Point(230, 10);
            this.lblSupplierName.Name = "lblSupplierName";
            this.lblSupplierName.Size = new System.Drawing.Size(85, 23);
            this.lblSupplierName.TabIndex = 4;
            this.lblSupplierName.Text = "供应商";
            this.lblSupplierName.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblInvoicesDate
            // 
            this.lblInvoicesDate.Location = new System.Drawing.Point(10, 45);
            this.lblInvoicesDate.Name = "lblInvoicesDate";
            this.lblInvoicesDate.Size = new System.Drawing.Size(85, 23);
            this.lblInvoicesDate.TabIndex = 6;
            this.lblInvoicesDate.Text = "单据日期";
            this.lblInvoicesDate.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // InvoicesDate
            // 
            this.InvoicesDate.Location = new System.Drawing.Point(100, 45);
            this.InvoicesDate.Name = "InvoicesDate";
            this.InvoicesDate.Size = new System.Drawing.Size(130, 23);
            this.InvoicesDate.TabIndex = 5;
            // 
            // lblMakeUser
            // 
            this.lblMakeUser.Location = new System.Drawing.Point(7, 84);
            this.lblMakeUser.Name = "lblMakeUser";
            this.lblMakeUser.Size = new System.Drawing.Size(85, 23);
            this.lblMakeUser.TabIndex = 8;
            this.lblMakeUser.Text = "制单人";
            this.lblMakeUser.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblDeliveryDate
            // 
            this.lblDeliveryDate.Location = new System.Drawing.Point(230, 45);
            this.lblDeliveryDate.Name = "lblDeliveryDate";
            this.lblDeliveryDate.Size = new System.Drawing.Size(85, 23);
            this.lblDeliveryDate.TabIndex = 10;
            this.lblDeliveryDate.Text = "交货日期";
            this.lblDeliveryDate.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DeliveryDate
            // 
            this.DeliveryDate.Location = new System.Drawing.Point(320, 45);
            this.DeliveryDate.Name = "DeliveryDate";
            this.DeliveryDate.Size = new System.Drawing.Size(130, 23);
            this.DeliveryDate.TabIndex = 9;
            // 
            // lblInvoicesImage
            // 
            this.lblInvoicesImage.Location = new System.Drawing.Point(230, 84);
            this.lblInvoicesImage.Name = "lblInvoicesImage";
            this.lblInvoicesImage.Size = new System.Drawing.Size(85, 23);
            this.lblInvoicesImage.TabIndex = 12;
            this.lblInvoicesImage.Text = "单据附件";
            this.lblInvoicesImage.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblInvoicesImage.Visible = false;
            // 
            // lblRemark
            // 
            this.lblRemark.Location = new System.Drawing.Point(4, 121);
            this.lblRemark.Name = "lblRemark";
            this.lblRemark.Size = new System.Drawing.Size(85, 23);
            this.lblRemark.TabIndex = 14;
            this.lblRemark.Text = "备注";
            this.lblRemark.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // SupplierName
            // 
            this.SupplierName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.SupplierName.FormattingEnabled = true;
            this.SupplierName.Location = new System.Drawing.Point(320, 8);
            this.SupplierName.Name = "SupplierName";
            this.SupplierName.Size = new System.Drawing.Size(130, 25);
            this.SupplierName.TabIndex = 15;
            // 
            // MakeUser
            // 
            this.MakeUser.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.MakeUser.FormattingEnabled = true;
            this.MakeUser.Location = new System.Drawing.Point(100, 82);
            this.MakeUser.Name = "MakeUser";
            this.MakeUser.Size = new System.Drawing.Size(127, 25);
            this.MakeUser.TabIndex = 16;
            // 
            // TotalPayable
            // 
            this.TotalPayable.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.TotalPayable.Location = new System.Drawing.Point(547, 10);
            this.TotalPayable.Name = "TotalPayable";
            this.TotalPayable.Size = new System.Drawing.Size(116, 17);
            this.TotalPayable.TabIndex = 48;
            this.TotalPayable.TabStop = true;
            this.TotalPayable.Text = "0.00";
            this.TotalPayable.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // yijie
            // 
            this.yijie.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.yijie.Location = new System.Drawing.Point(547, 48);
            this.yijie.Name = "yijie";
            this.yijie.Size = new System.Drawing.Size(115, 17);
            this.yijie.TabIndex = 47;
            this.yijie.TabStop = true;
            this.yijie.Text = "0.00";
            this.yijie.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label18
            // 
            this.label18.Location = new System.Drawing.Point(456, 82);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(85, 23);
            this.label18.TabIndex = 44;
            this.label18.Text = "未结金额";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // yijie1
            // 
            this.yijie1.Location = new System.Drawing.Point(456, 45);
            this.yijie1.Name = "yijie1";
            this.yijie1.Size = new System.Drawing.Size(85, 23);
            this.yijie1.TabIndex = 42;
            this.yijie1.Text = "已结总额";
            this.yijie1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // totalprice
            // 
            this.totalprice.Location = new System.Drawing.Point(456, 8);
            this.totalprice.Name = "totalprice";
            this.totalprice.Size = new System.Drawing.Size(85, 23);
            this.totalprice.TabIndex = 41;
            this.totalprice.Text = "总额";
            this.totalprice.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // weijie
            // 
            this.weijie.Location = new System.Drawing.Point(547, 81);
            this.weijie.Name = "weijie";
            this.weijie.Size = new System.Drawing.Size(115, 23);
            this.weijie.TabIndex = 49;
            this.weijie.Text = "0.00";
            this.weijie.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // InvoicesImage
            // 
            this.InvoicesImage.Location = new System.Drawing.Point(321, 84);
            this.InvoicesImage.Name = "InvoicesImage";
            this.InvoicesImage.Size = new System.Drawing.Size(91, 60);
            this.InvoicesImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.InvoicesImage.TabIndex = 50;
            this.InvoicesImage.TabStop = false;
            this.InvoicesImage.Visible = false;
            // 
            // button1
            // 
            this.button1.Image = ((System.Drawing.Image)(resources.GetObject("button1.Image")));
            this.button1.Location = new System.Drawing.Point(418, 84);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(26, 23);
            this.button1.TabIndex = 51;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            // 
            // tabControl2
            // 
            this.tabControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl2.Controls.Add(this.tabPageGouhuo);
            this.tabControl2.Location = new System.Drawing.Point(0, 184);
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(780, 200);
            this.tabControl2.TabIndex = 52;
            // 
            // tabPageGouhuo
            // 
            this.tabPageGouhuo.Controls.Add(this.dataGridViewBuyDetail);
            this.tabPageGouhuo.Location = new System.Drawing.Point(4, 26);
            this.tabPageGouhuo.Name = "tabPageGouhuo";
            this.tabPageGouhuo.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageGouhuo.Size = new System.Drawing.Size(772, 170);
            this.tabPageGouhuo.TabIndex = 0;
            this.tabPageGouhuo.Text = "购货明细";
            this.tabPageGouhuo.UseVisualStyleBackColor = true;
            // 
            // dataGridViewBuyDetail
            // 
            this.dataGridViewBuyDetail.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGridViewBuyDetail.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridViewBuyDetail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewBuyDetail.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.BuyID,
            this.BuyCodeD,
            this.BuyDetailCode,
            this.GoodsName,
            this.GoodsCode,
            this.GoodsUnit,
            this.GoodsSpec,
            this.BuyNumber,
            this.UnitPrice,
            this.DiscountPrice,
            this.TotalPriceD,
            this.TaxRate,
            this.TaxUnitPrice,
            this.TaxPrice,
            this.TotalTaxPrice,
            this.RemarkD});
            this.dataGridViewBuyDetail.ContextMenuStrip = this.contextMenuStrip1;
            this.dataGridViewBuyDetail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewBuyDetail.GridColor = System.Drawing.SystemColors.Control;
            this.dataGridViewBuyDetail.Location = new System.Drawing.Point(3, 3);
            this.dataGridViewBuyDetail.Name = "dataGridViewBuyDetail";
            this.dataGridViewBuyDetail.RowHeadersVisible = false;
            this.dataGridViewBuyDetail.RowHeadersWidth = 62;
            this.dataGridViewBuyDetail.RowTemplate.Height = 25;
            this.dataGridViewBuyDetail.Size = new System.Drawing.Size(766, 164);
            this.dataGridViewBuyDetail.TabIndex = 0;
            // 
            // BuyID
            // 
            this.BuyID.DataPropertyName = "BuyID";
            this.BuyID.HeaderText = "采购单ID";
            this.BuyID.MinimumWidth = 8;
            this.BuyID.Name = "BuyID";
            this.BuyID.Visible = false;
            this.BuyID.Width = 63;
            // 
            // BuyCodeD
            // 
            this.BuyCodeD.DataPropertyName = "BuyCode";
            this.BuyCodeD.HeaderText = "采购单号";
            this.BuyCodeD.MinimumWidth = 8;
            this.BuyCodeD.Name = "BuyCodeD";
            this.BuyCodeD.ReadOnly = true;
            this.BuyCodeD.Visible = false;
            this.BuyCodeD.Width = 62;
            // 
            // BuyDetailCode
            // 
            this.BuyDetailCode.DataPropertyName = "BuyDetailCode";
            this.BuyDetailCode.HeaderText = "采购明细单号";
            this.BuyDetailCode.MinimumWidth = 8;
            this.BuyDetailCode.Name = "BuyDetailCode";
            this.BuyDetailCode.ReadOnly = true;
            this.BuyDetailCode.Width = 105;
            // 
            // GoodsName
            // 
            this.GoodsName.HeaderText = "商品名称";
            this.GoodsName.MinimumWidth = 8;
            this.GoodsName.Name = "GoodsName";
            this.GoodsName.Width = 62;
            // 
            // GoodsCode
            // 
            this.GoodsCode.DataPropertyName = "GoodsCode";
            this.GoodsCode.HeaderText = "商品编码";
            this.GoodsCode.MinimumWidth = 8;
            this.GoodsCode.Name = "GoodsCode";
            this.GoodsCode.ReadOnly = true;
            this.GoodsCode.Width = 81;
            // 
            // GoodsUnit
            // 
            this.GoodsUnit.HeaderText = "计量单位";
            this.GoodsUnit.MinimumWidth = 9;
            this.GoodsUnit.Name = "GoodsUnit";
            this.GoodsUnit.ReadOnly = true;
            this.GoodsUnit.Width = 81;
            // 
            // GoodsSpec
            // 
            this.GoodsSpec.HeaderText = "商品规格";
            this.GoodsSpec.MinimumWidth = 9;
            this.GoodsSpec.Name = "GoodsSpec";
            this.GoodsSpec.ReadOnly = true;
            this.GoodsSpec.Width = 81;
            // 
            // BuyNumber
            // 
            this.BuyNumber.DataPropertyName = "BuyNumber";
            this.BuyNumber.HeaderText = "数量";
            this.BuyNumber.MinimumWidth = 8;
            this.BuyNumber.Name = "BuyNumber";
            this.BuyNumber.Width = 57;
            // 
            // UnitPrice
            // 
            this.UnitPrice.DataPropertyName = "UnitPrice";
            this.UnitPrice.HeaderText = "购货单价";
            this.UnitPrice.MinimumWidth = 8;
            this.UnitPrice.Name = "UnitPrice";
            this.UnitPrice.Width = 81;
            // 
            // DiscountPrice
            // 
            this.DiscountPrice.DataPropertyName = "DiscountPrice";
            this.DiscountPrice.HeaderText = "折扣金额";
            this.DiscountPrice.MinimumWidth = 8;
            this.DiscountPrice.Name = "DiscountPrice";
            this.DiscountPrice.Width = 81;
            // 
            // TotalPriceD
            // 
            this.TotalPriceD.DataPropertyName = "TotalPrice";
            this.TotalPriceD.HeaderText = "金额";
            this.TotalPriceD.MinimumWidth = 8;
            this.TotalPriceD.Name = "TotalPriceD";
            this.TotalPriceD.ReadOnly = true;
            this.TotalPriceD.Width = 57;
            // 
            // TaxRate
            // 
            this.TaxRate.DataPropertyName = "TaxRate";
            this.TaxRate.HeaderText = "税率";
            this.TaxRate.MinimumWidth = 8;
            this.TaxRate.Name = "TaxRate";
            this.TaxRate.Width = 57;
            // 
            // TaxUnitPrice
            // 
            this.TaxUnitPrice.DataPropertyName = "TaxUnitPrice";
            this.TaxUnitPrice.HeaderText = "含税单价";
            this.TaxUnitPrice.MinimumWidth = 8;
            this.TaxUnitPrice.Name = "TaxUnitPrice";
            this.TaxUnitPrice.ReadOnly = true;
            this.TaxUnitPrice.Width = 81;
            // 
            // TaxPrice
            // 
            this.TaxPrice.DataPropertyName = "TaxPrice";
            this.TaxPrice.HeaderText = "税额";
            this.TaxPrice.MinimumWidth = 8;
            this.TaxPrice.Name = "TaxPrice";
            this.TaxPrice.ReadOnly = true;
            this.TaxPrice.Width = 57;
            // 
            // TotalTaxPrice
            // 
            this.TotalTaxPrice.DataPropertyName = "TotalTaxPrice";
            this.TotalTaxPrice.HeaderText = "含税金额";
            this.TotalTaxPrice.MinimumWidth = 8;
            this.TotalTaxPrice.Name = "TotalTaxPrice";
            this.TotalTaxPrice.ReadOnly = true;
            this.TotalTaxPrice.Width = 81;
            // 
            // RemarkD
            // 
            this.RemarkD.DataPropertyName = "RemarkD";
            this.RemarkD.HeaderText = "备注";
            this.RemarkD.MinimumWidth = 8;
            this.RemarkD.Name = "RemarkD";
            this.RemarkD.Width = 57;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItemDel});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(113, 26);
            // 
            // toolStripMenuItemDel
            // 
            this.toolStripMenuItemDel.Name = "toolStripMenuItemDel";
            this.toolStripMenuItemDel.Size = new System.Drawing.Size(112, 22);
            this.toolStripMenuItemDel.Text = "删除行";
            // 
            // SettlementAccount
            // 
            this.SettlementAccount.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.SettlementAccount.FormattingEnabled = true;
            this.SettlementAccount.Location = new System.Drawing.Point(101, 156);
            this.SettlementAccount.Name = "SettlementAccount";
            this.SettlementAccount.Size = new System.Drawing.Size(127, 25);
            this.SettlementAccount.TabIndex = 54;
            this.SettlementAccount.Visible = false;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(10, 158);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(85, 23);
            this.label1.TabIndex = 53;
            this.label1.Text = "结算账户";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label1.Visible = false;
            // 
            // Remark
            // 
            this.Remark.Location = new System.Drawing.Point(100, 121);
            this.Remark.Name = "Remark";
            this.Remark.Size = new System.Drawing.Size(350, 60);
            this.Remark.TabIndex = 55;
            this.Remark.Text = "";
            // 
            // Frmw_Buy
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(778, 460);
            this.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.Name = "Frmw_Buy";
            this.Text = "Frmw_Buy";
            this.Controls.SetChildIndex(this.palTools, 0);
            this.Controls.SetChildIndex(this.tabControl1, 0);
            this.palTools.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabDataEdit.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.flowLayoutPanelTools.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.InvoicesImage)).EndInit();
            this.tabControl2.ResumeLayout(false);
            this.tabPageGouhuo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewBuyDetail)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private TextBox BuyCode;
        private Label lblBuyCode;
        private Label lblSupplierName;
        private DateTimePicker InvoicesDate;
        private Label lblInvoicesDate;
        private Label lblMakeUser;
        private DateTimePicker DeliveryDate;
        private Label lblDeliveryDate;
        private Label lblInvoicesImage;
        private Label lblRemark;
        private ComboBox SupplierName;
        private ComboBox MakeUser;
        private LinkLabel TotalPayable;
        private LinkLabel TotalAmountReturned;
        private Label label18;
        private Label totalprice;
        private Label weijie;
        private PictureBox InvoicesImage;
        private Button button1;
        private TabControl tabControl2;
        private TabPage tabPageGouhuo;
        private DataGridView dataGridViewBuyDetail;
        private ContextMenuStrip contextMenuStrip1;
        private ToolStripMenuItem toolStripMenuItemDel;
        private ComboBox SettlementAccount;
        private Label label1;
        private Label yijie1;
        private LinkLabel yijie;
        private RichTextBox Remark;
        private DataGridViewTextBoxColumn BuyID;
        private DataGridViewTextBoxColumn BuyCodeD;
        private DataGridViewTextBoxColumn BuyDetailCode;
        private DataGridViewComboBoxColumn GoodsName;
        private DataGridViewTextBoxColumn GoodsCode;
        private DataGridViewTextBoxColumn GoodsUnit;
        private DataGridViewTextBoxColumn GoodsSpec;
        private DataGridViewTextBoxColumn BuyNumber;
        private DataGridViewTextBoxColumn UnitPrice;
        private DataGridViewTextBoxColumn DiscountPrice;
        private DataGridViewTextBoxColumn TotalPriceD;
        private DataGridViewTextBoxColumn TaxRate;
        private DataGridViewTextBoxColumn TaxUnitPrice;
        private DataGridViewTextBoxColumn TaxPrice;
        private DataGridViewTextBoxColumn TotalTaxPrice;
        private DataGridViewTextBoxColumn RemarkD;
    }
}