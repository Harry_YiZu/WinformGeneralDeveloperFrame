namespace WinformDevFramework
{
    partial class Frmw_SettlementAccount
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblCode = new System.Windows.Forms.Label();
            this.Code = new System.Windows.Forms.TextBox();
            this.lblName = new System.Windows.Forms.Label();
            this.CName = new System.Windows.Forms.TextBox();
            this.lblBeginAmount = new System.Windows.Forms.Label();
            this.BeginAmount = new System.Windows.Forms.NumericUpDown();
            this.lblNowAmount = new System.Windows.Forms.Label();
            this.NowAmount = new System.Windows.Forms.NumericUpDown();
            this.lblSortOrder = new System.Windows.Forms.Label();
            this.SortOrder = new System.Windows.Forms.NumericUpDown();
            this.lblRemark = new System.Windows.Forms.Label();
            this.Remark = new System.Windows.Forms.TextBox();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.dataGridView3 = new System.Windows.Forms.DataGridView();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.dataGridView4 = new System.Windows.Forms.DataGridView();
            this.TotalPayable = new System.Windows.Forms.LinkLabel();
            this.TotalAmountReturned = new System.Windows.Forms.LinkLabel();
            this.TotalAmountPaid = new System.Windows.Forms.LinkLabel();
            this.TotalAmountOutstanding = new System.Windows.Forms.LinkLabel();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.palTools.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabDataEdit.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.flowLayoutPanelTools.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BeginAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NowAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SortOrder)).BeginInit();
            this.tabControl2.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).BeginInit();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView4)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.TotalPayable);
            this.groupBox1.Controls.Add(this.TotalAmountReturned);
            this.groupBox1.Controls.Add(this.TotalAmountPaid);
            this.groupBox1.Controls.Add(this.TotalAmountOutstanding);
            this.groupBox1.Controls.Add(this.label18);
            this.groupBox1.Controls.Add(this.label17);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.tabControl2);
            this.groupBox1.Controls.Add(this.Remark);
            this.groupBox1.Controls.Add(this.lblRemark);
            this.groupBox1.Controls.Add(this.SortOrder);
            this.groupBox1.Controls.Add(this.lblSortOrder);
            this.groupBox1.Controls.Add(this.NowAmount);
            this.groupBox1.Controls.Add(this.lblNowAmount);
            this.groupBox1.Controls.Add(this.BeginAmount);
            this.groupBox1.Controls.Add(this.lblBeginAmount);
            this.groupBox1.Controls.Add(this.CName);
            this.groupBox1.Controls.Add(this.lblName);
            this.groupBox1.Controls.Add(this.Code);
            this.groupBox1.Controls.Add(this.lblCode);
            this.groupBox1.Controls.SetChildIndex(this.lblCode, 0);
            this.groupBox1.Controls.SetChildIndex(this.Code, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblName, 0);
            this.groupBox1.Controls.SetChildIndex(this.CName, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblBeginAmount, 0);
            this.groupBox1.Controls.SetChildIndex(this.BeginAmount, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblNowAmount, 0);
            this.groupBox1.Controls.SetChildIndex(this.NowAmount, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblSortOrder, 0);
            this.groupBox1.Controls.SetChildIndex(this.SortOrder, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblRemark, 0);
            this.groupBox1.Controls.SetChildIndex(this.Remark, 0);
            this.groupBox1.Controls.SetChildIndex(this.tabControl2, 0);
            this.groupBox1.Controls.SetChildIndex(this.label15, 0);
            this.groupBox1.Controls.SetChildIndex(this.label16, 0);
            this.groupBox1.Controls.SetChildIndex(this.label17, 0);
            this.groupBox1.Controls.SetChildIndex(this.label18, 0);
            this.groupBox1.Controls.SetChildIndex(this.TotalAmountOutstanding, 0);
            this.groupBox1.Controls.SetChildIndex(this.TotalAmountPaid, 0);
            this.groupBox1.Controls.SetChildIndex(this.TotalAmountReturned, 0);
            this.groupBox1.Controls.SetChildIndex(this.TotalPayable, 0);
            this.groupBox1.Controls.SetChildIndex(this.txtID, 0);
            // 
            // lblCode
            // 
            this.lblCode.Location = new System.Drawing.Point(10, 10);
            this.lblCode.Name = "lblCode";
            this.lblCode.Size = new System.Drawing.Size(85, 23);
            this.lblCode.TabIndex = 2;
            this.lblCode.Text = "编号";
            this.lblCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Code
            // 
            this.Code.Location = new System.Drawing.Point(100, 10);
            this.Code.Name = "Code";
            this.Code.Size = new System.Drawing.Size(130, 23);
            this.Code.TabIndex = 1;
            // 
            // lblName
            // 
            this.lblName.Location = new System.Drawing.Point(230, 10);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(85, 23);
            this.lblName.TabIndex = 4;
            this.lblName.Text = "名称";
            this.lblName.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // CName
            // 
            this.CName.Location = new System.Drawing.Point(320, 10);
            this.CName.Name = "CName";
            this.CName.Size = new System.Drawing.Size(130, 23);
            this.CName.TabIndex = 3;
            // 
            // lblBeginAmount
            // 
            this.lblBeginAmount.Location = new System.Drawing.Point(230, 45);
            this.lblBeginAmount.Name = "lblBeginAmount";
            this.lblBeginAmount.Size = new System.Drawing.Size(85, 23);
            this.lblBeginAmount.TabIndex = 6;
            this.lblBeginAmount.Text = "期初金额";
            this.lblBeginAmount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BeginAmount
            // 
            this.BeginAmount.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.BeginAmount.Location = new System.Drawing.Point(320, 45);
            this.BeginAmount.Maximum = new decimal(new int[] {
            99999999,
            0,
            0,
            0});
            this.BeginAmount.Minimum = new decimal(new int[] {
            99999999,
            0,
            0,
            -2147483648});
            this.BeginAmount.Name = "BeginAmount";
            this.BeginAmount.Size = new System.Drawing.Size(130, 23);
            this.BeginAmount.TabIndex = 5;
            // 
            // lblNowAmount
            // 
            this.lblNowAmount.Location = new System.Drawing.Point(10, 45);
            this.lblNowAmount.Name = "lblNowAmount";
            this.lblNowAmount.Size = new System.Drawing.Size(85, 23);
            this.lblNowAmount.TabIndex = 8;
            this.lblNowAmount.Text = "当前余额";
            this.lblNowAmount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // NowAmount
            // 
            this.NowAmount.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.NowAmount.Location = new System.Drawing.Point(100, 45);
            this.NowAmount.Maximum = new decimal(new int[] {
            99999999,
            0,
            0,
            0});
            this.NowAmount.Minimum = new decimal(new int[] {
            99999999,
            0,
            0,
            -2147483648});
            this.NowAmount.Name = "NowAmount";
            this.NowAmount.Size = new System.Drawing.Size(130, 23);
            this.NowAmount.TabIndex = 7;
            // 
            // lblSortOrder
            // 
            this.lblSortOrder.Location = new System.Drawing.Point(10, 82);
            this.lblSortOrder.Name = "lblSortOrder";
            this.lblSortOrder.Size = new System.Drawing.Size(85, 23);
            this.lblSortOrder.TabIndex = 10;
            this.lblSortOrder.Text = "排序";
            this.lblSortOrder.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // SortOrder
            // 
            this.SortOrder.Location = new System.Drawing.Point(100, 82);
            this.SortOrder.Name = "SortOrder";
            this.SortOrder.Size = new System.Drawing.Size(130, 23);
            this.SortOrder.TabIndex = 9;
            // 
            // lblRemark
            // 
            this.lblRemark.Location = new System.Drawing.Point(230, 82);
            this.lblRemark.Name = "lblRemark";
            this.lblRemark.Size = new System.Drawing.Size(85, 23);
            this.lblRemark.TabIndex = 12;
            this.lblRemark.Text = "备注";
            this.lblRemark.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Remark
            // 
            this.Remark.Location = new System.Drawing.Point(320, 82);
            this.Remark.Name = "Remark";
            this.Remark.Size = new System.Drawing.Size(130, 23);
            this.Remark.TabIndex = 11;
            // 
            // tabControl2
            // 
            this.tabControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl2.Controls.Add(this.tabPage1);
            this.tabControl2.Controls.Add(this.tabPage2);
            this.tabControl2.Controls.Add(this.tabPage3);
            this.tabControl2.Controls.Add(this.tabPage4);
            this.tabControl2.Location = new System.Drawing.Point(0, 144);
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(786, 242);
            this.tabControl2.TabIndex = 34;
            this.tabControl2.Visible = false;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.dataGridView1);
            this.tabPage1.Location = new System.Drawing.Point(4, 26);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(778, 212);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "收款";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.GridColor = System.Drawing.SystemColors.Control;
            this.dataGridView1.Location = new System.Drawing.Point(3, 3);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.RowTemplate.Height = 25;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(772, 206);
            this.dataGridView1.TabIndex = 0;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.dataGridView2);
            this.tabPage2.Location = new System.Drawing.Point(4, 26);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(778, 212);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "付款";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // dataGridView2
            // 
            this.dataGridView2.AllowUserToAddRows = false;
            this.dataGridView2.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView2.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView2.GridColor = System.Drawing.SystemColors.Control;
            this.dataGridView2.Location = new System.Drawing.Point(3, 3);
            this.dataGridView2.MultiSelect = false;
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.ReadOnly = true;
            this.dataGridView2.RowHeadersVisible = false;
            this.dataGridView2.RowTemplate.Height = 25;
            this.dataGridView2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView2.Size = new System.Drawing.Size(772, 206);
            this.dataGridView2.TabIndex = 1;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.dataGridView3);
            this.tabPage3.Location = new System.Drawing.Point(4, 26);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(778, 212);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "其他收入";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // dataGridView3
            // 
            this.dataGridView3.AllowUserToAddRows = false;
            this.dataGridView3.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView3.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridView3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView3.GridColor = System.Drawing.SystemColors.Control;
            this.dataGridView3.Location = new System.Drawing.Point(3, 3);
            this.dataGridView3.MultiSelect = false;
            this.dataGridView3.Name = "dataGridView3";
            this.dataGridView3.ReadOnly = true;
            this.dataGridView3.RowHeadersVisible = false;
            this.dataGridView3.RowTemplate.Height = 25;
            this.dataGridView3.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView3.Size = new System.Drawing.Size(772, 206);
            this.dataGridView3.TabIndex = 1;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.dataGridView4);
            this.tabPage4.Location = new System.Drawing.Point(4, 26);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(778, 212);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "其他支出";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // dataGridView4
            // 
            this.dataGridView4.AllowUserToAddRows = false;
            this.dataGridView4.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView4.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridView4.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView4.GridColor = System.Drawing.SystemColors.Control;
            this.dataGridView4.Location = new System.Drawing.Point(3, 3);
            this.dataGridView4.MultiSelect = false;
            this.dataGridView4.Name = "dataGridView4";
            this.dataGridView4.ReadOnly = true;
            this.dataGridView4.RowHeadersVisible = false;
            this.dataGridView4.RowTemplate.Height = 25;
            this.dataGridView4.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView4.Size = new System.Drawing.Size(772, 206);
            this.dataGridView4.TabIndex = 1;
            // 
            // TotalPayable
            // 
            this.TotalPayable.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.TotalPayable.Location = new System.Drawing.Point(597, 12);
            this.TotalPayable.Name = "TotalPayable";
            this.TotalPayable.Size = new System.Drawing.Size(116, 17);
            this.TotalPayable.TabIndex = 48;
            this.TotalPayable.TabStop = true;
            this.TotalPayable.Text = "0.00";
            this.TotalPayable.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.TotalPayable.Visible = false;
            // 
            // TotalAmountReturned
            // 
            this.TotalAmountReturned.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.TotalAmountReturned.Location = new System.Drawing.Point(597, 45);
            this.TotalAmountReturned.Name = "TotalAmountReturned";
            this.TotalAmountReturned.Size = new System.Drawing.Size(115, 17);
            this.TotalAmountReturned.TabIndex = 47;
            this.TotalAmountReturned.TabStop = true;
            this.TotalAmountReturned.Text = "0.00";
            this.TotalAmountReturned.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.TotalAmountReturned.Visible = false;
            // 
            // TotalAmountPaid
            // 
            this.TotalAmountPaid.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.TotalAmountPaid.Location = new System.Drawing.Point(596, 82);
            this.TotalAmountPaid.Name = "TotalAmountPaid";
            this.TotalAmountPaid.Size = new System.Drawing.Size(116, 17);
            this.TotalAmountPaid.TabIndex = 46;
            this.TotalAmountPaid.TabStop = true;
            this.TotalAmountPaid.Text = "0.00";
            this.TotalAmountPaid.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.TotalAmountPaid.Visible = false;
            // 
            // TotalAmountOutstanding
            // 
            this.TotalAmountOutstanding.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.TotalAmountOutstanding.Location = new System.Drawing.Point(596, 121);
            this.TotalAmountOutstanding.Name = "TotalAmountOutstanding";
            this.TotalAmountOutstanding.Size = new System.Drawing.Size(115, 17);
            this.TotalAmountOutstanding.TabIndex = 45;
            this.TotalAmountOutstanding.TabStop = true;
            this.TotalAmountOutstanding.Text = "0.00";
            this.TotalAmountOutstanding.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.TotalAmountOutstanding.Visible = false;
            // 
            // label18
            // 
            this.label18.Location = new System.Drawing.Point(505, 118);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(85, 23);
            this.label18.TabIndex = 44;
            this.label18.Text = "未结款总额";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label18.Visible = false;
            // 
            // label17
            // 
            this.label17.Location = new System.Drawing.Point(505, 79);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(85, 23);
            this.label17.TabIndex = 43;
            this.label17.Text = "已结款总额";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label17.Visible = false;
            // 
            // label16
            // 
            this.label16.Location = new System.Drawing.Point(506, 42);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(85, 23);
            this.label16.TabIndex = 42;
            this.label16.Text = "退货款总额";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label16.Visible = false;
            // 
            // label15
            // 
            this.label15.Location = new System.Drawing.Point(506, 10);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(85, 23);
            this.label15.TabIndex = 41;
            this.label15.Text = "应付款总额";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label15.Visible = false;
            // 
            // Frmw_SettlementAccount
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Name = "Frmw_SettlementAccount";
            this.Text = "Frmw_SettlementAccount";
            this.palTools.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabDataEdit.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.flowLayoutPanelTools.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.BeginAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NowAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SortOrder)).EndInit();
            this.tabControl2.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.tabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).EndInit();
            this.tabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView4)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
            private TextBox Code;
            private Label lblCode;
            private TextBox CName;
            private Label lblName;
            private NumericUpDown BeginAmount;
            private Label lblBeginAmount;
            private NumericUpDown NowAmount;
            private Label lblNowAmount;
            private NumericUpDown SortOrder;
            private Label lblSortOrder;
            private TextBox Remark;
            private Label lblRemark;
        private TabControl tabControl2;
        private TabPage tabPage1;
        private DataGridView dataGridView1;
        private TabPage tabPage2;
        private DataGridView dataGridView2;
        private TabPage tabPage3;
        private DataGridView dataGridView3;
        private TabPage tabPage4;
        private DataGridView dataGridView4;
        private LinkLabel TotalPayable;
        private LinkLabel TotalAmountReturned;
        private LinkLabel TotalAmountPaid;
        private LinkLabel TotalAmountOutstanding;
        private Label label18;
        private Label label17;
        private Label label16;
        private Label label15;
    }
}