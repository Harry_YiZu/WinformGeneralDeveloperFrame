namespace WinformDevFramework
{
    partial class Frmw_Customer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblSupplierName = new System.Windows.Forms.Label();
            this.CustomerName = new System.Windows.Forms.TextBox();
            this.lblContactPerson = new System.Windows.Forms.Label();
            this.ContactPerson = new System.Windows.Forms.TextBox();
            this.lblPhoneNumber = new System.Windows.Forms.Label();
            this.PhoneNumber = new System.Windows.Forms.TextBox();
            this.lblEmail = new System.Windows.Forms.Label();
            this.Email = new System.Windows.Forms.TextBox();
            this.lblBeginCollect = new System.Windows.Forms.Label();
            this.BeginCollect = new System.Windows.Forms.NumericUpDown();
            this.lblEndPayCollect = new System.Windows.Forms.Label();
            this.EndPayCollect = new System.Windows.Forms.NumericUpDown();
            this.lblTaxtRate = new System.Windows.Forms.Label();
            this.TaxtRate = new System.Windows.Forms.NumericUpDown();
            this.lblSortOrder = new System.Windows.Forms.Label();
            this.SortOrder = new System.Windows.Forms.NumericUpDown();
            this.lblStatus = new System.Windows.Forms.Label();
            this.Status = new System.Windows.Forms.CheckBox();
            this.lblRemark = new System.Windows.Forms.Label();
            this.Remark = new System.Windows.Forms.TextBox();
            this.lblFaxing = new System.Windows.Forms.Label();
            this.Faxing = new System.Windows.Forms.TextBox();
            this.lblAddress = new System.Windows.Forms.Label();
            this.Address = new System.Windows.Forms.TextBox();
            this.lblBank = new System.Windows.Forms.Label();
            this.Bank = new System.Windows.Forms.TextBox();
            this.lblTaxpayerNumber = new System.Windows.Forms.Label();
            this.TaxpayerNumber = new System.Windows.Forms.TextBox();
            this.lblBankAccount = new System.Windows.Forms.Label();
            this.BankAccount = new System.Windows.Forms.TextBox();
            this.lblLandlinePhone = new System.Windows.Forms.Label();
            this.LandlinePhone = new System.Windows.Forms.TextBox();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.dataGridView3 = new System.Windows.Forms.DataGridView();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.dataGridView4 = new System.Windows.Forms.DataGridView();
            this.TotalPayable = new System.Windows.Forms.LinkLabel();
            this.TotalAmountReturned = new System.Windows.Forms.LinkLabel();
            this.TotalAmountPaid = new System.Windows.Forms.LinkLabel();
            this.TotalAmountOutstanding = new System.Windows.Forms.LinkLabel();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.CustomerCode = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.palTools.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabDataEdit.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.flowLayoutPanelTools.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BeginCollect)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EndPayCollect)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TaxtRate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SortOrder)).BeginInit();
            this.tabControl2.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).BeginInit();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView4)).BeginInit();
            this.SuspendLayout();
            // 
            // palTools
            // 
            this.palTools.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.palTools.Size = new System.Drawing.Size(874, 31);
            // 
            // tabControl1
            // 
            this.tabControl1.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.tabControl1.Size = new System.Drawing.Size(874, 520);
            // 
            // tabList
            // 
            this.tabList.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.tabList.Padding = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.tabList.Size = new System.Drawing.Size(866, 490);
            // 
            // tabDataEdit
            // 
            this.tabDataEdit.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.tabDataEdit.Padding = new System.Windows.Forms.Padding(6, 5, 6, 5);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.CustomerCode);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.TotalPayable);
            this.groupBox1.Controls.Add(this.TotalAmountReturned);
            this.groupBox1.Controls.Add(this.TotalAmountPaid);
            this.groupBox1.Controls.Add(this.TotalAmountOutstanding);
            this.groupBox1.Controls.Add(this.label18);
            this.groupBox1.Controls.Add(this.label17);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.tabControl2);
            this.groupBox1.Controls.Add(this.LandlinePhone);
            this.groupBox1.Controls.Add(this.lblLandlinePhone);
            this.groupBox1.Controls.Add(this.BankAccount);
            this.groupBox1.Controls.Add(this.lblBankAccount);
            this.groupBox1.Controls.Add(this.TaxpayerNumber);
            this.groupBox1.Controls.Add(this.lblTaxpayerNumber);
            this.groupBox1.Controls.Add(this.Bank);
            this.groupBox1.Controls.Add(this.lblBank);
            this.groupBox1.Controls.Add(this.Address);
            this.groupBox1.Controls.Add(this.lblAddress);
            this.groupBox1.Controls.Add(this.Faxing);
            this.groupBox1.Controls.Add(this.lblFaxing);
            this.groupBox1.Controls.Add(this.Remark);
            this.groupBox1.Controls.Add(this.lblRemark);
            this.groupBox1.Controls.Add(this.Status);
            this.groupBox1.Controls.Add(this.lblStatus);
            this.groupBox1.Controls.Add(this.SortOrder);
            this.groupBox1.Controls.Add(this.lblSortOrder);
            this.groupBox1.Controls.Add(this.TaxtRate);
            this.groupBox1.Controls.Add(this.lblTaxtRate);
            this.groupBox1.Controls.Add(this.EndPayCollect);
            this.groupBox1.Controls.Add(this.lblEndPayCollect);
            this.groupBox1.Controls.Add(this.BeginCollect);
            this.groupBox1.Controls.Add(this.lblBeginCollect);
            this.groupBox1.Controls.Add(this.Email);
            this.groupBox1.Controls.Add(this.lblEmail);
            this.groupBox1.Controls.Add(this.PhoneNumber);
            this.groupBox1.Controls.Add(this.lblPhoneNumber);
            this.groupBox1.Controls.Add(this.ContactPerson);
            this.groupBox1.Controls.Add(this.lblContactPerson);
            this.groupBox1.Controls.Add(this.CustomerName);
            this.groupBox1.Controls.Add(this.lblSupplierName);
            this.groupBox1.Location = new System.Drawing.Point(6, 5);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.groupBox1.Padding = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.groupBox1.Size = new System.Drawing.Size(780, 379);
            this.groupBox1.Controls.SetChildIndex(this.lblSupplierName, 0);
            this.groupBox1.Controls.SetChildIndex(this.CustomerName, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblContactPerson, 0);
            this.groupBox1.Controls.SetChildIndex(this.ContactPerson, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblPhoneNumber, 0);
            this.groupBox1.Controls.SetChildIndex(this.PhoneNumber, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblEmail, 0);
            this.groupBox1.Controls.SetChildIndex(this.Email, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblBeginCollect, 0);
            this.groupBox1.Controls.SetChildIndex(this.BeginCollect, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblEndPayCollect, 0);
            this.groupBox1.Controls.SetChildIndex(this.EndPayCollect, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblTaxtRate, 0);
            this.groupBox1.Controls.SetChildIndex(this.TaxtRate, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblSortOrder, 0);
            this.groupBox1.Controls.SetChildIndex(this.SortOrder, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblStatus, 0);
            this.groupBox1.Controls.SetChildIndex(this.Status, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblRemark, 0);
            this.groupBox1.Controls.SetChildIndex(this.Remark, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblFaxing, 0);
            this.groupBox1.Controls.SetChildIndex(this.Faxing, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblAddress, 0);
            this.groupBox1.Controls.SetChildIndex(this.Address, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblBank, 0);
            this.groupBox1.Controls.SetChildIndex(this.Bank, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblTaxpayerNumber, 0);
            this.groupBox1.Controls.SetChildIndex(this.TaxpayerNumber, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblBankAccount, 0);
            this.groupBox1.Controls.SetChildIndex(this.BankAccount, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblLandlinePhone, 0);
            this.groupBox1.Controls.SetChildIndex(this.LandlinePhone, 0);
            this.groupBox1.Controls.SetChildIndex(this.txtID, 0);
            this.groupBox1.Controls.SetChildIndex(this.tabControl2, 0);
            this.groupBox1.Controls.SetChildIndex(this.label15, 0);
            this.groupBox1.Controls.SetChildIndex(this.label16, 0);
            this.groupBox1.Controls.SetChildIndex(this.label17, 0);
            this.groupBox1.Controls.SetChildIndex(this.label18, 0);
            this.groupBox1.Controls.SetChildIndex(this.TotalAmountOutstanding, 0);
            this.groupBox1.Controls.SetChildIndex(this.TotalAmountPaid, 0);
            this.groupBox1.Controls.SetChildIndex(this.TotalAmountReturned, 0);
            this.groupBox1.Controls.SetChildIndex(this.TotalPayable, 0);
            this.groupBox1.Controls.SetChildIndex(this.label1, 0);
            this.groupBox1.Controls.SetChildIndex(this.CustomerCode, 0);
            // 
            // flowLayoutPanelTools
            // 
            this.flowLayoutPanelTools.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.flowLayoutPanelTools.Size = new System.Drawing.Size(870, 27);
            // 
            // txtID
            // 
            this.txtID.Location = new System.Drawing.Point(199, 31);
            this.txtID.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.txtID.Size = new System.Drawing.Size(56, 23);
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(2, 2);
            this.btnAdd.Margin = new System.Windows.Forms.Padding(2);
            this.btnAdd.Size = new System.Drawing.Size(54, 23);
            // 
            // btnEdit
            // 
            this.btnEdit.Location = new System.Drawing.Point(60, 2);
            this.btnEdit.Margin = new System.Windows.Forms.Padding(2);
            this.btnEdit.Size = new System.Drawing.Size(53, 23);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(117, 2);
            this.btnSave.Margin = new System.Windows.Forms.Padding(2);
            this.btnSave.Size = new System.Drawing.Size(53, 23);
            // 
            // btnCanel
            // 
            this.btnCanel.Location = new System.Drawing.Point(174, 2);
            this.btnCanel.Margin = new System.Windows.Forms.Padding(2);
            this.btnCanel.Size = new System.Drawing.Size(53, 23);
            // 
            // btnDel
            // 
            this.btnDel.Location = new System.Drawing.Point(231, 2);
            this.btnDel.Margin = new System.Windows.Forms.Padding(2);
            this.btnDel.Size = new System.Drawing.Size(53, 23);
            // 
            // btnResetPW
            // 
            this.btnResetPW.Location = new System.Drawing.Point(288, 2);
            this.btnResetPW.Margin = new System.Windows.Forms.Padding(2);
            this.btnResetPW.Size = new System.Drawing.Size(53, 25);
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(402, 2);
            this.btnClose.Margin = new System.Windows.Forms.Padding(2);
            this.btnClose.Size = new System.Drawing.Size(53, 23);
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(345, 2);
            this.btnSearch.Margin = new System.Windows.Forms.Padding(2);
            this.btnSearch.Size = new System.Drawing.Size(53, 23);
            // 
            // lblSupplierName
            // 
            this.lblSupplierName.Location = new System.Drawing.Point(10, 10);
            this.lblSupplierName.Name = "lblSupplierName";
            this.lblSupplierName.Size = new System.Drawing.Size(85, 23);
            this.lblSupplierName.TabIndex = 2;
            this.lblSupplierName.Text = "名称";
            this.lblSupplierName.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // CustomerName
            // 
            this.CustomerName.Location = new System.Drawing.Point(100, 10);
            this.CustomerName.Name = "CustomerName";
            this.CustomerName.Size = new System.Drawing.Size(130, 23);
            this.CustomerName.TabIndex = 1;
            // 
            // lblContactPerson
            // 
            this.lblContactPerson.Location = new System.Drawing.Point(230, 10);
            this.lblContactPerson.Name = "lblContactPerson";
            this.lblContactPerson.Size = new System.Drawing.Size(85, 23);
            this.lblContactPerson.TabIndex = 4;
            this.lblContactPerson.Text = "联系人";
            this.lblContactPerson.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ContactPerson
            // 
            this.ContactPerson.Location = new System.Drawing.Point(320, 10);
            this.ContactPerson.Name = "ContactPerson";
            this.ContactPerson.Size = new System.Drawing.Size(130, 23);
            this.ContactPerson.TabIndex = 3;
            // 
            // lblPhoneNumber
            // 
            this.lblPhoneNumber.Location = new System.Drawing.Point(450, 10);
            this.lblPhoneNumber.Name = "lblPhoneNumber";
            this.lblPhoneNumber.Size = new System.Drawing.Size(85, 23);
            this.lblPhoneNumber.TabIndex = 6;
            this.lblPhoneNumber.Text = "手机号码";
            this.lblPhoneNumber.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // PhoneNumber
            // 
            this.PhoneNumber.Location = new System.Drawing.Point(540, 10);
            this.PhoneNumber.Name = "PhoneNumber";
            this.PhoneNumber.Size = new System.Drawing.Size(130, 23);
            this.PhoneNumber.TabIndex = 5;
            // 
            // lblEmail
            // 
            this.lblEmail.Location = new System.Drawing.Point(230, 185);
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.Size = new System.Drawing.Size(85, 23);
            this.lblEmail.TabIndex = 8;
            this.lblEmail.Text = "邮箱";
            this.lblEmail.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Email
            // 
            this.Email.Location = new System.Drawing.Point(320, 185);
            this.Email.Name = "Email";
            this.Email.Size = new System.Drawing.Size(130, 23);
            this.Email.TabIndex = 7;
            // 
            // lblBeginCollect
            // 
            this.lblBeginCollect.Location = new System.Drawing.Point(230, 45);
            this.lblBeginCollect.Name = "lblBeginCollect";
            this.lblBeginCollect.Size = new System.Drawing.Size(85, 23);
            this.lblBeginCollect.TabIndex = 10;
            this.lblBeginCollect.Text = "期初应收";
            this.lblBeginCollect.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BeginCollect
            // 
            this.BeginCollect.Location = new System.Drawing.Point(320, 45);
            this.BeginCollect.Name = "BeginCollect";
            this.BeginCollect.Size = new System.Drawing.Size(130, 23);
            this.BeginCollect.TabIndex = 9;
            // 
            // lblEndPayCollect
            // 
            this.lblEndPayCollect.Location = new System.Drawing.Point(450, 45);
            this.lblEndPayCollect.Name = "lblEndPayCollect";
            this.lblEndPayCollect.Size = new System.Drawing.Size(85, 23);
            this.lblEndPayCollect.TabIndex = 12;
            this.lblEndPayCollect.Text = "期末应收";
            this.lblEndPayCollect.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // EndPayCollect
            // 
            this.EndPayCollect.Location = new System.Drawing.Point(540, 45);
            this.EndPayCollect.Name = "EndPayCollect";
            this.EndPayCollect.Size = new System.Drawing.Size(130, 23);
            this.EndPayCollect.TabIndex = 11;
            // 
            // lblTaxtRate
            // 
            this.lblTaxtRate.Location = new System.Drawing.Point(10, 80);
            this.lblTaxtRate.Name = "lblTaxtRate";
            this.lblTaxtRate.Size = new System.Drawing.Size(85, 23);
            this.lblTaxtRate.TabIndex = 14;
            this.lblTaxtRate.Text = "税率";
            this.lblTaxtRate.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TaxtRate
            // 
            this.TaxtRate.Location = new System.Drawing.Point(100, 80);
            this.TaxtRate.Name = "TaxtRate";
            this.TaxtRate.Size = new System.Drawing.Size(130, 23);
            this.TaxtRate.TabIndex = 13;
            // 
            // lblSortOrder
            // 
            this.lblSortOrder.Location = new System.Drawing.Point(230, 80);
            this.lblSortOrder.Name = "lblSortOrder";
            this.lblSortOrder.Size = new System.Drawing.Size(85, 23);
            this.lblSortOrder.TabIndex = 16;
            this.lblSortOrder.Text = "排序";
            this.lblSortOrder.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // SortOrder
            // 
            this.SortOrder.Location = new System.Drawing.Point(320, 80);
            this.SortOrder.Name = "SortOrder";
            this.SortOrder.Size = new System.Drawing.Size(130, 23);
            this.SortOrder.TabIndex = 15;
            // 
            // lblStatus
            // 
            this.lblStatus.Location = new System.Drawing.Point(450, 80);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(85, 23);
            this.lblStatus.TabIndex = 18;
            this.lblStatus.Text = "状态";
            this.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Status
            // 
            this.Status.Location = new System.Drawing.Point(540, 80);
            this.Status.Name = "Status";
            this.Status.Size = new System.Drawing.Size(130, 23);
            this.Status.TabIndex = 17;
            this.Status.Text = "是否启用";
            // 
            // lblRemark
            // 
            this.lblRemark.Location = new System.Drawing.Point(10, 115);
            this.lblRemark.Name = "lblRemark";
            this.lblRemark.Size = new System.Drawing.Size(85, 23);
            this.lblRemark.TabIndex = 20;
            this.lblRemark.Text = "备注";
            this.lblRemark.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Remark
            // 
            this.Remark.Location = new System.Drawing.Point(100, 115);
            this.Remark.Name = "Remark";
            this.Remark.Size = new System.Drawing.Size(130, 23);
            this.Remark.TabIndex = 19;
            // 
            // lblFaxing
            // 
            this.lblFaxing.Location = new System.Drawing.Point(230, 115);
            this.lblFaxing.Name = "lblFaxing";
            this.lblFaxing.Size = new System.Drawing.Size(85, 23);
            this.lblFaxing.TabIndex = 22;
            this.lblFaxing.Text = "传真";
            this.lblFaxing.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Faxing
            // 
            this.Faxing.Location = new System.Drawing.Point(320, 115);
            this.Faxing.Name = "Faxing";
            this.Faxing.Size = new System.Drawing.Size(130, 23);
            this.Faxing.TabIndex = 21;
            // 
            // lblAddress
            // 
            this.lblAddress.Location = new System.Drawing.Point(450, 115);
            this.lblAddress.Name = "lblAddress";
            this.lblAddress.Size = new System.Drawing.Size(85, 23);
            this.lblAddress.TabIndex = 24;
            this.lblAddress.Text = "地址";
            this.lblAddress.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Address
            // 
            this.Address.Location = new System.Drawing.Point(540, 115);
            this.Address.Name = "Address";
            this.Address.Size = new System.Drawing.Size(130, 23);
            this.Address.TabIndex = 23;
            // 
            // lblBank
            // 
            this.lblBank.Location = new System.Drawing.Point(10, 150);
            this.lblBank.Name = "lblBank";
            this.lblBank.Size = new System.Drawing.Size(85, 23);
            this.lblBank.TabIndex = 26;
            this.lblBank.Text = "开户行";
            this.lblBank.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Bank
            // 
            this.Bank.Location = new System.Drawing.Point(100, 150);
            this.Bank.Name = "Bank";
            this.Bank.Size = new System.Drawing.Size(130, 23);
            this.Bank.TabIndex = 25;
            // 
            // lblTaxpayerNumber
            // 
            this.lblTaxpayerNumber.Location = new System.Drawing.Point(230, 150);
            this.lblTaxpayerNumber.Name = "lblTaxpayerNumber";
            this.lblTaxpayerNumber.Size = new System.Drawing.Size(85, 23);
            this.lblTaxpayerNumber.TabIndex = 28;
            this.lblTaxpayerNumber.Text = "纳税人识别号";
            this.lblTaxpayerNumber.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TaxpayerNumber
            // 
            this.TaxpayerNumber.Location = new System.Drawing.Point(320, 150);
            this.TaxpayerNumber.Name = "TaxpayerNumber";
            this.TaxpayerNumber.Size = new System.Drawing.Size(130, 23);
            this.TaxpayerNumber.TabIndex = 27;
            // 
            // lblBankAccount
            // 
            this.lblBankAccount.Location = new System.Drawing.Point(450, 150);
            this.lblBankAccount.Name = "lblBankAccount";
            this.lblBankAccount.Size = new System.Drawing.Size(85, 23);
            this.lblBankAccount.TabIndex = 30;
            this.lblBankAccount.Text = "银行账号";
            this.lblBankAccount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BankAccount
            // 
            this.BankAccount.Location = new System.Drawing.Point(540, 150);
            this.BankAccount.Name = "BankAccount";
            this.BankAccount.Size = new System.Drawing.Size(130, 23);
            this.BankAccount.TabIndex = 29;
            // 
            // lblLandlinePhone
            // 
            this.lblLandlinePhone.Location = new System.Drawing.Point(10, 185);
            this.lblLandlinePhone.Name = "lblLandlinePhone";
            this.lblLandlinePhone.Size = new System.Drawing.Size(85, 23);
            this.lblLandlinePhone.TabIndex = 32;
            this.lblLandlinePhone.Text = "固定电话";
            this.lblLandlinePhone.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LandlinePhone
            // 
            this.LandlinePhone.Location = new System.Drawing.Point(100, 185);
            this.LandlinePhone.Name = "LandlinePhone";
            this.LandlinePhone.Size = new System.Drawing.Size(130, 23);
            this.LandlinePhone.TabIndex = 31;
            // 
            // tabControl2
            // 
            this.tabControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl2.Controls.Add(this.tabPage1);
            this.tabControl2.Controls.Add(this.tabPage2);
            this.tabControl2.Controls.Add(this.tabPage3);
            this.tabControl2.Controls.Add(this.tabPage4);
            this.tabControl2.Location = new System.Drawing.Point(0, 214);
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(783, 163);
            this.tabControl2.TabIndex = 33;
            this.tabControl2.Visible = false;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.dataGridView1);
            this.tabPage1.Location = new System.Drawing.Point(4, 26);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(775, 133);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "应付款";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.GridColor = System.Drawing.SystemColors.Control;
            this.dataGridView1.Location = new System.Drawing.Point(3, 3);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.RowHeadersWidth = 72;
            this.dataGridView1.RowTemplate.Height = 25;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(769, 127);
            this.dataGridView1.TabIndex = 0;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.dataGridView2);
            this.tabPage2.Location = new System.Drawing.Point(4, 26);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(775, 133);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "退货款";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // dataGridView2
            // 
            this.dataGridView2.AllowUserToAddRows = false;
            this.dataGridView2.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView2.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView2.GridColor = System.Drawing.SystemColors.Control;
            this.dataGridView2.Location = new System.Drawing.Point(3, 3);
            this.dataGridView2.MultiSelect = false;
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.ReadOnly = true;
            this.dataGridView2.RowHeadersVisible = false;
            this.dataGridView2.RowHeadersWidth = 72;
            this.dataGridView2.RowTemplate.Height = 25;
            this.dataGridView2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView2.Size = new System.Drawing.Size(769, 127);
            this.dataGridView2.TabIndex = 1;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.dataGridView3);
            this.tabPage3.Location = new System.Drawing.Point(4, 26);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(775, 133);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "已结款";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // dataGridView3
            // 
            this.dataGridView3.AllowUserToAddRows = false;
            this.dataGridView3.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView3.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridView3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView3.GridColor = System.Drawing.SystemColors.Control;
            this.dataGridView3.Location = new System.Drawing.Point(3, 3);
            this.dataGridView3.MultiSelect = false;
            this.dataGridView3.Name = "dataGridView3";
            this.dataGridView3.ReadOnly = true;
            this.dataGridView3.RowHeadersVisible = false;
            this.dataGridView3.RowHeadersWidth = 72;
            this.dataGridView3.RowTemplate.Height = 25;
            this.dataGridView3.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView3.Size = new System.Drawing.Size(769, 127);
            this.dataGridView3.TabIndex = 1;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.dataGridView4);
            this.tabPage4.Location = new System.Drawing.Point(4, 26);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(775, 133);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "未结款";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // dataGridView4
            // 
            this.dataGridView4.AllowUserToAddRows = false;
            this.dataGridView4.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView4.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridView4.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView4.GridColor = System.Drawing.SystemColors.Control;
            this.dataGridView4.Location = new System.Drawing.Point(3, 3);
            this.dataGridView4.MultiSelect = false;
            this.dataGridView4.Name = "dataGridView4";
            this.dataGridView4.ReadOnly = true;
            this.dataGridView4.RowHeadersVisible = false;
            this.dataGridView4.RowHeadersWidth = 72;
            this.dataGridView4.RowTemplate.Height = 25;
            this.dataGridView4.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView4.Size = new System.Drawing.Size(769, 127);
            this.dataGridView4.TabIndex = 1;
            // 
            // TotalPayable
            // 
            this.TotalPayable.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.TotalPayable.Location = new System.Drawing.Point(767, 12);
            this.TotalPayable.Name = "TotalPayable";
            this.TotalPayable.Size = new System.Drawing.Size(116, 17);
            this.TotalPayable.TabIndex = 48;
            this.TotalPayable.TabStop = true;
            this.TotalPayable.Text = "0.00";
            this.TotalPayable.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.TotalPayable.Visible = false;
            // 
            // TotalAmountReturned
            // 
            this.TotalAmountReturned.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.TotalAmountReturned.Location = new System.Drawing.Point(767, 48);
            this.TotalAmountReturned.Name = "TotalAmountReturned";
            this.TotalAmountReturned.Size = new System.Drawing.Size(115, 17);
            this.TotalAmountReturned.TabIndex = 47;
            this.TotalAmountReturned.TabStop = true;
            this.TotalAmountReturned.Text = "0.00";
            this.TotalAmountReturned.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.TotalAmountReturned.Visible = false;
            // 
            // TotalAmountPaid
            // 
            this.TotalAmountPaid.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.TotalAmountPaid.Location = new System.Drawing.Point(767, 83);
            this.TotalAmountPaid.Name = "TotalAmountPaid";
            this.TotalAmountPaid.Size = new System.Drawing.Size(116, 17);
            this.TotalAmountPaid.TabIndex = 46;
            this.TotalAmountPaid.TabStop = true;
            this.TotalAmountPaid.Text = "0.00";
            this.TotalAmountPaid.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.TotalAmountPaid.Visible = false;
            // 
            // TotalAmountOutstanding
            // 
            this.TotalAmountOutstanding.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.TotalAmountOutstanding.Location = new System.Drawing.Point(767, 118);
            this.TotalAmountOutstanding.Name = "TotalAmountOutstanding";
            this.TotalAmountOutstanding.Size = new System.Drawing.Size(115, 17);
            this.TotalAmountOutstanding.TabIndex = 45;
            this.TotalAmountOutstanding.TabStop = true;
            this.TotalAmountOutstanding.Text = "0.00";
            this.TotalAmountOutstanding.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.TotalAmountOutstanding.Visible = false;
            // 
            // label18
            // 
            this.label18.Location = new System.Drawing.Point(676, 115);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(85, 23);
            this.label18.TabIndex = 44;
            this.label18.Text = "未结款总额";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label18.Visible = false;
            // 
            // label17
            // 
            this.label17.Location = new System.Drawing.Point(676, 80);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(85, 23);
            this.label17.TabIndex = 43;
            this.label17.Text = "已结款总额";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label17.Visible = false;
            // 
            // label16
            // 
            this.label16.Location = new System.Drawing.Point(676, 45);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(85, 23);
            this.label16.TabIndex = 42;
            this.label16.Text = "退货款总额";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label16.Visible = false;
            // 
            // label15
            // 
            this.label15.Location = new System.Drawing.Point(676, 10);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(85, 23);
            this.label15.TabIndex = 41;
            this.label15.Text = "应付款总额";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label15.Visible = false;
            // 
            // CustomerCode
            // 
            this.CustomerCode.Location = new System.Drawing.Point(100, 42);
            this.CustomerCode.Name = "CustomerCode";
            this.CustomerCode.Size = new System.Drawing.Size(130, 23);
            this.CustomerCode.TabIndex = 49;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(10, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(85, 23);
            this.label1.TabIndex = 50;
            this.label1.Text = "客户编码";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Frmw_Customer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(874, 551);
            this.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.Name = "Frmw_Customer";
            this.Text = "Frmw_Customer";
            this.palTools.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabDataEdit.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.flowLayoutPanelTools.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.BeginCollect)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EndPayCollect)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TaxtRate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SortOrder)).EndInit();
            this.tabControl2.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.tabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).EndInit();
            this.tabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView4)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private TextBox CustomerName;
        private Label lblSupplierName;
        private TextBox ContactPerson;
        private Label lblContactPerson;
        private TextBox PhoneNumber;
        private Label lblPhoneNumber;
        private TextBox Email;
        private Label lblEmail;
        private NumericUpDown BeginCollect;
        private Label lblBeginCollect;
        private NumericUpDown EndPayCollect;
        private Label lblEndPayCollect;
        private NumericUpDown TaxtRate;
        private Label lblTaxtRate;
        private NumericUpDown SortOrder;
        private Label lblSortOrder;
        private CheckBox Status;
        private Label lblStatus;
        private TextBox Remark;
        private Label lblRemark;
        private TextBox Faxing;
        private Label lblFaxing;
        private TextBox Address;
        private Label lblAddress;
        private TextBox Bank;
        private Label lblBank;
        private TextBox TaxpayerNumber;
        private Label lblTaxpayerNumber;
        private TextBox BankAccount;
        private Label lblBankAccount;
        private TextBox LandlinePhone;
        private Label lblLandlinePhone;
        private TabControl tabControl2;
        private TabPage tabPage1;
        private DataGridView dataGridView1;
        private TabPage tabPage2;
        private DataGridView dataGridView2;
        private TabPage tabPage3;
        private DataGridView dataGridView3;
        private TabPage tabPage4;
        private DataGridView dataGridView4;
        private LinkLabel TotalPayable;
        private LinkLabel TotalAmountReturned;
        private LinkLabel TotalAmountPaid;
        private LinkLabel TotalAmountOutstanding;
        private Label label18;
        private Label label17;
        private Label label16;
        private Label label15;
        private TextBox CustomerCode;
        private Label label1;
    }
}