﻿namespace WinformDevFramework
{
    partial class FrmSupplier
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtSupplierName = new System.Windows.Forms.TextBox();
            this.txtContactPerson = new System.Windows.Forms.TextBox();
            this.联系人 = new System.Windows.Forms.Label();
            this.txtPhoneNumber = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtLandlinePhone = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtFaxing = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtBeginPay = new System.Windows.Forms.NumericUpDown();
            this.txtEndPay = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.txtTaxpayerNumber = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtTaxtRate = new System.Windows.Forms.NumericUpDown();
            this.txtBank = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtBankAccount = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtAddress = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtSortOrder = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.txtRemark = new System.Windows.Forms.RichTextBox();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.dataGridView3 = new System.Windows.Forms.DataGridView();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.dataGridView4 = new System.Windows.Forms.DataGridView();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.TotalAmountOutstanding = new System.Windows.Forms.LinkLabel();
            this.TotalAmountPaid = new System.Windows.Forms.LinkLabel();
            this.TotalAmountReturned = new System.Windows.Forms.LinkLabel();
            this.TotalPayable = new System.Windows.Forms.LinkLabel();
            this.label19 = new System.Windows.Forms.Label();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.SupplierCode = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.palTools.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabDataEdit.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.flowLayoutPanelTools.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtBeginPay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEndPay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTaxtRate)).BeginInit();
            this.tabControl2.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).BeginInit();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView4)).BeginInit();
            this.SuspendLayout();
            // 
            // palTools
            // 
            this.palTools.Size = new System.Drawing.Size(881, 31);
            // 
            // tabControl1
            // 
            this.tabControl1.Size = new System.Drawing.Size(881, 570);
            // 
            // tabList
            // 
            this.tabList.Size = new System.Drawing.Size(873, 540);
            // 
            // tabDataEdit
            // 
            this.tabDataEdit.Size = new System.Drawing.Size(873, 540);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.SupplierCode);
            this.groupBox1.Controls.Add(this.label20);
            this.groupBox1.Controls.Add(this.checkBox1);
            this.groupBox1.Controls.Add(this.label19);
            this.groupBox1.Controls.Add(this.TotalPayable);
            this.groupBox1.Controls.Add(this.TotalAmountReturned);
            this.groupBox1.Controls.Add(this.TotalAmountPaid);
            this.groupBox1.Controls.Add(this.TotalAmountOutstanding);
            this.groupBox1.Controls.Add(this.label18);
            this.groupBox1.Controls.Add(this.label17);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.tabControl2);
            this.groupBox1.Controls.Add(this.txtRemark);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.txtSortOrder);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.txtAddress);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.txtBankAccount);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.txtBank);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.txtTaxtRate);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.txtTaxpayerNumber);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.txtEndPay);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.txtBeginPay);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txtFaxing);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.txtEmail);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.txtLandlinePhone);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.txtPhoneNumber);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txtContactPerson);
            this.groupBox1.Controls.Add(this.联系人);
            this.groupBox1.Controls.Add(this.txtSupplierName);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Size = new System.Drawing.Size(867, 534);
            this.groupBox1.Controls.SetChildIndex(this.txtID, 0);
            this.groupBox1.Controls.SetChildIndex(this.label1, 0);
            this.groupBox1.Controls.SetChildIndex(this.txtSupplierName, 0);
            this.groupBox1.Controls.SetChildIndex(this.联系人, 0);
            this.groupBox1.Controls.SetChildIndex(this.txtContactPerson, 0);
            this.groupBox1.Controls.SetChildIndex(this.label3, 0);
            this.groupBox1.Controls.SetChildIndex(this.txtPhoneNumber, 0);
            this.groupBox1.Controls.SetChildIndex(this.label4, 0);
            this.groupBox1.Controls.SetChildIndex(this.txtLandlinePhone, 0);
            this.groupBox1.Controls.SetChildIndex(this.label5, 0);
            this.groupBox1.Controls.SetChildIndex(this.txtEmail, 0);
            this.groupBox1.Controls.SetChildIndex(this.label6, 0);
            this.groupBox1.Controls.SetChildIndex(this.txtFaxing, 0);
            this.groupBox1.Controls.SetChildIndex(this.label2, 0);
            this.groupBox1.Controls.SetChildIndex(this.txtBeginPay, 0);
            this.groupBox1.Controls.SetChildIndex(this.label7, 0);
            this.groupBox1.Controls.SetChildIndex(this.txtEndPay, 0);
            this.groupBox1.Controls.SetChildIndex(this.label8, 0);
            this.groupBox1.Controls.SetChildIndex(this.txtTaxpayerNumber, 0);
            this.groupBox1.Controls.SetChildIndex(this.label9, 0);
            this.groupBox1.Controls.SetChildIndex(this.txtTaxtRate, 0);
            this.groupBox1.Controls.SetChildIndex(this.label10, 0);
            this.groupBox1.Controls.SetChildIndex(this.txtBank, 0);
            this.groupBox1.Controls.SetChildIndex(this.label11, 0);
            this.groupBox1.Controls.SetChildIndex(this.txtBankAccount, 0);
            this.groupBox1.Controls.SetChildIndex(this.label12, 0);
            this.groupBox1.Controls.SetChildIndex(this.txtAddress, 0);
            this.groupBox1.Controls.SetChildIndex(this.label13, 0);
            this.groupBox1.Controls.SetChildIndex(this.txtSortOrder, 0);
            this.groupBox1.Controls.SetChildIndex(this.label14, 0);
            this.groupBox1.Controls.SetChildIndex(this.txtRemark, 0);
            this.groupBox1.Controls.SetChildIndex(this.tabControl2, 0);
            this.groupBox1.Controls.SetChildIndex(this.label15, 0);
            this.groupBox1.Controls.SetChildIndex(this.label16, 0);
            this.groupBox1.Controls.SetChildIndex(this.label17, 0);
            this.groupBox1.Controls.SetChildIndex(this.label18, 0);
            this.groupBox1.Controls.SetChildIndex(this.TotalAmountOutstanding, 0);
            this.groupBox1.Controls.SetChildIndex(this.TotalAmountPaid, 0);
            this.groupBox1.Controls.SetChildIndex(this.TotalAmountReturned, 0);
            this.groupBox1.Controls.SetChildIndex(this.TotalPayable, 0);
            this.groupBox1.Controls.SetChildIndex(this.label19, 0);
            this.groupBox1.Controls.SetChildIndex(this.checkBox1, 0);
            this.groupBox1.Controls.SetChildIndex(this.label20, 0);
            this.groupBox1.Controls.SetChildIndex(this.SupplierCode, 0);
            // 
            // flowLayoutPanelTools
            // 
            this.flowLayoutPanelTools.AutoSize = true;
            this.flowLayoutPanelTools.Size = new System.Drawing.Size(877, 27);
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(8, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(85, 23);
            this.label1.TabIndex = 1;
            this.label1.Text = "供应商名称";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtSupplierName
            // 
            this.txtSupplierName.Location = new System.Drawing.Point(99, 36);
            this.txtSupplierName.Name = "txtSupplierName";
            this.txtSupplierName.Size = new System.Drawing.Size(131, 23);
            this.txtSupplierName.TabIndex = 2;
            // 
            // txtContactPerson
            // 
            this.txtContactPerson.Location = new System.Drawing.Point(557, 35);
            this.txtContactPerson.Name = "txtContactPerson";
            this.txtContactPerson.Size = new System.Drawing.Size(131, 23);
            this.txtContactPerson.TabIndex = 4;
            // 
            // 联系人
            // 
            this.联系人.Location = new System.Drawing.Point(466, 34);
            this.联系人.Name = "联系人";
            this.联系人.Size = new System.Drawing.Size(85, 23);
            this.联系人.TabIndex = 3;
            this.联系人.Text = "联系人";
            this.联系人.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtPhoneNumber
            // 
            this.txtPhoneNumber.Location = new System.Drawing.Point(99, 76);
            this.txtPhoneNumber.Name = "txtPhoneNumber";
            this.txtPhoneNumber.Size = new System.Drawing.Size(131, 23);
            this.txtPhoneNumber.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(8, 75);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(85, 23);
            this.label3.TabIndex = 5;
            this.label3.Text = "手机号码";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtLandlinePhone
            // 
            this.txtLandlinePhone.Location = new System.Drawing.Point(327, 77);
            this.txtLandlinePhone.Name = "txtLandlinePhone";
            this.txtLandlinePhone.Size = new System.Drawing.Size(131, 23);
            this.txtLandlinePhone.TabIndex = 8;
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(236, 76);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(85, 23);
            this.label4.TabIndex = 7;
            this.label4.Text = "固定电话";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtEmail
            // 
            this.txtEmail.Location = new System.Drawing.Point(557, 78);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(131, 23);
            this.txtEmail.TabIndex = 10;
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(466, 77);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(85, 23);
            this.label5.TabIndex = 9;
            this.label5.Text = "电子邮箱";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtFaxing
            // 
            this.txtFaxing.Location = new System.Drawing.Point(99, 116);
            this.txtFaxing.Name = "txtFaxing";
            this.txtFaxing.Size = new System.Drawing.Size(131, 23);
            this.txtFaxing.TabIndex = 12;
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(8, 115);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(85, 23);
            this.label6.TabIndex = 11;
            this.label6.Text = "传真";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(8, 154);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(85, 23);
            this.label2.TabIndex = 13;
            this.label2.Text = "期初应付";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtBeginPay
            // 
            this.txtBeginPay.DecimalPlaces = 2;
            this.txtBeginPay.Location = new System.Drawing.Point(99, 155);
            this.txtBeginPay.Maximum = new decimal(new int[] {
            1215752192,
            23,
            0,
            0});
            this.txtBeginPay.Name = "txtBeginPay";
            this.txtBeginPay.Size = new System.Drawing.Size(120, 23);
            this.txtBeginPay.TabIndex = 15;
            // 
            // txtEndPay
            // 
            this.txtEndPay.DecimalPlaces = 2;
            this.txtEndPay.Location = new System.Drawing.Point(329, 155);
            this.txtEndPay.Maximum = new decimal(new int[] {
            1215752192,
            23,
            0,
            0});
            this.txtEndPay.Name = "txtEndPay";
            this.txtEndPay.Size = new System.Drawing.Size(120, 23);
            this.txtEndPay.TabIndex = 17;
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(238, 154);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(85, 23);
            this.label7.TabIndex = 16;
            this.label7.Text = "期末应付";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtTaxpayerNumber
            // 
            this.txtTaxpayerNumber.Location = new System.Drawing.Point(99, 196);
            this.txtTaxpayerNumber.Name = "txtTaxpayerNumber";
            this.txtTaxpayerNumber.Size = new System.Drawing.Size(131, 23);
            this.txtTaxpayerNumber.TabIndex = 19;
            // 
            // label8
            // 
            this.label8.Location = new System.Drawing.Point(8, 195);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(85, 23);
            this.label8.TabIndex = 18;
            this.label8.Text = "纳税人识别号";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label9
            // 
            this.label9.Location = new System.Drawing.Point(238, 196);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(85, 23);
            this.label9.TabIndex = 20;
            this.label9.Text = "税率(%)";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtTaxtRate
            // 
            this.txtTaxtRate.DecimalPlaces = 2;
            this.txtTaxtRate.Location = new System.Drawing.Point(329, 197);
            this.txtTaxtRate.Name = "txtTaxtRate";
            this.txtTaxtRate.Size = new System.Drawing.Size(120, 23);
            this.txtTaxtRate.TabIndex = 21;
            // 
            // txtBank
            // 
            this.txtBank.Location = new System.Drawing.Point(99, 235);
            this.txtBank.Name = "txtBank";
            this.txtBank.Size = new System.Drawing.Size(131, 23);
            this.txtBank.TabIndex = 23;
            // 
            // label10
            // 
            this.label10.Location = new System.Drawing.Point(8, 234);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(85, 23);
            this.label10.TabIndex = 22;
            this.label10.Text = "开户行";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtBankAccount
            // 
            this.txtBankAccount.Location = new System.Drawing.Point(329, 235);
            this.txtBankAccount.Name = "txtBankAccount";
            this.txtBankAccount.Size = new System.Drawing.Size(131, 23);
            this.txtBankAccount.TabIndex = 25;
            // 
            // label11
            // 
            this.label11.Location = new System.Drawing.Point(238, 234);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(85, 23);
            this.label11.TabIndex = 24;
            this.label11.Text = "银行账号";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtAddress
            // 
            this.txtAddress.Location = new System.Drawing.Point(329, 117);
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Size = new System.Drawing.Size(131, 23);
            this.txtAddress.TabIndex = 27;
            // 
            // label12
            // 
            this.label12.Location = new System.Drawing.Point(238, 116);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(85, 23);
            this.label12.TabIndex = 26;
            this.label12.Text = "地址";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtSortOrder
            // 
            this.txtSortOrder.Location = new System.Drawing.Point(557, 117);
            this.txtSortOrder.Name = "txtSortOrder";
            this.txtSortOrder.Size = new System.Drawing.Size(131, 23);
            this.txtSortOrder.TabIndex = 29;
            this.txtSortOrder.Text = "1";
            // 
            // label13
            // 
            this.label13.Location = new System.Drawing.Point(466, 116);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(85, 23);
            this.label13.TabIndex = 28;
            this.label13.Text = "排序";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label14
            // 
            this.label14.Location = new System.Drawing.Point(8, 274);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(85, 23);
            this.label14.TabIndex = 30;
            this.label14.Text = "备注";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtRemark
            // 
            this.txtRemark.Location = new System.Drawing.Point(99, 274);
            this.txtRemark.Name = "txtRemark";
            this.txtRemark.Size = new System.Drawing.Size(361, 96);
            this.txtRemark.TabIndex = 31;
            this.txtRemark.Text = "";
            // 
            // tabControl2
            // 
            this.tabControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl2.Controls.Add(this.tabPage1);
            this.tabControl2.Controls.Add(this.tabPage2);
            this.tabControl2.Controls.Add(this.tabPage3);
            this.tabControl2.Controls.Add(this.tabPage4);
            this.tabControl2.Location = new System.Drawing.Point(0, 376);
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(870, 161);
            this.tabControl2.TabIndex = 32;
            this.tabControl2.Visible = false;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.dataGridView1);
            this.tabPage1.Location = new System.Drawing.Point(4, 26);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(862, 131);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "应付款";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.GridColor = System.Drawing.SystemColors.Control;
            this.dataGridView1.Location = new System.Drawing.Point(3, 3);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.RowTemplate.Height = 25;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(856, 125);
            this.dataGridView1.TabIndex = 0;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.dataGridView2);
            this.tabPage2.Location = new System.Drawing.Point(4, 26);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(781, 0);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "退货款";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // dataGridView2
            // 
            this.dataGridView2.AllowUserToAddRows = false;
            this.dataGridView2.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView2.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView2.GridColor = System.Drawing.SystemColors.Control;
            this.dataGridView2.Location = new System.Drawing.Point(3, 3);
            this.dataGridView2.MultiSelect = false;
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.ReadOnly = true;
            this.dataGridView2.RowHeadersVisible = false;
            this.dataGridView2.RowTemplate.Height = 25;
            this.dataGridView2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView2.Size = new System.Drawing.Size(775, 0);
            this.dataGridView2.TabIndex = 1;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.dataGridView3);
            this.tabPage3.Location = new System.Drawing.Point(4, 26);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(781, 0);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "已结款";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // dataGridView3
            // 
            this.dataGridView3.AllowUserToAddRows = false;
            this.dataGridView3.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView3.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridView3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView3.GridColor = System.Drawing.SystemColors.Control;
            this.dataGridView3.Location = new System.Drawing.Point(3, 3);
            this.dataGridView3.MultiSelect = false;
            this.dataGridView3.Name = "dataGridView3";
            this.dataGridView3.ReadOnly = true;
            this.dataGridView3.RowHeadersVisible = false;
            this.dataGridView3.RowTemplate.Height = 25;
            this.dataGridView3.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView3.Size = new System.Drawing.Size(775, 0);
            this.dataGridView3.TabIndex = 1;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.dataGridView4);
            this.tabPage4.Location = new System.Drawing.Point(4, 26);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(781, 0);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "未结款";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // dataGridView4
            // 
            this.dataGridView4.AllowUserToAddRows = false;
            this.dataGridView4.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView4.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridView4.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView4.GridColor = System.Drawing.SystemColors.Control;
            this.dataGridView4.Location = new System.Drawing.Point(3, 3);
            this.dataGridView4.MultiSelect = false;
            this.dataGridView4.Name = "dataGridView4";
            this.dataGridView4.ReadOnly = true;
            this.dataGridView4.RowHeadersVisible = false;
            this.dataGridView4.RowTemplate.Height = 25;
            this.dataGridView4.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView4.Size = new System.Drawing.Size(775, 0);
            this.dataGridView4.TabIndex = 1;
            // 
            // label15
            // 
            this.label15.Location = new System.Drawing.Point(466, 195);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(85, 23);
            this.label15.TabIndex = 33;
            this.label15.Text = "应付款总额";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label15.Visible = false;
            // 
            // label16
            // 
            this.label16.Location = new System.Drawing.Point(466, 274);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(85, 23);
            this.label16.TabIndex = 34;
            this.label16.Text = "退货款总额";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label16.Visible = false;
            // 
            // label17
            // 
            this.label17.Location = new System.Drawing.Point(466, 237);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(85, 23);
            this.label17.TabIndex = 35;
            this.label17.Text = "已结款总额";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label17.Visible = false;
            // 
            // label18
            // 
            this.label18.Location = new System.Drawing.Point(466, 316);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(85, 23);
            this.label18.TabIndex = 36;
            this.label18.Text = "未结款总额";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label18.Visible = false;
            // 
            // TotalAmountOutstanding
            // 
            this.TotalAmountOutstanding.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.TotalAmountOutstanding.Location = new System.Drawing.Point(557, 319);
            this.TotalAmountOutstanding.Name = "TotalAmountOutstanding";
            this.TotalAmountOutstanding.Size = new System.Drawing.Size(115, 17);
            this.TotalAmountOutstanding.TabIndex = 37;
            this.TotalAmountOutstanding.TabStop = true;
            this.TotalAmountOutstanding.Text = "0.00";
            this.TotalAmountOutstanding.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.TotalAmountOutstanding.Visible = false;
            this.TotalAmountOutstanding.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.TotalAmountOutstanding_LinkClicked);
            // 
            // TotalAmountPaid
            // 
            this.TotalAmountPaid.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.TotalAmountPaid.Location = new System.Drawing.Point(557, 240);
            this.TotalAmountPaid.Name = "TotalAmountPaid";
            this.TotalAmountPaid.Size = new System.Drawing.Size(116, 17);
            this.TotalAmountPaid.TabIndex = 38;
            this.TotalAmountPaid.TabStop = true;
            this.TotalAmountPaid.Text = "0.00";
            this.TotalAmountPaid.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.TotalAmountPaid.Visible = false;
            this.TotalAmountPaid.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.TotalAmountPaid_LinkClicked);
            // 
            // TotalAmountReturned
            // 
            this.TotalAmountReturned.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.TotalAmountReturned.Location = new System.Drawing.Point(557, 277);
            this.TotalAmountReturned.Name = "TotalAmountReturned";
            this.TotalAmountReturned.Size = new System.Drawing.Size(115, 17);
            this.TotalAmountReturned.TabIndex = 39;
            this.TotalAmountReturned.TabStop = true;
            this.TotalAmountReturned.Text = "0.00";
            this.TotalAmountReturned.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.TotalAmountReturned.Visible = false;
            this.TotalAmountReturned.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.TotalAmountReturned_LinkClicked);
            // 
            // TotalPayable
            // 
            this.TotalPayable.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.TotalPayable.Location = new System.Drawing.Point(557, 197);
            this.TotalPayable.Name = "TotalPayable";
            this.TotalPayable.Size = new System.Drawing.Size(116, 17);
            this.TotalPayable.TabIndex = 40;
            this.TotalPayable.TabStop = true;
            this.TotalPayable.Text = "0.00";
            this.TotalPayable.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.TotalPayable.Visible = false;
            this.TotalPayable.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.TotalPayable_LinkClicked);
            // 
            // label19
            // 
            this.label19.Location = new System.Drawing.Point(466, 154);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(85, 23);
            this.label19.TabIndex = 41;
            this.label19.Text = "状态";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(557, 156);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(75, 21);
            this.checkBox1.TabIndex = 42;
            this.checkBox1.Text = "是否启用";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // SupplierCode
            // 
            this.SupplierCode.Location = new System.Drawing.Point(329, 35);
            this.SupplierCode.Name = "SupplierCode";
            this.SupplierCode.Size = new System.Drawing.Size(131, 23);
            this.SupplierCode.TabIndex = 44;
            // 
            // label20
            // 
            this.label20.Location = new System.Drawing.Point(238, 34);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(85, 23);
            this.label20.TabIndex = 43;
            this.label20.Text = "供应商编码";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // FrmSupplier
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(881, 601);
            this.Name = "FrmSupplier";
            this.Text = "FrmSupplier";
            this.palTools.ResumeLayout(false);
            this.palTools.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabDataEdit.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.flowLayoutPanelTools.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtBeginPay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEndPay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTaxtRate)).EndInit();
            this.tabControl2.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.tabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).EndInit();
            this.tabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView4)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private TextBox txtSupplierName;
        private Label label1;
        private TextBox txtContactPerson;
        private Label 联系人;
        private TextBox txtPhoneNumber;
        private Label label3;
        private TextBox txtFaxing;
        private Label label6;
        private TextBox txtEmail;
        private Label label5;
        private TextBox txtLandlinePhone;
        private Label label4;
        private Label label2;
        private NumericUpDown txtBeginPay;
        private NumericUpDown txtEndPay;
        private Label label7;
        private Label label9;
        private TextBox txtTaxpayerNumber;
        private Label label8;
        private NumericUpDown txtTaxtRate;
        private TextBox txtBankAccount;
        private Label label11;
        private TextBox txtBank;
        private Label label10;
        private TextBox txtAddress;
        private Label label12;
        private TextBox txtSortOrder;
        private Label label13;
        private Label label14;
        private RichTextBox txtRemark;
        private TabControl tabControl2;
        private TabPage tabPage1;
        private TabPage tabPage2;
        private TabPage tabPage3;
        private TabPage tabPage4;
        private DataGridView dataGridView1;
        private DataGridView dataGridView2;
        private DataGridView dataGridView3;
        private DataGridView dataGridView4;
        private Label label18;
        private Label label17;
        private Label label16;
        private Label label15;
        private LinkLabel TotalAmountOutstanding;
        private LinkLabel TotalPayable;
        private LinkLabel TotalAmountReturned;
        private LinkLabel TotalAmountPaid;
        private Label label19;
        private CheckBox checkBox1;
        private TextBox SupplierCode;
        private Label label20;
    }
}