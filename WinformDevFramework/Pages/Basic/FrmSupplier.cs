﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WinformDevFramework.Core.Winform;
using WinformDevFramework.IServices;
using WinformDevFramework.Services;

namespace WinformDevFramework
{
    public partial class FrmSupplier : BaseForm1
    {
        private Iw_SupplierServices _iwSupplierServices;
        public FrmSupplier(Iw_SupplierServices iwSupplierServices)
        {
            _iwSupplierServices = iwSupplierServices;
            InitializeComponent();
        }

        public override void Init()
        {
            base.Init();
            this.dataGridViewList.DataSource = GetData();
            this.dataGridViewList.Columns["SupplierName"]!.HeaderText = "名称";
            this.dataGridViewList.Columns["ContactPerson"]!.HeaderText = "联系人";
            this.dataGridViewList.Columns["PhoneNumber"].HeaderText = "手机号码";
            this.dataGridViewList.Columns["Email"]!.HeaderText = "邮箱";
            this.dataGridViewList.Columns["BeginPay"]!.HeaderText = "期初应付";
            this.dataGridViewList.Columns["EndPay"].HeaderText = "期末应付";
            this.dataGridViewList.Columns["TaxtRate"]!.HeaderText = "税率";
            this.dataGridViewList.Columns["SortOrder"]!.HeaderText = "排序";
            this.dataGridViewList.Columns["Status"].HeaderText = "状态";
            this.dataGridViewList.Columns["Faxing"]!.HeaderText = "传真";
            this.dataGridViewList.Columns["Address"]!.HeaderText = "地址";
            this.dataGridViewList.Columns["Bank"].HeaderText = "开户行"; 
            this.dataGridViewList.Columns["SupplierCode"].HeaderText = "供应商编码"; 
            this.dataGridViewList.Columns["TaxpayerNumber"]!.HeaderText = "纳税人识别号";
            this.dataGridViewList.Columns["BankAccount"]!.HeaderText = "银行账号";
            this.dataGridViewList.Columns["LandlinePhone"].HeaderText = "固定电话";
            this.dataGridViewList.Columns["Remark"].HeaderText = "备注";


            this.dataGridViewList.Columns["TotalPayable"].Visible = false;
            this.dataGridViewList.Columns["TotalAmountReturned"].Visible = false;
            this.dataGridViewList.Columns["TotalAmountPaid"].Visible = false;
            this.dataGridViewList.Columns["TotalAmountOutstanding"].Visible = false;

            SetToolButtonStatus(formStatus);
            SetToolsButton();
        }

        public override void TabControlSelectingFunction(object? sender, TabControlCancelEventArgs e)
        {
            base.TabControlSelectingFunction(sender, e);
        }

        public override void DataGridViewListDoubleClick(object? sender, DataGridViewCellEventArgs e)
        {
            base.DataGridViewListDoubleClick(sender, e);
            var g = (DataGridView)sender;
            var model = g.CurrentRow.DataBoundItem as w_Supplier;
            //给详情页设置数据
            this.txtID.Text = model.ID.ToString();
            txtSupplierName.Text = model.SupplierName;
            SupplierCode.Text = model.SupplierCode;
            txtContactPerson.Text = model.ContactPerson;
            txtPhoneNumber.Text = model.PhoneNumber;
            txtEmail.Text = model.Email;
            txtBeginPay.Value = model.BeginPay ?? 0;
            txtEndPay.Value = model.EndPay ?? 0;
            txtTaxtRate.Value = model.TaxtRate ?? 0;
            txtSortOrder.Text = model.SortOrder.ToString();
            checkBox1.Checked = model.Status.Value;
            txtFaxing.Text = model.Faxing;
            txtAddress.Text = model.Address;
            txtBank.Text = model.Bank;
            txtTaxpayerNumber.Text = model.TaxpayerNumber;
            txtBankAccount.Text = model.BankAccount;
            txtLandlinePhone.Text = model.LandlinePhone;
            TotalPayable.Text = model.TotalPayable.ToString();
            TotalAmountReturned.Text = model.TotalAmountReturned.ToString();
            TotalAmountPaid.Text = model.TotalAmountPaid.ToString();
            TotalAmountOutstanding.Text = model.TotalAmountOutstanding.ToString();
            txtRemark.Text = model.Remark;
            //TODO设置tab页数据

            SetToolButtonStatus(formStatus);
            
        }

        public override void AddFunction(object sender, EventArgs e)
        {
            base.AddFunction(sender, e);
            SetToolButtonStatus(formStatus);
        }

        public override void EditFunction(object sender, EventArgs e)
        {
            base.EditFunction(sender, e);
            SetToolButtonStatus(formStatus);
            SupplierCode.ReadOnly = true;
        }

        public override void SaveFunction(object sender, EventArgs e)
        {
            base.SaveFunction(sender, e);
            //新增
            if (string.IsNullOrEmpty(txtID.Text))
            {
                w_Supplier model = new w_Supplier();
                model.SupplierName = txtSupplierName.Text; 
                model.ContactPerson= txtContactPerson.Text;
                model.PhoneNumber= txtPhoneNumber.Text;
                model.Email= txtEmail.Text;
                model.BeginPay = txtBeginPay.Value;
                model.EndPay = txtEndPay.Value;
                model.TaxtRate = txtTaxtRate.Value;
                model.SortOrder= int.Parse(string.IsNullOrEmpty(txtSortOrder.Text)?"1": txtSortOrder.Text);
                model.Status= checkBox1.Checked;
                model.Faxing= txtFaxing.Text;
                model.Address= txtAddress.Text;
                model.Bank= txtBank.Text;
                model.TaxpayerNumber= txtTaxpayerNumber.Text;
                model.BankAccount= txtBankAccount.Text; 
                model.LandlinePhone= txtLandlinePhone.Text;
                model.Remark= txtRemark.Text;
                model.SupplierCode = SupplierCode.Text;
                var id = _iwSupplierServices.Insert(model);
                txtID.Text = id.ToString();
                if (id > 0)
                {
                    MessageBox.Show("保存成功！", "提示");
                }
            }
            // 修改
            else
            {
                w_Supplier model = _iwSupplierServices.QueryById(int.Parse(txtID.Text));
                model.SupplierName = txtSupplierName.Text;
                model.ContactPerson = txtContactPerson.Text;
                model.PhoneNumber = txtPhoneNumber.Text;
                model.Email = txtEmail.Text;
                model.BeginPay = txtBeginPay.Value;
                model.EndPay = txtEndPay.Value;
                model.TaxtRate = txtTaxtRate.Value;
                model.SortOrder = int.Parse(txtSortOrder.Text);
                model.Status = checkBox1.Checked;
                model.Faxing = txtFaxing.Text;
                model.Address = txtAddress.Text;
                model.Bank = txtBank.Text;
                model.TaxpayerNumber = txtTaxpayerNumber.Text;
                model.BankAccount = txtBankAccount.Text;
                model.LandlinePhone = txtLandlinePhone.Text;
                model.Remark = txtRemark.Text;
                model.SupplierCode = SupplierCode.Text;
                if (_iwSupplierServices.Update(model))
                {
                    MessageBox.Show("保存成功！", "提示");
                }
            }
            SetToolButtonStatus(formStatus);
            this.dataGridViewList.DataSource = GetData();
        }

        public override void CanelFunction(object sender, EventArgs e)
        {
            bool isAdd = formStatus == FormStatus.Add;
            base.CanelFunction(sender, e);
            SetToolButtonStatus(formStatus);
            btnEdit.Enabled = !isAdd;
            btnDel.Enabled= !isAdd;
        }

        public override void DelFunction(object sender, EventArgs e)
        {
            base.DelFunction(sender, e);
            var result = MessageBox.Show("是否删除？", "确认", MessageBoxButtons.YesNoCancel);
            if (result == DialogResult.Yes)
            {
                _iwSupplierServices.DeleteById(Int32.Parse(txtID.Text));
                formStatus = FormStatus.Del;
                SetToolButtonStatus(formStatus);
                this.dataGridViewList.DataSource = GetData();
            }
        }

        public override void SetToolButtonStatus(FormStatus status)
        {
            base.SetToolButtonStatus(status);
            switch (status)
            {
                case FormStatus.Add:
                    {
                        btnAdd.Enabled = false;
                        btnEdit.Enabled = false;
                        btnSave.Enabled = true;
                        btnDel.Enabled = false;
                        btnCanel.Enabled = true;
                        SetControlStatus(this.groupBox1, true);
                        ClearControlsText(this.groupBox1);
                        break;
                    }
                case FormStatus.Edit:
                    {
                        btnAdd.Enabled = false;
                        btnEdit.Enabled = false;
                        btnSave.Enabled = true;
                        btnDel.Enabled = false;
                        btnCanel.Enabled = true;
                        SetControlStatus(this.groupBox1, true);
                        break;
                    }
                case FormStatus.View:
                    {
                        btnAdd.Enabled = true;
                        btnEdit.Enabled = true;
                        btnSave.Enabled = false;
                        btnDel.Enabled = true;
                        btnCanel.Enabled = false;
                        SetControlStatus(this.groupBox1, false);
                        break;
                    }
                case FormStatus.Canel:
                    {
                        btnAdd.Enabled = true;
                        btnEdit.Enabled = true;
                        btnSave.Enabled = false;
                        btnDel.Enabled = false;
                        btnCanel.Enabled = false;
                        SetControlStatus(this.groupBox1, false);
                        break;
                    }
                case FormStatus.First:
                    {
                        btnAdd.Enabled = true;
                        btnEdit.Enabled = false;
                        btnSave.Enabled = false;
                        btnDel.Enabled = false;
                        btnCanel.Enabled = false;
                        SetControlStatus(this.groupBox1, false);
                        break;
                    }
                case FormStatus.Save:
                    {
                        btnAdd.Enabled = true;
                        btnEdit.Enabled = true;
                        btnSave.Enabled = false;
                        btnDel.Enabled = true;
                        btnCanel.Enabled = false;
                        SetControlStatus(this.groupBox1, false);
                        break;
                    }
                case FormStatus.Del:
                    {
                        btnAdd.Enabled = true;
                        btnEdit.Enabled = false;
                        btnSave.Enabled = false;
                        btnDel.Enabled = false;
                        btnCanel.Enabled = false;
                        SetControlStatus(this.groupBox1, false);
                        ClearControlsText(this.groupBox1);
                        break;
                    }
            }
        }
        public override bool ValidateData()
        {
            if (string.IsNullOrEmpty(txtSupplierName.Text))
            {
                MessageBox.Show("字典内容不能为空！");
                return false;
            }
            return true;
        }
        /// <summary>
        /// 获取数据
        /// </summary>
        /// <returns></returns>
        private List<w_Supplier> GetData()
        {
            List<w_Supplier> data = new List<w_Supplier>();
            data = _iwSupplierServices.Query();
            return data;
        }

        private void TotalAmountPaid_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            tabControl2.SelectedTab = tabPage3;
        }

        private void TotalPayable_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            tabControl2.SelectedTab = tabPage1;
        }

        private void TotalAmountReturned_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            tabControl2.SelectedTab = tabPage2;
        }

        private void TotalAmountOutstanding_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            tabControl2.SelectedTab = tabPage4;
        }
    }
}
