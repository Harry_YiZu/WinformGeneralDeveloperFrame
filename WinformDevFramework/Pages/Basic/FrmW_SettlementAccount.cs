using SqlSugar;
using WinformDevFramework.Core.Winform;
using WinformDevFramework.IServices;
using WinformDevFramework;

namespace WinformDevFramework
{
    public partial class Frmw_SettlementAccount : BaseForm1
    {
        private Iw_SettlementAccountServices _w_SettlementAccountServices;
        public Frmw_SettlementAccount(Iw_SettlementAccountServices w_SettlementAccountServices)
        {
            _w_SettlementAccountServices= w_SettlementAccountServices;
            InitializeComponent();
        }

        public override void Init()
        {
            base.Init();
            //查询数据
            this.dataGridViewList.DataSource = GetData();
           
            // 设置隐藏列
            this.dataGridViewList.Columns["Code"].HeaderText = "编号";
            this.dataGridViewList.Columns["Name"].HeaderText = "名称";
            this.dataGridViewList.Columns["BeginAmount"].HeaderText = "期初金额";
            this.dataGridViewList.Columns["NowAmount"].HeaderText = "当前余额";
            this.dataGridViewList.Columns["SortOrder"].HeaderText = "排序";
            this.dataGridViewList.Columns["Remark"].HeaderText = "备注";
            SetToolButtonStatus(formStatus);
            SetToolsButton();
        }

        public override void TabControlSelectingFunction(object? sender, TabControlCancelEventArgs e)
        {
            base.TabControlSelectingFunction(sender, e);
        }

        public override void DataGridViewListDoubleClick(object? sender, DataGridViewCellEventArgs e)
        {
            base.DataGridViewListDoubleClick(sender, e);
            var g = (DataGridView)sender;
            var model = g.CurrentRow.DataBoundItem as w_SettlementAccount;

            //给详情页设置数据
            this.txtID.Text = model.ID.ToString();           
            this.Code.Text = model.Code.ToString();  
            this.CName.Text = model.Name.ToString();  
            this.BeginAmount.Text = model.BeginAmount.ToString();  
            this.NowAmount.Text = model.NowAmount.ToString();  
            this.SortOrder.Text = model.SortOrder.ToString();  
            this.Remark.Text = model.Remark.ToString();  
            SetToolButtonStatus(formStatus);
        }

        public override void AddFunction(object sender, EventArgs e)
        {
            base.AddFunction(sender, e);
            SetToolButtonStatus(formStatus);
        }

        public override void EditFunction(object sender, EventArgs e)
        {
            base.EditFunction(sender, e);
            SetToolButtonStatus(formStatus);
            Code.ReadOnly = true;

        }

        public override void SaveFunction(object sender, EventArgs e)
        {
            base.SaveFunction(sender, e);
            {
                //新增
                if (string.IsNullOrEmpty(txtID.Text))
                {
                    w_SettlementAccount model = new w_SettlementAccount();
                    // TODO获取界面的数据
                    model.Code=this.Code.Text;
                    model.Name=this.CName.Text;
                    model.BeginAmount=this.BeginAmount.Value;
                    model.NowAmount=this.NowAmount.Value;
                    model.SortOrder=this.SortOrder.ToInt32();
                    model.Remark=this.Remark.Text;
                    var id = _w_SettlementAccountServices.Insert(model);
                    txtID.Text = id.ToString();
                    if (id > 0)
                    {
                        MessageBox.Show("保存成功！", "提示");
                    }
                }
                // 修改
                else
                {
                    w_SettlementAccount model = _w_SettlementAccountServices.QueryById(int.Parse(txtID.Text));
                    // TODO获取界面的数据
                    model.Code = this.Code.Text;
                    model.Name = this.CName.Text;
                    model.BeginAmount = this.BeginAmount.Value;
                    model.NowAmount = this.NowAmount.Value;
                    model.SortOrder = this.SortOrder.ToInt32();
                    model.Remark=this.Remark.Text;
                    if (_w_SettlementAccountServices.Update(model))
                    {
                        MessageBox.Show("保存成功！", "提示");
                    }
                }
                SetToolButtonStatus(formStatus);
                //列表重新赋值
                this.dataGridViewList.DataSource = GetData();
            }
        }

        public override void CanelFunction(object sender, EventArgs e)
        {
            bool isAdd = formStatus == FormStatus.Add;
            base.CanelFunction(sender, e);
            SetToolButtonStatus(formStatus);
            btnEdit.Enabled = !isAdd;
            btnDel.Enabled = !isAdd;
        }

        public override void DelFunction(object sender, EventArgs e)
        {
            base.DelFunction(sender, e);
            var result = MessageBox.Show("是否删除？", "确认", MessageBoxButtons.YesNoCancel);
            if (result == DialogResult.Yes)
            {
                _w_SettlementAccountServices.DeleteById(Int32.Parse(txtID.Text));
                formStatus = FormStatus.Del;
                SetToolButtonStatus(formStatus);
                //列表重新赋值
                this.dataGridViewList.DataSource = GetData();
            }
        }

        public override void SetToolButtonStatus(FormStatus status)
        {
            base.SetToolButtonStatus(status);
            switch (status)
            {
                case FormStatus.Add:
                    {
                        btnAdd.Enabled = false;
                        btnEdit.Enabled = false;
                        btnSave.Enabled = true;
                        btnDel.Enabled = false;
                        btnCanel.Enabled = true;
                        SetControlStatus(this.groupBox1, true);
                        ClearControlsText(this.groupBox1);
                        break;
                    }
                case FormStatus.Edit:
                    {
                        btnAdd.Enabled = false;
                        btnEdit.Enabled = false;
                        btnSave.Enabled = true;
                        btnDel.Enabled = false;
                        btnCanel.Enabled = true;
                        SetControlStatus(this.groupBox1, true);
                        break;
                    }
                case FormStatus.View:
                    {
                        btnAdd.Enabled = true;
                        btnEdit.Enabled = true;
                        btnSave.Enabled = false;
                        btnDel.Enabled = true;
                        btnCanel.Enabled = false;
                        SetControlStatus(this.groupBox1, false);
                        break;
                    }
                case FormStatus.Canel:
                    {
                        btnAdd.Enabled = true;
                        btnEdit.Enabled = true;
                        btnSave.Enabled = false;
                        btnDel.Enabled = false;
                        btnCanel.Enabled = false;
                        SetControlStatus(this.groupBox1, false);
                        break;
                    }
                case FormStatus.First:
                    {
                        btnAdd.Enabled = true;
                        btnEdit.Enabled = false;
                        btnSave.Enabled = false;
                        btnDel.Enabled = false;
                        btnCanel.Enabled = false;
                        SetControlStatus(this.groupBox1, false);
                        break;
                    }
                case FormStatus.Save:
                    {
                        btnAdd.Enabled = true;
                        btnEdit.Enabled = true;
                        btnSave.Enabled = false;
                        btnDel.Enabled = true;
                        btnCanel.Enabled = false;
                        SetControlStatus(this.groupBox1, false);
                        break;
                    }
                case FormStatus.Del:
                    {
                        btnAdd.Enabled = true;
                        btnEdit.Enabled = false;
                        btnSave.Enabled = false;
                        btnDel.Enabled = false;
                        btnCanel.Enabled = false;
                        SetControlStatus(this.groupBox1, false);
                        ClearControlsText(this.groupBox1);
                        break;
                    }
            }
        }

        public override bool ValidateData()
        {
            if (string.IsNullOrEmpty(Code.Text))
            {
                MessageBox.Show("Code不能为空！");
                return false;
            }
            if (string.IsNullOrEmpty(CName.Text))
            {
                MessageBox.Show("Name不能为空！");
                return false;
            }
            return true;
        }
        /// <summary>
        /// 获取数据
        /// </summary>
        /// <returns></returns>
        private List<w_SettlementAccount> GetData()
        {
            List<w_SettlementAccount> data = new List<w_SettlementAccount>();
            data = _w_SettlementAccountServices.Query();
            return data;
        }
    }
}
