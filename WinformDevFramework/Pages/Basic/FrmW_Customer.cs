using SqlSugar;
using WinformDevFramework.Core.Winform;
using WinformDevFramework.IServices;
using WinformDevFramework;

namespace WinformDevFramework
{
    public partial class Frmw_Customer : BaseForm1
    {
        private Iw_CustomerServices _w_CustomerServices;
        public Frmw_Customer(Iw_CustomerServices w_CustomerServices)
        {
            _w_CustomerServices = w_CustomerServices;
            InitializeComponent();
        }

        public override void Init()
        {
            base.Init();
            //查询数据
            this.dataGridViewList.DataSource = GetData();

            // 设置隐藏列
            this.dataGridViewList.Columns["CustomerName"].HeaderText = "名称";
            this.dataGridViewList.Columns["ContactPerson"].HeaderText = "联系人";
            this.dataGridViewList.Columns["PhoneNumber"].HeaderText = "手机号码";
            this.dataGridViewList.Columns["Email"].HeaderText = "邮箱";
            this.dataGridViewList.Columns["BeginCollect"].HeaderText = "期初应收";
            this.dataGridViewList.Columns["EndPayCollect"].HeaderText = "期末应收";
            this.dataGridViewList.Columns["TaxtRate"].HeaderText = "税率";
            this.dataGridViewList.Columns["SortOrder"].HeaderText = "排序";
            this.dataGridViewList.Columns["Status"].HeaderText = "状态";
            this.dataGridViewList.Columns["Remark"].HeaderText = "备注";
            this.dataGridViewList.Columns["Faxing"].HeaderText = "传真";
            this.dataGridViewList.Columns["Address"].HeaderText = "地址";
            this.dataGridViewList.Columns["Bank"].HeaderText = "开户行";
            this.dataGridViewList.Columns["TaxpayerNumber"].HeaderText = "纳税人识别号";
            this.dataGridViewList.Columns["BankAccount"].HeaderText = "银行账号";
            this.dataGridViewList.Columns["LandlinePhone"].HeaderText = "固定电话";
            this.dataGridViewList.Columns["CustomerCode"].HeaderText = "客户编码";
            this.dataGridViewList.Columns["TotalPayable"].Visible = false;
            this.dataGridViewList.Columns["TotalAmountReturned"].Visible = false;
            this.dataGridViewList.Columns["TotalAmountPaid"].Visible = false;
            this.dataGridViewList.Columns["TotalAmountOutstanding"].Visible = false;

            SetToolButtonStatus(formStatus);
            SetToolsButton();
        }

        public override void TabControlSelectingFunction(object? sender, TabControlCancelEventArgs e)
        {
            base.TabControlSelectingFunction(sender, e);
        }

        public override void DataGridViewListDoubleClick(object? sender, DataGridViewCellEventArgs e)
        {
            base.DataGridViewListDoubleClick(sender, e);
            var g = (DataGridView)sender;
            var model = g.CurrentRow.DataBoundItem as w_Customer;

            //给详情页设置数据
            this.txtID.Text = model.ID.ToString();
            this.CustomerName.Text = model.CustomerName.ToString();
            this.CustomerCode.Text = model.CustomerCode;
            this.ContactPerson.Text = model.ContactPerson.ToString();
            this.PhoneNumber.Text = model.PhoneNumber.ToString();
            this.Email.Text = model.Email.ToString();
            this.BeginCollect.Text = model.BeginCollect.ToString();
            this.EndPayCollect.Text = model.EndPayCollect.ToString();
            this.TaxtRate.Text = model.TaxtRate.ToString();
            this.SortOrder.Text = model.SortOrder.ToString();
            this.Status.Checked = model.Status.Value;
            this.Remark.Text = model.Remark.ToString();
            this.Faxing.Text = model.Faxing.ToString();
            this.Address.Text = model.Address.ToString();
            this.Bank.Text = model.Bank.ToString();
            this.TaxpayerNumber.Text = model.TaxpayerNumber.ToString();
            this.BankAccount.Text = model.BankAccount.ToString();
            this.LandlinePhone.Text = model.LandlinePhone.ToString();
            SetToolButtonStatus(formStatus);
        }

        public override void AddFunction(object sender, EventArgs e)
        {
            base.AddFunction(sender, e);
            SetToolButtonStatus(formStatus);
        }

        public override void EditFunction(object sender, EventArgs e)
        {
            base.EditFunction(sender, e);
            SetToolButtonStatus(formStatus);
            CustomerCode.ReadOnly = true;
        }

        public override void SaveFunction(object sender, EventArgs e)
        {
            base.SaveFunction(sender, e);
            {
                //新增
                if (string.IsNullOrEmpty(txtID.Text))
                {
                    w_Customer model = new w_Customer();
                    // TODO获取界面的数据
                    model.CustomerName = this.CustomerName.Text;
                    model.CustomerCode = this.CustomerCode.Text;
                    model.ContactPerson = this.ContactPerson.Text;
                    model.PhoneNumber = this.PhoneNumber.Text;
                    model.Email = this.Email.Text;
                    model.BeginCollect = this.BeginCollect.Value;
                    model.EndPayCollect = this.EndPayCollect.Value;
                    model.TaxtRate = this.TaxtRate.Value;
                    model.SortOrder = this.SortOrder.ToInt32();
                    model.Status = this.Status.Checked;
                    model.Remark = this.Remark.Text;
                    model.Faxing = this.Faxing.Text;
                    model.Address = this.Address.Text;
                    model.Bank = this.Bank.Text;
                    model.TaxpayerNumber = this.TaxpayerNumber.Text;
                    model.BankAccount = this.BankAccount.Text;
                    model.LandlinePhone = this.LandlinePhone.Text;
                    var id = _w_CustomerServices.Insert(model);
                    txtID.Text = id.ToString();
                    if (id > 0)
                    {
                        MessageBox.Show("保存成功！", "提示");
                    }
                }
                // 修改
                else
                {
                    w_Customer model = _w_CustomerServices.QueryById(int.Parse(txtID.Text));
                    // TODO获取界面的数据
                    model.CustomerName = this.CustomerName.Text;
                    model.CustomerCode = this.CustomerCode.Text;
                    model.ContactPerson = this.ContactPerson.Text;
                    model.PhoneNumber = this.PhoneNumber.Text;
                    model.Email = this.Email.Text;
                    model.BeginCollect = this.BeginCollect.Value;
                    model.EndPayCollect = this.EndPayCollect.Value;
                    model.TaxtRate = this.TaxtRate.Value;
                    model.SortOrder = this.SortOrder.Text.ToInt32();
                    model.Status = this.Status.Checked;
                    model.Remark = this.Remark.Text;
                    model.Faxing = this.Faxing.Text;
                    model.Address = this.Address.Text;
                    model.Bank = this.Bank.Text;
                    model.TaxpayerNumber = this.TaxpayerNumber.Text;
                    model.BankAccount = this.BankAccount.Text;
                    model.LandlinePhone = this.LandlinePhone.Text;
                    if (_w_CustomerServices.Update(model))
                    {
                        MessageBox.Show("保存成功！", "提示");
                    }
                }
                SetToolButtonStatus(formStatus);
                //列表重新赋值
                this.dataGridViewList.DataSource = GetData();
            }
        }

        public override void CanelFunction(object sender, EventArgs e)
        {
            bool isAdd = formStatus == FormStatus.Add;
            base.CanelFunction(sender, e);
            SetToolButtonStatus(formStatus);
            btnEdit.Enabled = !isAdd;
            btnDel.Enabled = !isAdd;
        }

        public override void DelFunction(object sender, EventArgs e)
        {
            base.DelFunction(sender, e);
            var result = MessageBox.Show("是否删除？", "确认", MessageBoxButtons.YesNoCancel);
            if (result == DialogResult.Yes)
            {
                _w_CustomerServices.DeleteById(Int32.Parse(txtID.Text));
                formStatus = FormStatus.Del;
                SetToolButtonStatus(formStatus);
                //列表重新赋值
                this.dataGridViewList.DataSource = GetData();
            }
        }

        public override void SetToolButtonStatus(FormStatus status)
        {
            base.SetToolButtonStatus(status);
            switch (status)
            {
                case FormStatus.Add:
                    {
                        btnAdd.Enabled = false;
                        btnEdit.Enabled = false;
                        btnSave.Enabled = true;
                        btnDel.Enabled = false;
                        btnCanel.Enabled = true;
                        SetControlStatus(this.groupBox1, true);
                        ClearControlsText(this.groupBox1);
                        break;
                    }
                case FormStatus.Edit:
                    {
                        btnAdd.Enabled = false;
                        btnEdit.Enabled = false;
                        btnSave.Enabled = true;
                        btnDel.Enabled = false;
                        btnCanel.Enabled = true;
                        SetControlStatus(this.groupBox1, true);
                        break;
                    }
                case FormStatus.View:
                    {
                        btnAdd.Enabled = true;
                        btnEdit.Enabled = true;
                        btnSave.Enabled = false;
                        btnDel.Enabled = true;
                        btnCanel.Enabled = false;
                        SetControlStatus(this.groupBox1, false);
                        break;
                    }
                case FormStatus.Canel:
                    {
                        btnAdd.Enabled = true;
                        btnEdit.Enabled = true;
                        btnSave.Enabled = false;
                        btnDel.Enabled = false;
                        btnCanel.Enabled = false;
                        SetControlStatus(this.groupBox1, false);
                        break;
                    }
                case FormStatus.First:
                    {
                        btnAdd.Enabled = true;
                        btnEdit.Enabled = false;
                        btnSave.Enabled = false;
                        btnDel.Enabled = false;
                        btnCanel.Enabled = false;
                        SetControlStatus(this.groupBox1, false);
                        break;
                    }
                case FormStatus.Save:
                    {
                        btnAdd.Enabled = true;
                        btnEdit.Enabled = true;
                        btnSave.Enabled = false;
                        btnDel.Enabled = true;
                        btnCanel.Enabled = false;
                        SetControlStatus(this.groupBox1, false);
                        break;
                    }
                case FormStatus.Del:
                    {
                        btnAdd.Enabled = true;
                        btnEdit.Enabled = false;
                        btnSave.Enabled = false;
                        btnDel.Enabled = false;
                        btnCanel.Enabled = false;
                        SetControlStatus(this.groupBox1, false);
                        ClearControlsText(this.groupBox1);
                        break;
                    }
            }
        }

        public override bool ValidateData()
        {
            return true;
        }
        /// <summary>
        /// 获取数据
        /// </summary>
        /// <returns></returns>
        private List<w_Customer> GetData()
        {
            List<w_Customer> data = new List<w_Customer>();
            data = _w_CustomerServices.Query();
            return data;
        }

        private void TotalAmountPaid_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            tabControl2.SelectedTab = tabPage3;
        }

        private void TotalPayable_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            tabControl2.SelectedTab = tabPage1;
        }

        private void TotalAmountReturned_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            tabControl2.SelectedTab = tabPage2;
        }

        private void TotalAmountOutstanding_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            tabControl2.SelectedTab = tabPage4;
        }

        private void tabControl2_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }
    }
}
