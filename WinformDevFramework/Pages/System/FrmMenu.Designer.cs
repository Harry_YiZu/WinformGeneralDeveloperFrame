﻿namespace WinformDevFramework
{
    partial class FrmMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ltMenuTitle = new WinformDevFramework.UserControls.Label_TextBox();
            this.ltMenuUrl = new WinformDevFramework.UserControls.Label_TextBox();
            this.ltIcon = new WinformDevFramework.UserControls.Label_TextBox();
            this.ltSort = new WinformDevFramework.UserControls.Label_TextBox();
            this.ltCreateBy = new WinformDevFramework.UserControls.Label_TextBox();
            this.btnSelectIcon = new System.Windows.Forms.Button();
            this.lcMenuType = new WinformDevFramework.UserControls.Label_ComBox();
            this.ldCreateDate = new WinformDevFramework.UserControls.Label_DateTime();
            this.ldUpdateDate = new WinformDevFramework.UserControls.Label_DateTime();
            this.ltUpdateBy = new WinformDevFramework.UserControls.Label_TextBox();
            this.uiComboTreeViewPMenu = new Sunny.UI.UIComboTreeView();
            this.label1 = new System.Windows.Forms.Label();
            this.txtMenuCode = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtPermissionCode = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.pictureBox = new System.Windows.Forms.PictureBox();
            this.palTools.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabDataEdit.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.flowLayoutPanelTools.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtPermissionCode);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txtMenuCode);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.uiComboTreeViewPMenu);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.ldUpdateDate);
            this.groupBox1.Controls.Add(this.ltUpdateBy);
            this.groupBox1.Controls.Add(this.ldCreateDate);
            this.groupBox1.Controls.Add(this.lcMenuType);
            this.groupBox1.Controls.Add(this.pictureBox);
            this.groupBox1.Controls.Add(this.btnSelectIcon);
            this.groupBox1.Controls.Add(this.ltCreateBy);
            this.groupBox1.Controls.Add(this.ltSort);
            this.groupBox1.Controls.Add(this.ltIcon);
            this.groupBox1.Controls.Add(this.ltMenuUrl);
            this.groupBox1.Controls.Add(this.ltMenuTitle);
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            this.groupBox1.Controls.SetChildIndex(this.ltMenuTitle, 0);
            this.groupBox1.Controls.SetChildIndex(this.ltMenuUrl, 0);
            this.groupBox1.Controls.SetChildIndex(this.ltIcon, 0);
            this.groupBox1.Controls.SetChildIndex(this.ltSort, 0);
            this.groupBox1.Controls.SetChildIndex(this.ltCreateBy, 0);
            this.groupBox1.Controls.SetChildIndex(this.btnSelectIcon, 0);
            this.groupBox1.Controls.SetChildIndex(this.pictureBox, 0);
            this.groupBox1.Controls.SetChildIndex(this.lcMenuType, 0);
            this.groupBox1.Controls.SetChildIndex(this.ldCreateDate, 0);
            this.groupBox1.Controls.SetChildIndex(this.ltUpdateBy, 0);
            this.groupBox1.Controls.SetChildIndex(this.ldUpdateDate, 0);
            this.groupBox1.Controls.SetChildIndex(this.uiComboTreeViewPMenu, 0);
            this.groupBox1.Controls.SetChildIndex(this.label1, 0);
            this.groupBox1.Controls.SetChildIndex(this.txtID, 0);
            this.groupBox1.Controls.SetChildIndex(this.label2, 0);
            this.groupBox1.Controls.SetChildIndex(this.txtMenuCode, 0);
            this.groupBox1.Controls.SetChildIndex(this.label3, 0);
            this.groupBox1.Controls.SetChildIndex(this.txtPermissionCode, 0);
            // 
            // txtID
            // 
            this.txtID.Location = new System.Drawing.Point(667, 33);
            // 
            // ltMenuTitle
            // 
            this.ltMenuTitle.Location = new System.Drawing.Point(6, 32);
            this.ltMenuTitle.Name = "ltMenuTitle";
            this.ltMenuTitle.Size = new System.Drawing.Size(184, 28);
            this.ltMenuTitle.TabIndex = 1;
            // 
            // ltMenuUrl
            // 
            this.ltMenuUrl.Location = new System.Drawing.Point(196, 32);
            this.ltMenuUrl.Name = "ltMenuUrl";
            this.ltMenuUrl.Size = new System.Drawing.Size(184, 28);
            this.ltMenuUrl.TabIndex = 2;
            // 
            // ltIcon
            // 
            this.ltIcon.Location = new System.Drawing.Point(408, 77);
            this.ltIcon.Name = "ltIcon";
            this.ltIcon.Size = new System.Drawing.Size(184, 28);
            this.ltIcon.TabIndex = 3;
            // 
            // ltSort
            // 
            this.ltSort.Location = new System.Drawing.Point(6, 77);
            this.ltSort.Name = "ltSort";
            this.ltSort.Size = new System.Drawing.Size(184, 28);
            this.ltSort.TabIndex = 4;
            // 
            // ltCreateBy
            // 
            this.ltCreateBy.Location = new System.Drawing.Point(7, 183);
            this.ltCreateBy.Name = "ltCreateBy";
            this.ltCreateBy.Size = new System.Drawing.Size(184, 28);
            this.ltCreateBy.TabIndex = 7;
            // 
            // btnSelectIcon
            // 
            this.btnSelectIcon.Location = new System.Drawing.Point(594, 80);
            this.btnSelectIcon.Name = "btnSelectIcon";
            this.btnSelectIcon.Size = new System.Drawing.Size(47, 23);
            this.btnSelectIcon.TabIndex = 8;
            this.btnSelectIcon.Text = "选择";
            this.btnSelectIcon.UseVisualStyleBackColor = true;
            this.btnSelectIcon.Click += new System.EventHandler(this.btnSelectIcon_Click);
            // 
            // lcMenuType
            // 
            this.lcMenuType.Location = new System.Drawing.Point(194, 77);
            this.lcMenuType.Name = "lcMenuType";
            this.lcMenuType.Size = new System.Drawing.Size(208, 28);
            this.lcMenuType.TabIndex = 10;
            // 
            // ldCreateDate
            // 
            this.ldCreateDate.Location = new System.Drawing.Point(194, 183);
            this.ldCreateDate.Name = "ldCreateDate";
            this.ldCreateDate.Size = new System.Drawing.Size(284, 28);
            this.ldCreateDate.TabIndex = 11;
            // 
            // ldUpdateDate
            // 
            this.ldUpdateDate.Location = new System.Drawing.Point(194, 236);
            this.ldUpdateDate.Name = "ldUpdateDate";
            this.ldUpdateDate.Size = new System.Drawing.Size(284, 28);
            this.ldUpdateDate.TabIndex = 13;
            // 
            // ltUpdateBy
            // 
            this.ltUpdateBy.Location = new System.Drawing.Point(7, 236);
            this.ltUpdateBy.Name = "ltUpdateBy";
            this.ltUpdateBy.Size = new System.Drawing.Size(184, 28);
            this.ltUpdateBy.TabIndex = 12;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(441, 39);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 17);
            this.label1.TabIndex = 15;
            this.label1.Text = "父菜单";
            // 
            // txtMenuCode
            // 
            this.txtMenuCode.Location = new System.Drawing.Point(87, 129);
            this.txtMenuCode.Name = "txtMenuCode";
            this.txtMenuCode.Size = new System.Drawing.Size(100, 23);
            this.txtMenuCode.TabIndex = 17;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(22, 129);
            this.label2.Name = "label2";
            this.label2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label2.Size = new System.Drawing.Size(59, 23);
            this.label2.TabIndex = 16;
            this.label2.Text = "菜单编码";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            //
            // uiComboTreeViewPMenu
            // 
            this.uiComboTreeViewPMenu.DropDownStyle = Sunny.UI.UIDropDownStyle.DropDownList;
            this.uiComboTreeViewPMenu.FillColor = System.Drawing.Color.White;
            this.uiComboTreeViewPMenu.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.uiComboTreeViewPMenu.Location = new System.Drawing.Point(491, 31);
            this.uiComboTreeViewPMenu.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uiComboTreeViewPMenu.MinimumSize = new System.Drawing.Size(63, 0);
            this.uiComboTreeViewPMenu.Name = "uiComboTreeViewPMenu";
            this.uiComboTreeViewPMenu.Padding = new System.Windows.Forms.Padding(0, 0, 30, 2);
            this.uiComboTreeViewPMenu.RectColor = System.Drawing.Color.Gray;
            this.uiComboTreeViewPMenu.Size = new System.Drawing.Size(150, 28);
            this.uiComboTreeViewPMenu.Style = Sunny.UI.UIStyle.Custom;
            this.uiComboTreeViewPMenu.TabIndex = 14;
            this.uiComboTreeViewPMenu.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.uiComboTreeViewPMenu.Watermark = "";
            // 
            // 
            // txtPermissionCode
            // 
            this.txtPermissionCode.Location = new System.Drawing.Point(277, 129);
            this.txtPermissionCode.Name = "txtPermissionCode";
            this.txtPermissionCode.Size = new System.Drawing.Size(100, 23);
            this.txtPermissionCode.TabIndex = 19;
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(212, 129);
            this.label3.Name = "label3";
            this.label3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label3.Size = new System.Drawing.Size(59, 23);
            this.label3.TabIndex = 18;
            this.label3.Text = "权限编码";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // pictureBox
            // 
            this.pictureBox.Location = new System.Drawing.Point(650, 80);
            this.pictureBox.Name = "pictureBox";
            this.pictureBox.Size = new System.Drawing.Size(20, 20);
            this.pictureBox.TabIndex = 9;
            this.pictureBox.TabStop = false;
            // 
            // FrmMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Name = "FrmMenu";
            this.Text = "FrmMenu";
            this.palTools.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabDataEdit.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.flowLayoutPanelTools.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private UserControls.Label_TextBox ltMenuTitle;
        private UserControls.Label_TextBox ltIcon;
        private UserControls.Label_TextBox ltMenuUrl;
        private UserControls.Label_TextBox ltSort;
        private UserControls.Label_TextBox ltCreateBy;
        private Button btnSelectIcon;
        private UserControls.Label_ComBox lcMenuType;
        private UserControls.Label_DateTime ldCreateDate;
        private UserControls.Label_DateTime ldUpdateDate;
        private UserControls.Label_TextBox ltUpdateBy;
        private Sunny.UI.UIComboTreeView uiComboTreeViewPMenu;
        private Label label1;
        private TextBox txtPermissionCode;
        private Label label3;
        private TextBox txtMenuCode;
        private Label label2;
        private PictureBox pictureBox;
    }
}