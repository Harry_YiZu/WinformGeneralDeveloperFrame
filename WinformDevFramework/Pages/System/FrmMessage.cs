﻿
using System.Windows.Forms;
using Tulpep.NotificationWindow;
using WinformDevFramework.IServices;
using WinformDevFramework.IServices.System;
using WinformDevFramework.Models;
using WinformDevFramework.Services;

namespace WinformDevFramework
{
    public partial class FrmMessage : Form
    {
        private IsysMessageServices _sysMessageServices;
        private ISysUserServices _sysUserServices;
        public FrmMessage( ISysUserServices sysUserServices,IsysMessageServices sysMessageServices)
        {
            _sysMessageServices=sysMessageServices;
            _sysUserServices=sysUserServices;
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
        }

        //发送消息
        private void btnSendMessage_Click(object sender, EventArgs e)
        {
            //校验数据
            if (string.IsNullOrEmpty(txtMessageTitle.Text))
            {
                MessageBox.Show("消息标题不能为空");
                return ;
            }
            if (string.IsNullOrEmpty(txtMessageContent.Text))
            {
                MessageBox.Show("消息内容不能为空");
                return;
            }
            List<sysUser> userList = new List<sysUser>();
            foreach (DataGridViewRow row in dataGridView2.Rows)
            {
                if (bool.Parse(row.Cells[0].Value?.ToString() ?? "false"))
                {
                    var user = row.DataBoundItem as sysUser;
                    userList.Add(user);
                }
            }

            if (!userList.Any())
            {
                MessageBox.Show("请至少选择一名用户");
                return;
            }

            //发送数据
            List<sysMessage> messageList = new List<sysMessage>();
            userList.ForEach(u =>
            {
                sysMessage message = new sysMessage();
                message.Content = txtMessageContent.Text;
                message.Title= txtMessageTitle.Text;
                message.FromUserID = AppInfo.User.ID;
                message.CreateTime=DateTime.Now;
                message.ToUserID = u.ID;
                message.IsSend = false;
                messageList.Add(message);
            });
            _sysMessageServices.Insert(messageList);
            MessageBox.Show("发送成功");
        }

        /// <summary>
        /// 初始化数据
        /// </summary>
        private void Init()
        {
            this.dataGridView2.DataSource = _sysUserServices.Query();
            SetDataGridView2Colums();
        }
        /// <summary>
        /// 校验数据
        /// </summary>
        /// <returns></returns>
        private bool ValidateData()
        {
            return false;
        }
        /// <summary>
        /// 搜索
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSearch_Click(object sender, EventArgs e)
        {
            SetUser(txtUserName.Text);
        }
        /// <summary>
        /// 设置用户
        /// </summary>
        private void SetUser(string username = "")
        {
            List<sysUser> users=new List<sysUser>();
            if (!string.IsNullOrEmpty(username))
                users = _sysUserServices.QueryListByClause(p => p.Username.Equals(username));
            else
            {
                users = _sysUserServices.Query();
            }
            dataGridView2.DataSource = users;
        }
        /// <summary>
        /// 设置用户表
        /// </summary>
        private void SetDataGridView2Colums()
        {
            string[] cols2 = new[] { "Username", "Fullname", "Sex", "Email", "PhoneNumber" };
            this.dataGridView2.Columns["Username"]!.HeaderText = "账号";
            this.dataGridView2.Columns["Fullname"]!.HeaderText = "昵称";
            this.dataGridView2.Columns["Sex"]!.HeaderText = "性别";
            this.dataGridView2.Columns["Email"]!.HeaderText = "邮箱";
            this.dataGridView2.Columns["PhoneNumber"]!.HeaderText = "电话号码";
            foreach (DataGridViewColumn column in dataGridView2.Columns)
            {
                if (!cols2.Contains(column.Name))
                {
                    column.Visible = false;
                }
            }

            // 添加一列用于显示多选框
            DataGridViewCheckBoxColumn checkBoxColumn = new DataGridViewCheckBoxColumn();
            checkBoxColumn.HeaderText = "";
            checkBoxColumn.Name = "checkBoxColumn";
            dataGridView2.Columns.Insert(0, checkBoxColumn);
        }

        private void FrmMessage_Load(object sender, EventArgs e)
        {
            Init();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            TabControl parentTabControl = this.Parent.Parent as TabControl;
            TabPage tabpage = this.Parent as TabPage;
            parentTabControl.TabPages.Remove(tabpage);
        }
    }
}
