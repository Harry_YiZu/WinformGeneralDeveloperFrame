using Sunny.UI;

namespace WinformDevFramework
{
    partial class FrmsysDept
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblDeptName = new System.Windows.Forms.Label();
            this.DeptName = new System.Windows.Forms.TextBox();
            this.lblPDeptID = new System.Windows.Forms.Label();
            this.lblLeaderID = new System.Windows.Forms.Label();
            this.lblRemark = new System.Windows.Forms.Label();
            this.Remark = new System.Windows.Forms.TextBox();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.dataGridViewUser = new System.Windows.Forms.DataGridView();
            this.LeaderID = new System.Windows.Forms.ComboBox();
            this.npgsqlCommandBuilder1 = new Npgsql.NpgsqlCommandBuilder();
            this.PDeptID = new Sunny.UI.UIComboTreeView();
            this.palTools.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabDataEdit.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.flowLayoutPanelTools.SuspendLayout();
            this.tabControl2.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewUser)).BeginInit();
            this.SuspendLayout();
            // 
            // palTools
            // 
            this.palTools.Size = new System.Drawing.Size(715, 31);
            // 
            // tabControl1
            // 
            this.tabControl1.Size = new System.Drawing.Size(715, 446);
            // 
            // tabList
            // 
            this.tabList.Size = new System.Drawing.Size(707, 416);
            // 
            // tabDataEdit
            // 
            this.tabDataEdit.Size = new System.Drawing.Size(707, 416);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.PDeptID);
            this.groupBox1.Controls.Add(this.LeaderID);
            this.groupBox1.Controls.Add(this.tabControl2);
            this.groupBox1.Controls.Add(this.Remark);
            this.groupBox1.Controls.Add(this.lblRemark);
            this.groupBox1.Controls.Add(this.lblLeaderID);
            this.groupBox1.Controls.Add(this.lblPDeptID);
            this.groupBox1.Controls.Add(this.DeptName);
            this.groupBox1.Controls.Add(this.lblDeptName);
            this.groupBox1.Size = new System.Drawing.Size(701, 410);
            this.groupBox1.Controls.SetChildIndex(this.lblDeptName, 0);
            this.groupBox1.Controls.SetChildIndex(this.DeptName, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblPDeptID, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblLeaderID, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblRemark, 0);
            this.groupBox1.Controls.SetChildIndex(this.Remark, 0);
            this.groupBox1.Controls.SetChildIndex(this.txtID, 0);
            this.groupBox1.Controls.SetChildIndex(this.tabControl2, 0);
            this.groupBox1.Controls.SetChildIndex(this.LeaderID, 0);
            this.groupBox1.Controls.SetChildIndex(this.PDeptID, 0);
            // 
            // flowLayoutPanelTools
            // 
            this.flowLayoutPanelTools.Size = new System.Drawing.Size(711, 27);
            // 
            // lblDeptName
            // 
            this.lblDeptName.Location = new System.Drawing.Point(10, 10);
            this.lblDeptName.Name = "lblDeptName";
            this.lblDeptName.Size = new System.Drawing.Size(85, 23);
            this.lblDeptName.TabIndex = 2;
            this.lblDeptName.Text = "机构名称";
            this.lblDeptName.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DeptName
            // 
            this.DeptName.Location = new System.Drawing.Point(100, 10);
            this.DeptName.Name = "DeptName";
            this.DeptName.Size = new System.Drawing.Size(130, 23);
            this.DeptName.TabIndex = 1;
            // 
            // lblPDeptID
            // 
            this.lblPDeptID.Location = new System.Drawing.Point(230, 10);
            this.lblPDeptID.Name = "lblPDeptID";
            this.lblPDeptID.Size = new System.Drawing.Size(85, 23);
            this.lblPDeptID.TabIndex = 4;
            this.lblPDeptID.Text = "上级机构";
            this.lblPDeptID.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblLeaderID
            // 
            this.lblLeaderID.Location = new System.Drawing.Point(10, 46);
            this.lblLeaderID.Name = "lblLeaderID";
            this.lblLeaderID.Size = new System.Drawing.Size(85, 23);
            this.lblLeaderID.TabIndex = 6;
            this.lblLeaderID.Text = "负责人";
            this.lblLeaderID.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblRemark
            // 
            this.lblRemark.Location = new System.Drawing.Point(230, 45);
            this.lblRemark.Name = "lblRemark";
            this.lblRemark.Size = new System.Drawing.Size(85, 23);
            this.lblRemark.TabIndex = 8;
            this.lblRemark.Text = "备注";
            this.lblRemark.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Remark
            // 
            this.Remark.Location = new System.Drawing.Point(320, 45);
            this.Remark.Name = "Remark";
            this.Remark.Size = new System.Drawing.Size(130, 23);
            this.Remark.TabIndex = 7;
            // 
            // tabControl2
            // 
            this.tabControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl2.Controls.Add(this.tabPage2);
            this.tabControl2.Location = new System.Drawing.Point(-2, 116);
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(699, 297);
            this.tabControl2.TabIndex = 9;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.dataGridViewUser);
            this.tabPage2.Location = new System.Drawing.Point(4, 26);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(691, 267);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "用户";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // dataGridViewUser
            // 
            this.dataGridViewUser.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridViewUser.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridViewUser.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewUser.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewUser.GridColor = System.Drawing.SystemColors.Control;
            this.dataGridViewUser.Location = new System.Drawing.Point(3, 3);
            this.dataGridViewUser.Name = "dataGridViewUser";
            this.dataGridViewUser.ReadOnly = true;
            this.dataGridViewUser.RowHeadersVisible = false;
            this.dataGridViewUser.RowTemplate.Height = 25;
            this.dataGridViewUser.Size = new System.Drawing.Size(685, 261);
            this.dataGridViewUser.TabIndex = 0;
            // 
            // LeaderID
            // 
            this.LeaderID.FormattingEnabled = true;
            this.LeaderID.Location = new System.Drawing.Point(100, 46);
            this.LeaderID.Name = "LeaderID";
            this.LeaderID.Size = new System.Drawing.Size(130, 25);
            this.LeaderID.TabIndex = 10;
            // 
            // npgsqlCommandBuilder1
            // 
            this.npgsqlCommandBuilder1.QuotePrefix = "\"";
            this.npgsqlCommandBuilder1.QuoteSuffix = "\"";
            // 
            // PDeptID
            // 
            this.PDeptID.DropDownStyle = Sunny.UI.UIDropDownStyle.DropDownList;
            this.PDeptID.FillColor = System.Drawing.Color.White;
            this.PDeptID.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.PDeptID.Location = new System.Drawing.Point(320, 10);
            this.PDeptID.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.PDeptID.MinimumSize = new System.Drawing.Size(63, 0);
            this.PDeptID.Name = "PDeptID";
            this.PDeptID.Padding = new System.Windows.Forms.Padding(0, 0, 30, 2);
            this.PDeptID.RectColor = System.Drawing.Color.Gray;
            this.PDeptID.Size = new System.Drawing.Size(150, 28);
            this.PDeptID.Style = Sunny.UI.UIStyle.Custom;
            this.PDeptID.TabIndex = 15;
            this.PDeptID.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.PDeptID.Watermark = "";
            //
            // FrmsysDept
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(715, 477);
            this.DoubleBuffered = true;
            this.Name = "FrmsysDept";
            this.Text = "FrmsysDept";
            this.palTools.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabDataEdit.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.flowLayoutPanelTools.ResumeLayout(false);
            this.tabControl2.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewUser)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
            private TextBox DeptName;
            private Label lblDeptName;
            private Label lblPDeptID;
            private Label lblLeaderID;
            private TextBox Remark;
            private Label lblRemark;
        private TabControl tabControl2;
        private TabPage tabPage2;
        private DataGridView dataGridViewUser;
        private ComboBox LeaderID;
        private Npgsql.NpgsqlCommandBuilder npgsqlCommandBuilder1;
        private UIComboTreeView PDeptID;
    }
}