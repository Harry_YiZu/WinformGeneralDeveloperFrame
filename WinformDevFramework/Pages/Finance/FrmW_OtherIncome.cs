using SqlSugar;
using WinformDevFramework.Core.Winform;
using WinformDevFramework.IServices;
using WinformDevFramework;
using WinformDevFramework.IServices.System;
using WinformDevFramework.Services;

namespace WinformDevFramework
{
    public partial class Frmw_OtherIncome : BaseForm1
    {
        private Iw_OtherIncomeServices _w_OtherIncomeServices;
        private Iw_CustomerServices _w_CustomerServices;
        private Iw_SupplierServices _w_SupplierServices;
        private Iw_SettlementAccountServices _w_SettlementAccountServices;
        private IsysDicDataServices _w_SysDicDataServices;
        private ISysUserServices _w_SysUserServices;
        public Frmw_OtherIncome(Iw_OtherIncomeServices w_OtherIncomeServices, Iw_CustomerServices w_CustomerServices, Iw_SupplierServices w_SupplierServices, Iw_SettlementAccountServices w_SettlementAccountServices, IsysDicDataServices w_SysDicDataServices, ISysUserServices w_SysUserServices)
        {
            _w_OtherIncomeServices = w_OtherIncomeServices;
            _w_CustomerServices = w_CustomerServices;
            _w_SupplierServices = w_SupplierServices;
            _w_SettlementAccountServices = w_SettlementAccountServices;
            _w_SysDicDataServices = w_SysDicDataServices;
            _w_SysUserServices = w_SysUserServices;
            InitializeComponent();
        }

        public override void Init()
        {
            base.Init();
            //查询数据
            this.dataGridViewList.DataSource = GetData();

            // 设置隐藏列
            this.dataGridViewList.Columns["ID"].HeaderText = "";
            this.dataGridViewList.Columns["OtherIncomeCode"].HeaderText = "其他收入单号";
            this.dataGridViewList.Columns["FlatCode"].HeaderText = "对方单位编码";
            this.dataGridViewList.Columns["FlatName"].HeaderText = "对方单位名称";
            this.dataGridViewList.Columns["InvicesDate"].HeaderText = "单据日期";
            this.dataGridViewList.Columns["SettlementCode"].HeaderText = "结算账户编码";
            this.dataGridViewList.Columns["SettlementName"].HeaderText = "结算账户名称";
            this.dataGridViewList.Columns["IncomeTypeID"].HeaderText = "收入类别ID";
            this.dataGridViewList.Columns["IncomeType"].HeaderText = "收入类别";
            this.dataGridViewList.Columns["TotalPrice"].HeaderText = "金额";
            this.dataGridViewList.Columns["MakeUserID"].Visible = false;
            this.dataGridViewList.Columns["ReviewUserID"].Visible = false;
            this.dataGridViewList.Columns["Status"].Visible = false;
            this.dataGridViewList.Columns["Remark"].HeaderText = "备注";

            //设置结算账户
            SettlementName.DataSource = _w_SettlementAccountServices.Query();
            SettlementName.DisplayMember = "Name";
            SettlementName.ValueMember = "Code";

            //设置对方单位名称

            //客户
            var customers = _w_CustomerServices.Query();
            //供应商
            var suppliers = _w_SupplierServices.Query();
            List<FlatInfo> flats = new List<FlatInfo>();
            customers.ForEach(p => flats.Add(new FlatInfo()
            {
                Name = p.CustomerName,
                Code = p.CustomerCode
            }));
            suppliers.ForEach(p => flats.Add(new FlatInfo()
            {
                Name = p.SupplierName,
                Code = p.SupplierCode
            }));
            FlatName.DataSource = flats;
            FlatName.DisplayMember = "Name";
            FlatName.ValueMember = "Code";

            //设置收入类别
            IncomeTypeID.DataSource = _w_SysDicDataServices.QueryListByClause(p => p.DicTypeID == 2006);
            IncomeTypeID.DisplayMember = "DicData";
            IncomeTypeID.ValueMember = "ID";

            //设置制单人
            MakeUserID.DataSource = _w_SysUserServices.Query();
            MakeUserID.DisplayMember = "Fullname";
            MakeUserID.ValueMember = "ID";

            //设置审核人
            ReviewUserID.DataSource = _w_SysUserServices.Query();
            ReviewUserID.DisplayMember = "Fullname";
            ReviewUserID.ValueMember = "ID";
            SetToolButtonStatus(formStatus);
            SetToolsButton();
        }

        public override void TabControlSelectingFunction(object? sender, TabControlCancelEventArgs e)
        {
            base.TabControlSelectingFunction(sender, e);
        }

        public override void DataGridViewListDoubleClick(object? sender, DataGridViewCellEventArgs e)
        {
            base.DataGridViewListDoubleClick(sender, e);
            var g = (DataGridView)sender;
            var model = g.CurrentRow.DataBoundItem as w_OtherIncome;

            //给详情页设置数据
            this.txtID.Text = model.ID.ToString();
            this.OtherIncomeCode.Text = model.OtherIncomeCode.ToString();
            this.FlatCode.Text = model.FlatCode.ToString();
            this.FlatName.Text = model.FlatName.ToString();
            this.InvicesDate.Text = model.InvicesDate.ToString();
            this.SettlementCode.Text = model.SettlementCode.ToString();
            this.SettlementName.Text = model.SettlementName.ToString();
            this.IncomeTypeID.SelectedValue = model.IncomeTypeID;
            this.TotalPrice.Text = model.TotalPrice.ToString();
            this.MakeUserID.Text = model.MakeUserID.ToString();
            this.ReviewUserID.Text = model.ReviewUserID.ToString();
            this.Status.Text = model.Status.ToString();
            this.Remark.Text = model.Remark.ToString();
            SetToolButtonStatus(formStatus);
        }

        public override void AddFunction(object sender, EventArgs e)
        {
            base.AddFunction(sender, e);
            SetToolButtonStatus(formStatus);

            FlatCode.Text = FlatName.SelectedValue.ToString();
            SettlementCode.Text = SettlementName.SelectedValue.ToString();
            MakeUserID.SelectedValue = AppInfo.User.ID;
        }

        public override void EditFunction(object sender, EventArgs e)
        {
            base.EditFunction(sender, e);
            SetToolButtonStatus(formStatus);
        }

        public override void SaveFunction(object sender, EventArgs e)
        {
            base.SaveFunction(sender, e);
            {
                //新增
                if (string.IsNullOrEmpty(txtID.Text))
                {
                    w_OtherIncome model = new w_OtherIncome();
                    // TODO获取界面的数据
                    // TODO获取界面的数据
                    if (string.IsNullOrEmpty(this.OtherIncomeCode.Text))
                    {
                        model.OtherIncomeCode = $"QTSR{DateTime.Now.ToString("yyyyMMddHHmmss")}";
                    }
                    else
                    {
                        model.OtherIncomeCode = this.OtherIncomeCode.Text;
                    }
                    model.FlatCode = this.FlatCode.Text;
                    model.FlatName = this.FlatName.Text;
                    model.InvicesDate = this.InvicesDate.Text.ToDateTime();
                    model.SettlementCode = this.SettlementCode.Text;
                    model.SettlementName = this.SettlementName.Text;
                    model.IncomeTypeID = this.IncomeTypeID.SelectedValue.ToInt32();
                    model.IncomeType = this.IncomeTypeID.Text;
                    model.TotalPrice = this.TotalPrice.Text.ToDecimal();
                    model.MakeUserID = this.MakeUserID.SelectedValue.ToInt32();
                    model.ReviewUserID = this.ReviewUserID.SelectedValue.ToInt32();
                    model.Status = this.Status.Text;
                    model.Remark = this.Remark.Text;
                    var id = _w_OtherIncomeServices.Insert(model);
                    txtID.Text = id.ToString();
                    if (id > 0)
                    {
                        var settlementAccount =
                            _w_SettlementAccountServices.QueryByClause(p => p.Code == model.SettlementCode);
                        if (settlementAccount != null)
                        {
                            settlementAccount.NowAmount += model.TotalPrice;
                            _w_SettlementAccountServices.Update(settlementAccount);
                        }
                        MessageBox.Show("保存成功！", "提示");
                        OtherIncomeCode.Text = model.OtherIncomeCode;
                    }
                }
                // 修改
                else
                {
                    w_OtherIncome model = _w_OtherIncomeServices.QueryById(int.Parse(txtID.Text));
                    // TODO获取界面的数据
                    model.OtherIncomeCode = this.OtherIncomeCode.Text;
                    model.FlatCode = this.FlatCode.Text;
                    model.FlatName = this.FlatName.Text;
                    model.InvicesDate = this.InvicesDate.Text.ToDateTime();
                    model.SettlementCode = this.SettlementCode.Text;
                    model.SettlementName = this.SettlementName.Text;
                    model.IncomeTypeID = this.IncomeTypeID.SelectedValue.ToInt32();
                    model.IncomeType = this.IncomeTypeID.Text;
                    model.TotalPrice = this.TotalPrice.Text.ToDecimal();
                    model.MakeUserID = this.MakeUserID.SelectedValue.ToInt32();
                    model.ReviewUserID = this.ReviewUserID.SelectedValue.ToInt32();
                    model.Status = this.Status.Text;
                    model.Remark = this.Remark.Text;
                    if (_w_OtherIncomeServices.Update(model))
                    {
                        MessageBox.Show("保存成功！", "提示");
                    }
                }
                SetToolButtonStatus(formStatus);
                //列表重新赋值
                this.dataGridViewList.DataSource = GetData();
            }
        }

        public override void CanelFunction(object sender, EventArgs e)
        {
            bool isAdd = formStatus == FormStatus.Add;
            base.CanelFunction(sender, e);
            SetToolButtonStatus(formStatus);
            btnEdit.Enabled = !isAdd;
            btnDel.Enabled = !isAdd;

        }

        public override void DelFunction(object sender, EventArgs e)
        {
            base.DelFunction(sender, e);
            var result = MessageBox.Show("是否删除？", "确认", MessageBoxButtons.YesNoCancel);
            if (result == DialogResult.Yes)
            {
                _w_OtherIncomeServices.DeleteById(Int32.Parse(txtID.Text));
                var settlementAccount = _w_SettlementAccountServices.QueryByClause(p => p.Code == SettlementCode.Text);
                if (settlementAccount != null)
                {
                    settlementAccount.NowAmount -= TotalPrice.Text.ToDecimal();
                }
                formStatus = FormStatus.Del;
                SetToolButtonStatus(formStatus);
                //列表重新赋值
                this.dataGridViewList.DataSource = GetData();
            }
        }

        public override void SetToolButtonStatus(FormStatus status)
        {
            base.SetToolButtonStatus(status);
            switch (status)
            {
                case FormStatus.Add:
                    {
                        btnAdd.Enabled = false;
                        btnEdit.Enabled = false;
                        btnSave.Enabled = true;
                        btnDel.Enabled = false;
                        btnCanel.Enabled = true;
                        SetControlStatus(this.groupBox1, true);
                        ClearControlsText(this.groupBox1);
                        break;
                    }
                case FormStatus.Edit:
                    {
                        btnAdd.Enabled = false;
                        btnEdit.Enabled = false;
                        btnSave.Enabled = true;
                        btnDel.Enabled = false;
                        btnCanel.Enabled = true;
                        SetControlStatus(this.groupBox1, true);
                        break;
                    }
                case FormStatus.View:
                    {
                        btnAdd.Enabled = true;
                        btnEdit.Enabled = true;
                        btnSave.Enabled = false;
                        btnDel.Enabled = true;
                        btnCanel.Enabled = false;
                        SetControlStatus(this.groupBox1, false);
                        break;
                    }
                case FormStatus.Canel:
                    {
                        btnAdd.Enabled = true;
                        btnEdit.Enabled = true;
                        btnSave.Enabled = false;
                        btnDel.Enabled = false;
                        btnCanel.Enabled = false;
                        SetControlStatus(this.groupBox1, false);
                        break;
                    }
                case FormStatus.First:
                    {
                        btnAdd.Enabled = true;
                        btnEdit.Enabled = false;
                        btnSave.Enabled = false;
                        btnDel.Enabled = false;
                        btnCanel.Enabled = false;
                        SetControlStatus(this.groupBox1, false);
                        break;
                    }
                case FormStatus.Save:
                    {
                        btnAdd.Enabled = true;
                        btnEdit.Enabled = true;
                        btnSave.Enabled = false;
                        btnDel.Enabled = true;
                        btnCanel.Enabled = false;
                        SetControlStatus(this.groupBox1, false);
                        break;
                    }
                case FormStatus.Del:
                    {
                        btnAdd.Enabled = true;
                        btnEdit.Enabled = false;
                        btnSave.Enabled = false;
                        btnDel.Enabled = false;
                        btnCanel.Enabled = false;
                        SetControlStatus(this.groupBox1, false);
                        ClearControlsText(this.groupBox1);
                        break;
                    }
            }
        }

        public override bool ValidateData()
        {
            return true;
        }
        /// <summary>
        /// 获取数据
        /// </summary>
        /// <returns></returns>
        private List<w_OtherIncome> GetData()
        {
            List<w_OtherIncome> data = new List<w_OtherIncome>();
            data = _w_OtherIncomeServices.Query();
            return data;
        }
    }
}
