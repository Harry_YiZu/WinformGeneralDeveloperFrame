namespace WinformDevFramework
{
    partial class Frmw_Receivable
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblReceivableCode = new System.Windows.Forms.Label();
            this.ReceivableCode = new System.Windows.Forms.TextBox();
            this.lblCustomerCode = new System.Windows.Forms.Label();
            this.CustomerCode = new System.Windows.Forms.TextBox();
            this.lblCustomerName = new System.Windows.Forms.Label();
            this.lblReceivableDate = new System.Windows.Forms.Label();
            this.ReceivableDate = new System.Windows.Forms.DateTimePicker();
            this.lblMakeUserID = new System.Windows.Forms.Label();
            this.lblSettlementCode = new System.Windows.Forms.Label();
            this.SettlementCode = new System.Windows.Forms.TextBox();
            this.lblSettlementName = new System.Windows.Forms.Label();
            this.lblSettlementType = new System.Windows.Forms.Label();
            this.lblTotalPrice = new System.Windows.Forms.Label();
            this.TotalPrice = new System.Windows.Forms.TextBox();
            this.lblRemark = new System.Windows.Forms.Label();
            this.Remark = new System.Windows.Forms.TextBox();
            this.CustomerName = new System.Windows.Forms.ComboBox();
            this.SettlementName = new System.Windows.Forms.ComboBox();
            this.SettlementType = new System.Windows.Forms.ComboBox();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.dataGridViewDetail = new System.Windows.Forms.DataGridView();
            this.SaleCode = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.ReceivableCodeD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ReceivableDetailCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Amount1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Amount2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Amount3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Amount4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RemarkD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MakeUserID = new System.Windows.Forms.ComboBox();
            this.palTools.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabDataEdit.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.flowLayoutPanelTools.SuspendLayout();
            this.tabControl2.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewDetail)).BeginInit();
            this.SuspendLayout();
            // 
            // palTools
            // 
            this.palTools.Margin = new System.Windows.Forms.Padding(2);
            // 
            // tabControl1
            // 
            this.tabControl1.Margin = new System.Windows.Forms.Padding(2);
            // 
            // tabList
            // 
            this.tabList.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.tabList.Padding = new System.Windows.Forms.Padding(6, 5, 6, 5);
            // 
            // tabDataEdit
            // 
            this.tabDataEdit.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.tabDataEdit.Padding = new System.Windows.Forms.Padding(6, 5, 6, 5);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.MakeUserID);
            this.groupBox1.Controls.Add(this.tabControl2);
            this.groupBox1.Controls.Add(this.SettlementType);
            this.groupBox1.Controls.Add(this.SettlementName);
            this.groupBox1.Controls.Add(this.CustomerName);
            this.groupBox1.Controls.Add(this.Remark);
            this.groupBox1.Controls.Add(this.lblRemark);
            this.groupBox1.Controls.Add(this.TotalPrice);
            this.groupBox1.Controls.Add(this.lblTotalPrice);
            this.groupBox1.Controls.Add(this.lblSettlementType);
            this.groupBox1.Controls.Add(this.lblSettlementName);
            this.groupBox1.Controls.Add(this.SettlementCode);
            this.groupBox1.Controls.Add(this.lblSettlementCode);
            this.groupBox1.Controls.Add(this.lblMakeUserID);
            this.groupBox1.Controls.Add(this.ReceivableDate);
            this.groupBox1.Controls.Add(this.lblReceivableDate);
            this.groupBox1.Controls.Add(this.lblCustomerName);
            this.groupBox1.Controls.Add(this.CustomerCode);
            this.groupBox1.Controls.Add(this.lblCustomerCode);
            this.groupBox1.Controls.Add(this.ReceivableCode);
            this.groupBox1.Controls.Add(this.lblReceivableCode);
            this.groupBox1.Location = new System.Drawing.Point(6, 5);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox1.Size = new System.Drawing.Size(780, 379);
            this.groupBox1.Controls.SetChildIndex(this.lblReceivableCode, 0);
            this.groupBox1.Controls.SetChildIndex(this.ReceivableCode, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblCustomerCode, 0);
            this.groupBox1.Controls.SetChildIndex(this.CustomerCode, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblCustomerName, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblReceivableDate, 0);
            this.groupBox1.Controls.SetChildIndex(this.ReceivableDate, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblMakeUserID, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblSettlementCode, 0);
            this.groupBox1.Controls.SetChildIndex(this.SettlementCode, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblSettlementName, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblSettlementType, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblTotalPrice, 0);
            this.groupBox1.Controls.SetChildIndex(this.TotalPrice, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblRemark, 0);
            this.groupBox1.Controls.SetChildIndex(this.Remark, 0);
            this.groupBox1.Controls.SetChildIndex(this.txtID, 0);
            this.groupBox1.Controls.SetChildIndex(this.CustomerName, 0);
            this.groupBox1.Controls.SetChildIndex(this.SettlementName, 0);
            this.groupBox1.Controls.SetChildIndex(this.SettlementType, 0);
            this.groupBox1.Controls.SetChildIndex(this.tabControl2, 0);
            this.groupBox1.Controls.SetChildIndex(this.MakeUserID, 0);
            // 
            // flowLayoutPanelTools
            // 
            this.flowLayoutPanelTools.Margin = new System.Windows.Forms.Padding(2);
            // 
            // txtID
            // 
            this.txtID.Location = new System.Drawing.Point(199, 31);
            this.txtID.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.txtID.Size = new System.Drawing.Size(56, 23);
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(2, 2);
            this.btnAdd.Margin = new System.Windows.Forms.Padding(2);
            this.btnAdd.Size = new System.Drawing.Size(53, 23);
            // 
            // btnEdit
            // 
            this.btnEdit.Location = new System.Drawing.Point(59, 2);
            this.btnEdit.Margin = new System.Windows.Forms.Padding(2);
            this.btnEdit.Size = new System.Drawing.Size(53, 23);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(116, 2);
            this.btnSave.Margin = new System.Windows.Forms.Padding(2);
            this.btnSave.Size = new System.Drawing.Size(54, 23);
            // 
            // btnCanel
            // 
            this.btnCanel.Location = new System.Drawing.Point(174, 2);
            this.btnCanel.Margin = new System.Windows.Forms.Padding(2);
            this.btnCanel.Size = new System.Drawing.Size(48, 23);
            // 
            // btnDel
            // 
            this.btnDel.Location = new System.Drawing.Point(226, 2);
            this.btnDel.Margin = new System.Windows.Forms.Padding(2);
            this.btnDel.Size = new System.Drawing.Size(50, 25);
            // 
            // btnResetPW
            // 
            this.btnResetPW.Location = new System.Drawing.Point(280, 2);
            this.btnResetPW.Margin = new System.Windows.Forms.Padding(2);
            this.btnResetPW.Size = new System.Drawing.Size(77, 25);
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(416, 2);
            this.btnClose.Margin = new System.Windows.Forms.Padding(2);
            this.btnClose.Size = new System.Drawing.Size(52, 23);
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(361, 2);
            this.btnSearch.Margin = new System.Windows.Forms.Padding(2);
            this.btnSearch.Size = new System.Drawing.Size(51, 23);
            // 
            // lblReceivableCode
            // 
            this.lblReceivableCode.Location = new System.Drawing.Point(10, 10);
            this.lblReceivableCode.Name = "lblReceivableCode";
            this.lblReceivableCode.Size = new System.Drawing.Size(85, 23);
            this.lblReceivableCode.TabIndex = 2;
            this.lblReceivableCode.Text = "收款单号";
            this.lblReceivableCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ReceivableCode
            // 
            this.ReceivableCode.Location = new System.Drawing.Point(100, 10);
            this.ReceivableCode.Name = "ReceivableCode";
            this.ReceivableCode.PlaceholderText = "为空自动生成";
            this.ReceivableCode.Size = new System.Drawing.Size(130, 23);
            this.ReceivableCode.TabIndex = 1;
            // 
            // lblCustomerCode
            // 
            this.lblCustomerCode.Location = new System.Drawing.Point(450, 10);
            this.lblCustomerCode.Name = "lblCustomerCode";
            this.lblCustomerCode.Size = new System.Drawing.Size(85, 23);
            this.lblCustomerCode.TabIndex = 4;
            this.lblCustomerCode.Text = "客户编码";
            this.lblCustomerCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // CustomerCode
            // 
            this.CustomerCode.Location = new System.Drawing.Point(540, 10);
            this.CustomerCode.Name = "CustomerCode";
            this.CustomerCode.ReadOnly = true;
            this.CustomerCode.Size = new System.Drawing.Size(130, 23);
            this.CustomerCode.TabIndex = 3;
            // 
            // lblCustomerName
            // 
            this.lblCustomerName.Location = new System.Drawing.Point(230, 10);
            this.lblCustomerName.Name = "lblCustomerName";
            this.lblCustomerName.Size = new System.Drawing.Size(85, 23);
            this.lblCustomerName.TabIndex = 6;
            this.lblCustomerName.Text = "客户名称";
            this.lblCustomerName.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblReceivableDate
            // 
            this.lblReceivableDate.Location = new System.Drawing.Point(10, 45);
            this.lblReceivableDate.Name = "lblReceivableDate";
            this.lblReceivableDate.Size = new System.Drawing.Size(85, 23);
            this.lblReceivableDate.TabIndex = 8;
            this.lblReceivableDate.Text = "收款日期";
            this.lblReceivableDate.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ReceivableDate
            // 
            this.ReceivableDate.Location = new System.Drawing.Point(100, 45);
            this.ReceivableDate.Name = "ReceivableDate";
            this.ReceivableDate.Size = new System.Drawing.Size(130, 23);
            this.ReceivableDate.TabIndex = 7;
            // 
            // lblMakeUserID
            // 
            this.lblMakeUserID.Location = new System.Drawing.Point(230, 45);
            this.lblMakeUserID.Name = "lblMakeUserID";
            this.lblMakeUserID.Size = new System.Drawing.Size(85, 23);
            this.lblMakeUserID.TabIndex = 10;
            this.lblMakeUserID.Text = "制单人";
            this.lblMakeUserID.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblSettlementCode
            // 
            this.lblSettlementCode.Location = new System.Drawing.Point(230, 80);
            this.lblSettlementCode.Name = "lblSettlementCode";
            this.lblSettlementCode.Size = new System.Drawing.Size(85, 23);
            this.lblSettlementCode.TabIndex = 12;
            this.lblSettlementCode.Text = "结算账户编码";
            this.lblSettlementCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // SettlementCode
            // 
            this.SettlementCode.Location = new System.Drawing.Point(320, 80);
            this.SettlementCode.Name = "SettlementCode";
            this.SettlementCode.ReadOnly = true;
            this.SettlementCode.Size = new System.Drawing.Size(130, 23);
            this.SettlementCode.TabIndex = 11;
            // 
            // lblSettlementName
            // 
            this.lblSettlementName.Location = new System.Drawing.Point(10, 80);
            this.lblSettlementName.Name = "lblSettlementName";
            this.lblSettlementName.Size = new System.Drawing.Size(85, 23);
            this.lblSettlementName.TabIndex = 14;
            this.lblSettlementName.Text = "结算账户名称";
            this.lblSettlementName.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblSettlementType
            // 
            this.lblSettlementType.Location = new System.Drawing.Point(450, 80);
            this.lblSettlementType.Name = "lblSettlementType";
            this.lblSettlementType.Size = new System.Drawing.Size(85, 23);
            this.lblSettlementType.TabIndex = 16;
            this.lblSettlementType.Text = "结算方式";
            this.lblSettlementType.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblTotalPrice
            // 
            this.lblTotalPrice.Location = new System.Drawing.Point(450, 44);
            this.lblTotalPrice.Name = "lblTotalPrice";
            this.lblTotalPrice.Size = new System.Drawing.Size(85, 23);
            this.lblTotalPrice.TabIndex = 18;
            this.lblTotalPrice.Text = "实收金额";
            this.lblTotalPrice.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TotalPrice
            // 
            this.TotalPrice.Location = new System.Drawing.Point(540, 44);
            this.TotalPrice.Name = "TotalPrice";
            this.TotalPrice.Size = new System.Drawing.Size(130, 23);
            this.TotalPrice.TabIndex = 17;
            // 
            // lblRemark
            // 
            this.lblRemark.Location = new System.Drawing.Point(10, 115);
            this.lblRemark.Name = "lblRemark";
            this.lblRemark.Size = new System.Drawing.Size(85, 23);
            this.lblRemark.TabIndex = 20;
            this.lblRemark.Text = "备注";
            this.lblRemark.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Remark
            // 
            this.Remark.Location = new System.Drawing.Point(100, 115);
            this.Remark.Name = "Remark";
            this.Remark.Size = new System.Drawing.Size(130, 23);
            this.Remark.TabIndex = 19;
            // 
            // CustomerName
            // 
            this.CustomerName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CustomerName.FormattingEnabled = true;
            this.CustomerName.Location = new System.Drawing.Point(320, 10);
            this.CustomerName.Name = "CustomerName";
            this.CustomerName.Size = new System.Drawing.Size(130, 25);
            this.CustomerName.TabIndex = 21;
            // 
            // SettlementName
            // 
            this.SettlementName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.SettlementName.FormattingEnabled = true;
            this.SettlementName.Location = new System.Drawing.Point(100, 78);
            this.SettlementName.Name = "SettlementName";
            this.SettlementName.Size = new System.Drawing.Size(130, 25);
            this.SettlementName.TabIndex = 22;
            // 
            // SettlementType
            // 
            this.SettlementType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.SettlementType.FormattingEnabled = true;
            this.SettlementType.Location = new System.Drawing.Point(540, 78);
            this.SettlementType.Name = "SettlementType";
            this.SettlementType.Size = new System.Drawing.Size(130, 25);
            this.SettlementType.TabIndex = 23;
            // 
            // tabControl2
            // 
            this.tabControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl2.Controls.Add(this.tabPage1);
            this.tabControl2.Location = new System.Drawing.Point(0, 144);
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(776, 229);
            this.tabControl2.TabIndex = 24;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.dataGridViewDetail);
            this.tabPage1.Location = new System.Drawing.Point(4, 26);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(768, 199);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "结算明细";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // dataGridViewDetail
            // 
            this.dataGridViewDetail.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGridViewDetail.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridViewDetail.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridViewDetail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewDetail.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.SaleCode,
            this.ReceivableCodeD,
            this.ReceivableDetailCode,
            this.Amount1,
            this.Amount2,
            this.Amount3,
            this.Amount4,
            this.RemarkD});
            this.dataGridViewDetail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewDetail.GridColor = System.Drawing.SystemColors.Control;
            this.dataGridViewDetail.Location = new System.Drawing.Point(3, 3);
            this.dataGridViewDetail.Name = "dataGridViewDetail";
            this.dataGridViewDetail.RowHeadersVisible = false;
            this.dataGridViewDetail.RowHeadersWidth = 72;
            this.dataGridViewDetail.RowTemplate.Height = 25;
            this.dataGridViewDetail.Size = new System.Drawing.Size(762, 193);
            this.dataGridViewDetail.TabIndex = 0;
            // 
            // SaleCode
            // 
            this.SaleCode.HeaderText = "销售单号";
            this.SaleCode.MinimumWidth = 9;
            this.SaleCode.Name = "SaleCode";
            this.SaleCode.Width = 62;
            // 
            // ReceivableCodeD
            // 
            this.ReceivableCodeD.DataPropertyName = "ReceivableCode";
            this.ReceivableCodeD.HeaderText = "收款单号";
            this.ReceivableCodeD.MinimumWidth = 9;
            this.ReceivableCodeD.Name = "ReceivableCodeD";
            this.ReceivableCodeD.ReadOnly = true;
            this.ReceivableCodeD.Width = 81;
            // 
            // ReceivableDetailCode
            // 
            this.ReceivableDetailCode.DataPropertyName = "ReceivableDetailCode";
            this.ReceivableDetailCode.HeaderText = "收款明细单号";
            this.ReceivableDetailCode.MinimumWidth = 9;
            this.ReceivableDetailCode.Name = "ReceivableDetailCode";
            this.ReceivableDetailCode.ReadOnly = true;
            this.ReceivableDetailCode.Width = 105;
            // 
            // Amount1
            // 
            this.Amount1.DataPropertyName = "Amount1";
            this.Amount1.HeaderText = "单据金额";
            this.Amount1.MinimumWidth = 9;
            this.Amount1.Name = "Amount1";
            this.Amount1.ReadOnly = true;
            this.Amount1.Width = 81;
            // 
            // Amount2
            // 
            this.Amount2.DataPropertyName = "Amount2";
            this.Amount2.HeaderText = "已结金额";
            this.Amount2.MinimumWidth = 9;
            this.Amount2.Name = "Amount2";
            this.Amount2.ReadOnly = true;
            this.Amount2.Width = 81;
            // 
            // Amount3
            // 
            this.Amount3.DataPropertyName = "Amount3";
            this.Amount3.HeaderText = "未结金额";
            this.Amount3.MinimumWidth = 9;
            this.Amount3.Name = "Amount3";
            this.Amount3.ReadOnly = true;
            this.Amount3.Width = 81;
            // 
            // Amount4
            // 
            this.Amount4.DataPropertyName = "Amount4";
            this.Amount4.HeaderText = "本次结算金额";
            this.Amount4.MinimumWidth = 9;
            this.Amount4.Name = "Amount4";
            this.Amount4.Width = 105;
            // 
            // RemarkD
            // 
            this.RemarkD.DataPropertyName = "Remark";
            this.RemarkD.HeaderText = "备注";
            this.RemarkD.MinimumWidth = 9;
            this.RemarkD.Name = "RemarkD";
            this.RemarkD.Width = 57;
            // 
            // MakeUserID
            // 
            this.MakeUserID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.MakeUserID.FormattingEnabled = true;
            this.MakeUserID.Location = new System.Drawing.Point(320, 45);
            this.MakeUserID.Name = "MakeUserID";
            this.MakeUserID.Size = new System.Drawing.Size(130, 25);
            this.MakeUserID.TabIndex = 33;
            // 
            // Frmw_Receivable
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.Name = "Frmw_Receivable";
            this.Text = "Frmw_Receivable";
            this.palTools.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabDataEdit.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.flowLayoutPanelTools.ResumeLayout(false);
            this.tabControl2.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewDetail)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private TextBox ReceivableCode;
        private Label lblReceivableCode;
        private TextBox CustomerCode;
        private Label lblCustomerCode;
        private Label lblCustomerName;
        private DateTimePicker ReceivableDate;
        private Label lblReceivableDate;
        private Label lblMakeUserID;
        private TextBox SettlementCode;
        private Label lblSettlementCode;
        private Label lblSettlementName;
        private Label lblSettlementType;
        private TextBox TotalPrice;
        private Label lblTotalPrice;
        private TextBox Remark;
        private Label lblRemark;
        private ComboBox CustomerName;
        private ComboBox SettlementName;
        private ComboBox SettlementType;
        private TabControl tabControl2;
        private TabPage tabPage1;
        private DataGridView dataGridViewDetail;
        private DataGridViewComboBoxColumn SaleCode;
        private DataGridViewTextBoxColumn ReceivableCodeD;
        private DataGridViewTextBoxColumn ReceivableDetailCode;
        private DataGridViewTextBoxColumn Amount1;
        private DataGridViewTextBoxColumn Amount2;
        private DataGridViewTextBoxColumn Amount3;
        private DataGridViewTextBoxColumn Amount4;
        private DataGridViewTextBoxColumn RemarkD;
        private ComboBox MakeUserID;
    }
}