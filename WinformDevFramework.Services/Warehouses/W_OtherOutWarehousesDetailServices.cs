using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WinformDevFramework.IRepository.System;
using WinformDevFramework.IRepository.UnitOfWork;
using WinformDevFramework.IServices.System;
using WinformDevFramework.IServices;
using WinformDevFramework.IRepository;
using WinformDevFramework.Models;
namespace WinformDevFramework.Services
{
    public class w_OtherOutWarehouseDetailServices : BaseServices<w_OtherOutWarehouseDetail>, Iw_OtherOutWarehouseDetailServices
    {
        private readonly Iw_OtherOutWarehouseDetailRepository _dal;
        private readonly IUnitOfWork _unitOfWork;

        public w_OtherOutWarehouseDetailServices(IUnitOfWork unitOfWork, Iw_OtherOutWarehouseDetailRepository dal)
        {
            this._dal = dal;
            base.BaseDal = dal;
            _unitOfWork = unitOfWork;
        }
    }
}