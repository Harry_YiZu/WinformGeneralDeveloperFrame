using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WinformDevFramework.IRepository.System;
using WinformDevFramework.IRepository.UnitOfWork;
using WinformDevFramework.IServices.System;
using WinformDevFramework.IServices;
using WinformDevFramework.IRepository;
using WinformDevFramework.Models;
namespace WinformDevFramework.Services
{
    public class w_OtherInWarehouseDetailServices : BaseServices<w_OtherInWarehouseDetail>, Iw_OtherInWarehouseDetailServices
    {
        private readonly Iw_OtherInWarehouseDetailRepository _dal;
        private readonly IUnitOfWork _unitOfWork;

        public w_OtherInWarehouseDetailServices(IUnitOfWork unitOfWork, Iw_OtherInWarehouseDetailRepository dal)
        {
            this._dal = dal;
            base.BaseDal = dal;
            _unitOfWork = unitOfWork;
        }
    }
}