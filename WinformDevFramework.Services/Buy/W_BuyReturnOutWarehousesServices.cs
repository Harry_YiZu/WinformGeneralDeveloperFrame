using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WinformDevFramework.IRepository.System;
using WinformDevFramework.IRepository.UnitOfWork;
using WinformDevFramework.IServices.System;
using WinformDevFramework.IServices;
using WinformDevFramework.IRepository;
using WinformDevFramework.Models;
namespace WinformDevFramework.Services
{
    public class w_BuyReturnOutWarehouseServices : BaseServices<w_BuyReturnOutWarehouse>, Iw_BuyReturnOutWarehouseServices
    {
        private readonly Iw_BuyReturnOutWarehouseRepository _dal;
        private readonly IUnitOfWork _unitOfWork;

        public w_BuyReturnOutWarehouseServices(IUnitOfWork unitOfWork, Iw_BuyReturnOutWarehouseRepository dal)
        {
            this._dal = dal;
            base.BaseDal = dal;
            _unitOfWork = unitOfWork;
        }

        public int AddBuyOutWarehouseInfo(w_BuyReturnOutWarehouse buy, List<w_BuyReturnOutWarehouseDetail> detail)
        {
            return _dal.AddBuyOutWarehouseInfo(buy,detail);
        }

        public bool UpdateBuyOutWarehouseInfo(w_BuyReturnOutWarehouse buy, List<w_BuyReturnOutWarehouseDetail> detail)
        {
            return _dal.UpdateBuyOutWarehouseInfo(buy, detail);
        }

        public bool DeleteBuyOutWarehouseInfo(w_BuyReturnOutWarehouse buy)
        {
            return _dal.DeleteBuyOutWarehouseInfo(buy);
        }
    }
}