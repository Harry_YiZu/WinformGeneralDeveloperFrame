using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WinformDevFramework.IRepository.System;
using WinformDevFramework.IRepository.UnitOfWork;
using WinformDevFramework.IServices.System;
using WinformDevFramework.IServices;
using WinformDevFramework.IRepository;
using WinformDevFramework.Models;
namespace WinformDevFramework.Services
{
    public class w_BuyServices : BaseServices<w_Buy>, Iw_BuyServices
    {
        private readonly Iw_BuyRepository _dal;
        private readonly IUnitOfWork _unitOfWork;

        public w_BuyServices(IUnitOfWork unitOfWork, Iw_BuyRepository dal)
        {
            this._dal = dal;
            base.BaseDal = dal;
            _unitOfWork = unitOfWork;
        }

        public int AddBuyInfo(w_Buy buy, List<w_BuyDetail> detail)
        {
            return _dal.AddBuyInfo(buy, detail);
        }

        public bool UpdateBuyInfo(w_Buy buy, List<w_BuyDetail> detail)
        {
            return _dal.UpdateBuyInfo(buy, detail);
        }

        public bool DeleteBuyInfo(w_Buy buy)
        {
            return _dal.DeleteBuyInfo(buy);
        }
    }
}