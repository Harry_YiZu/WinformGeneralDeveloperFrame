using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WinformDevFramework.IRepository.System;
using WinformDevFramework.IRepository.UnitOfWork;
using WinformDevFramework.IServices.System;
using WinformDevFramework.IServices;
using WinformDevFramework.IRepository;
using WinformDevFramework.Models;
namespace WinformDevFramework.Services
{
    public class w_BuyReturnOutWarehouseDetailServices : BaseServices<w_BuyReturnOutWarehouseDetail>, Iw_BuyReturnOutWarehouseDetailServices
    {
        private readonly Iw_BuyReturnOutWarehouseDetailRepository _dal;
        private readonly IUnitOfWork _unitOfWork;

        public w_BuyReturnOutWarehouseDetailServices(IUnitOfWork unitOfWork, Iw_BuyReturnOutWarehouseDetailRepository dal)
        {
            this._dal = dal;
            base.BaseDal = dal;
            _unitOfWork = unitOfWork;
        }
    }
}