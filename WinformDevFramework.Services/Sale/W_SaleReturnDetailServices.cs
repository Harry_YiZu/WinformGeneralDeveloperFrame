using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WinformDevFramework.IRepository.System;
using WinformDevFramework.IRepository.UnitOfWork;
using WinformDevFramework.IServices.System;
using WinformDevFramework.IServices;
using WinformDevFramework.IRepository;
using WinformDevFramework.Models;
namespace WinformDevFramework.Services
{
    public class w_SaleReturnDetailServices : BaseServices<w_SaleReturnDetail>, Iw_SaleReturnDetailServices
    {
        private readonly Iw_SaleReturnDetailRepository _dal;
        private readonly IUnitOfWork _unitOfWork;

        public w_SaleReturnDetailServices(IUnitOfWork unitOfWork, Iw_SaleReturnDetailRepository dal)
        {
            this._dal = dal;
            base.BaseDal = dal;
            _unitOfWork = unitOfWork;
        }
    }
}