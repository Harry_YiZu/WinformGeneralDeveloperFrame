using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WinformDevFramework.IRepository.System;
using WinformDevFramework.IRepository.UnitOfWork;
using WinformDevFramework.IServices.System;
using WinformDevFramework.IServices;
using WinformDevFramework.IRepository;
using WinformDevFramework.Models;
namespace WinformDevFramework.Services
{
    public class w_SaleReturnInWarehouseServices : BaseServices<w_SaleReturnInWarehouse>, Iw_SaleReturnInWarehouseServices
    {
        private readonly Iw_SaleReturnInWarehouseRepository _dal;
        private readonly IUnitOfWork _unitOfWork;

        public w_SaleReturnInWarehouseServices(IUnitOfWork unitOfWork, Iw_SaleReturnInWarehouseRepository dal)
        {
            this._dal = dal;
            base.BaseDal = dal;
            _unitOfWork = unitOfWork;
        }

        public int AddSaleReturnInWarehouseInfo(w_SaleReturnInWarehouse buy, List<w_SaleReturnInWarehouseDetail> detail)
        {
            return _dal.AddSaleReturnInWarehouseInfo(buy, detail);
        }

        public bool UpdateSaleReturnInWarehouseInfo(w_SaleReturnInWarehouse buy, List<w_SaleReturnInWarehouseDetail> detail)
        {
            return _dal.UpdateSaleReturnInWarehouseInfo(buy, detail);
        }

        public bool DeleteSaleReturnInWarehouseInfo(w_SaleReturnInWarehouse buy)
        {
            return _dal.DeleteSaleReturnInWarehouseInfo(buy);
        }
    }
}