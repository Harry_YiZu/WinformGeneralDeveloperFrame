using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WinformDevFramework.IRepository.System;
using WinformDevFramework.IRepository.UnitOfWork;
using WinformDevFramework.IServices.System;
using WinformDevFramework.IServices;
using WinformDevFramework.IRepository;
using WinformDevFramework.Models;
namespace WinformDevFramework.Services
{
    public class w_SaleOutWarehouseDetailServices : BaseServices<w_SaleOutWarehouseDetail>, Iw_SaleOutWarehouseDetailServices
    {
        private readonly Iw_SaleOutWarehouseDetailRepository _dal;
        private readonly IUnitOfWork _unitOfWork;

        public w_SaleOutWarehouseDetailServices(IUnitOfWork unitOfWork, Iw_SaleOutWarehouseDetailRepository dal)
        {
            this._dal = dal;
            base.BaseDal = dal;
            _unitOfWork = unitOfWork;
        }
    }
}