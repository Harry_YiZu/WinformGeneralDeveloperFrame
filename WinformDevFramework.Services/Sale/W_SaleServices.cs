using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WinformDevFramework.IRepository.System;
using WinformDevFramework.IRepository.UnitOfWork;
using WinformDevFramework.IServices.System;
using WinformDevFramework.IServices;
using WinformDevFramework.IRepository;
using WinformDevFramework.Models;
namespace WinformDevFramework.Services
{
    public class w_SaleServices : BaseServices<w_Sale>, Iw_SaleServices
    {
        private readonly Iw_SaleRepository _dal;
        private readonly IUnitOfWork _unitOfWork;

        public w_SaleServices(IUnitOfWork unitOfWork, Iw_SaleRepository dal)
        {
            this._dal = dal;
            base.BaseDal = dal;
            _unitOfWork = unitOfWork;
        }

        public int AddSaleInfo(w_Sale buy, List<w_SaleDetail> detail)
        {
            return _dal.AddSaleInfo(buy,detail);
        }

        public bool UpdateSaleInfo(w_Sale buy, List<w_SaleDetail> detail)
        {
            return _dal.UpdateSaleInfo(buy, detail);
        }
    }
}