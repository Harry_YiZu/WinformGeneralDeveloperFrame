using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WinformDevFramework.IRepository.System;
using WinformDevFramework.IRepository.UnitOfWork;
using WinformDevFramework.IServices.System;
using WinformDevFramework.IServices;
using WinformDevFramework.IRepository;
using WinformDevFramework.Models;
namespace WinformDevFramework.Services
{
    public class w_ReceivableDetailServices : BaseServices<w_ReceivableDetail>, Iw_ReceivableDetailServices
    {
        private readonly Iw_ReceivableDetailRepository _dal;
        private readonly IUnitOfWork _unitOfWork;

        public w_ReceivableDetailServices(IUnitOfWork unitOfWork, Iw_ReceivableDetailRepository dal)
        {
            this._dal = dal;
            base.BaseDal = dal;
            _unitOfWork = unitOfWork;
        }
    }
}