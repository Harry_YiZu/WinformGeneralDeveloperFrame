﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinformDevFramework
{
    public class FlatInfo
    {
        public string Code { get; set; }
        public string Name { get; set; }
    }
}
