﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace WinformDevFramework
{
    ///<summary>
    ///结算账户
    ///</summary>
    [SugarTable("w_SettlementAccount")]
    public partial class w_SettlementAccount
    {
           public w_SettlementAccount(){


           }
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public int ID {get;set;}

           /// <summary>
           /// Desc:编号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Code {get;set;}

           /// <summary>
           /// Desc:名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Name {get;set;}

           /// <summary>
           /// Desc:期初金额
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? BeginAmount {get;set;}

           /// <summary>
           /// Desc:当前余额
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? NowAmount {get;set;}

           /// <summary>
           /// Desc:排序
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? SortOrder {get;set;}

           /// <summary>
           /// Desc:备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Remark {get;set;}

    }
}
