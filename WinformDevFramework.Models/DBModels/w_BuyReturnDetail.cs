﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace WinformDevFramework
{
    ///<summary>
    ///
    ///</summary>
    [SugarTable("w_BuyReturnDetail")]
    public partial class w_BuyReturnDetail
    {
           public w_BuyReturnDetail(){


           }
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public int ID {get;set;}

           /// <summary>
           /// Desc:采购退货单号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BuyReturnCode {get;set;}

           /// <summary>
           /// Desc:采购退货明细单号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BuyReturnDetailCode {get;set;}

           /// <summary>
           /// Desc:采购单号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BuyCode {get;set;}

           /// <summary>
           /// Desc:商品编号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GoodsCode {get;set;}

           /// <summary>
           /// Desc:商品名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GoodsName {get;set;}

           /// <summary>
           /// Desc:商品规格
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GoodsSpec {get;set;}

           /// <summary>
           /// Desc:计量单位
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GoodsUnit {get;set;}

           /// <summary>
           /// Desc:退货数量
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? ReturnNumber {get;set;}

           /// <summary>
           /// Desc:退货单价
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? UnitPrice {get;set;}

           /// <summary>
           /// Desc:折扣价格
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? DiscountPrice {get;set;}

           /// <summary>
           /// Desc:退货金额
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? TotalPrice {get;set;}

           /// <summary>
           /// Desc:税率
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? TaxRate {get;set;}

           /// <summary>
           /// Desc:含税单价
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? TaxUnitPrice {get;set;}

           /// <summary>
           /// Desc:税额
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? TaxPrice {get;set;}

           /// <summary>
           /// Desc:含税金额
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? TotalTaxPrice {get;set;}

           /// <summary>
           /// Desc:备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Remark {get;set;}

    }
}
