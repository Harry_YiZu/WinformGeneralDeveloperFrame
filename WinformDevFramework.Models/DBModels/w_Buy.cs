﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace WinformDevFramework
{
    ///<summary>
    ///
    ///</summary>
    [SugarTable("w_Buy")]
    public partial class w_Buy
    {
           public w_Buy(){


           }
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public int ID {get;set;}

           /// <summary>
           /// Desc:采购单号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BuyCode {get;set;}

           /// <summary>
           /// Desc:供应商名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SupplierName {get;set;}

           /// <summary>
           /// Desc:单据日期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? InvoicesDate {get;set;}

           /// <summary>
           /// Desc:制单人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string MakeUserName {get;set;}

           /// <summary>
           /// Desc:交货日期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? DeliveryDate {get;set;}

           /// <summary>
           /// Desc:单据
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string InvoicesImage {get;set;}

           /// <summary>
           /// Desc:备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Remark {get;set;}

           /// <summary>
           /// Desc:结算账户
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SettlementAccount {get;set;}

           /// <summary>
           /// Desc:单据状态
           /// Default:0
           /// Nullable:True
           /// </summary>           
           public string Status {get;set;}

           /// <summary>
           /// Desc:供应商编码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SupplierCode {get;set;}

           /// <summary>
           /// Desc:总金额
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? TotalPrice {get;set;}

           /// <summary>
           /// Desc:已结金额
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? TotalPrice1 {get;set;}

           /// <summary>
           /// Desc:未结金额
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? TotalPrice2 {get;set;}

    }
}
