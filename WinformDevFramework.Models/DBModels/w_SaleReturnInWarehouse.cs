﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace WinformDevFramework
{
    ///<summary>
    ///
    ///</summary>
    [SugarTable("w_SaleReturnInWarehouse")]
    public partial class w_SaleReturnInWarehouse
    {
           public w_SaleReturnInWarehouse(){


           }
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public int ID {get;set;}

           /// <summary>
           /// Desc:销售退货单号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SaleReturnCode {get;set;}

           /// <summary>
           /// Desc:销售单号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SaleCode {get;set;}

           /// <summary>
           /// Desc:客户编码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CustomerCode {get;set;}

           /// <summary>
           /// Desc:客户名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CustomerName {get;set;}

           /// <summary>
           /// Desc:入库单号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SaleReturnInWarehouseCode {get;set;}

           /// <summary>
           /// Desc:单据日期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? InvicesDate {get;set;}

           /// <summary>
           /// Desc:单据附件
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string InvicesFile {get;set;}

           /// <summary>
           /// Desc:制单人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? MakeUserID {get;set;}

           /// <summary>
           /// Desc:审核人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? ReviewUserID {get;set;}

           /// <summary>
           /// Desc:单据状态
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Status {get;set;}

           /// <summary>
           /// Desc:备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Remark {get;set;}

    }
}
