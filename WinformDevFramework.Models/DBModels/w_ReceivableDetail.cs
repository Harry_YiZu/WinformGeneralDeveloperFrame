﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace WinformDevFramework
{
    ///<summary>
    ///
    ///</summary>
    [SugarTable("w_ReceivableDetail")]
    public partial class w_ReceivableDetail
    {
           public w_ReceivableDetail(){


           }
           /// <summary>
           /// Desc:收款单号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ReceivableCode {get;set;}

           /// <summary>
           /// Desc:收款明细单号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string ReceivableDetailCode {get;set;}

           /// <summary>
           /// Desc:销售单号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SaleCode {get;set;}

           /// <summary>
           /// Desc:单据金额
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? Amount1 {get;set;}

           /// <summary>
           /// Desc:已结金额
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? Amount2 {get;set;}

           /// <summary>
           /// Desc:未结金额
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? Amount3 {get;set;}

           /// <summary>
           /// Desc:本次结算金额
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? Amount4 {get;set;}

           /// <summary>
           /// Desc:备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Remark {get;set;}

    }
}
