﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace WinformDevFramework
{
    ///<summary>
    ///
    ///</summary>
    [SugarTable("w_PaymentDetail")]
    public partial class w_PaymentDetail
    {
           public w_PaymentDetail(){


           }
           /// <summary>
           /// Desc:付款单号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PayCode {get;set;}

           /// <summary>
           /// Desc:付款明细单号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PayDetailCode {get;set;}

           /// <summary>
           /// Desc:采购单号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BuyCode {get;set;}

           /// <summary>
           /// Desc:单据金额
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? Amount1 {get;set;}

           /// <summary>
           /// Desc:已结金额
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? Amount2 {get;set;}

           /// <summary>
           /// Desc:未结金额
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? Amount3 {get;set;}

           /// <summary>
           /// Desc:本次结算金额
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? Amount4 {get;set;}

           /// <summary>
           /// Desc:备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Remark {get;set;}

    }
}
