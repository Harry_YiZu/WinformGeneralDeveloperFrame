﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace WinformDevFramework
{
    ///<summary>
    ///
    ///</summary>
    [SugarTable("w_BuyDetail")]
    public partial class w_BuyDetail
    {
           public w_BuyDetail(){


           }
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public int ID {get;set;}

           /// <summary>
           /// Desc:采购单ID
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? BuyID {get;set;}

           /// <summary>
           /// Desc:采购单号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BuyCode {get;set;}

           /// <summary>
           /// Desc:采购明细单号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BuyDetailCode {get;set;}

           /// <summary>
           /// Desc:商品名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GoodsName {get;set;}

           /// <summary>
           /// Desc:商品编码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GoodsCode {get;set;}
           /// <summary>
           /// Desc:商品规格
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GoodsSpec { get; set; }
           /// <summary>
           /// Desc:商品单位
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GoodsUnit { get; set; }

        /// <summary>
        /// Desc:数量
        /// Default:
        /// Nullable:True
        /// </summary>           
        public decimal? BuyNumber {get;set;}

           /// <summary>
           /// Desc:购货单价
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? UnitPrice {get;set;}

           /// <summary>
           /// Desc:折扣价格
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? DiscountPrice {get;set;}

           /// <summary>
           /// Desc:金额
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? TotalPrice {get;set;}

           /// <summary>
           /// Desc:税率
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? TaxRate {get;set;}

           /// <summary>
           /// Desc:含税单价
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? TaxUnitPrice {get;set;}

           /// <summary>
           /// Desc:税额
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? TaxPrice {get;set;}

           /// <summary>
           /// Desc:含税金额
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? TotalTaxPrice {get;set;}

           /// <summary>
           /// Desc:beizhu
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Remark {get;set;}

           /// <summary>
           /// Desc:仓库
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? Warehouse {get;set;}

    }
}
