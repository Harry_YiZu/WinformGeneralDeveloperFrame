﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace WinformDevFramework
{
    ///<summary>
    ///
    ///</summary>
    [SugarTable("w_OtherInWarehouse")]
    public partial class w_OtherInWarehouse
    {
           public w_OtherInWarehouse(){


           }
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public int ID {get;set;}

           /// <summary>
           /// Desc:业务类别
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TypeCode {get;set;}

           /// <summary>
           /// Desc:单据日期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? InvoicesDate {get;set;}

           /// <summary>
           /// Desc:入库单号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string OtherInWarehouseCode {get;set;}

           /// <summary>
           /// Desc:单据附件
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string InvoicesImage {get;set;}

           /// <summary>
           /// Desc:备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Remark {get;set;}

           /// <summary>
           /// Desc:单据状态
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Status {get;set;}

           /// <summary>
           /// Desc:制单人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? MakeUserID {get;set;}

    }
}
