﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace WinformDevFramework
{
    ///<summary>
    ///
    ///</summary>
    [SugarTable("w_Payment")]
    public partial class w_Payment
    {
           public w_Payment(){


           }
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public int ID {get;set;}

           /// <summary>
           /// Desc:付款单号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PayCode {get;set;}

           /// <summary>
           /// Desc:供应商编码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SupplierCode {get;set;}

           /// <summary>
           /// Desc:供应商名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SupplierName {get;set;}

           /// <summary>
           /// Desc:付款时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? PayDate {get;set;}

           /// <summary>
           /// Desc:制单人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? MakeUserID {get;set;}

           /// <summary>
           /// Desc:结算方式
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SettlementType {get;set;}

           /// <summary>
           /// Desc:结算账户
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SettlementCode {get;set;}

           /// <summary>
           /// Desc:结算账户名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SettlementName {get;set;}

           /// <summary>
           /// Desc:付款金额
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? TotalPrice {get;set;}

           /// <summary>
           /// Desc:备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Remark {get;set;}

    }
}
