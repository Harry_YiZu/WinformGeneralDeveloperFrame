﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace WinformDevFramework
{
    ///<summary>
    ///
    ///</summary>
    [SugarTable("w_SaleDetail")]
    public partial class w_SaleDetail
    {
           public w_SaleDetail(){


           }
           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public int ID {get;set;}

           /// <summary>
           /// Desc:主表ID
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? SaleID {get;set;}

           /// <summary>
           /// Desc:主表编号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SaleCode { get;set;}

           /// <summary>
           /// Desc:明细单号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string SaleDetailCode { get;set;}

           /// <summary>
           /// Desc:商品编码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GoodsCode {get;set;}

           /// <summary>
           /// Desc:商品名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GoodsName {get;set;}

           /// <summary>
           /// Desc:商品规格
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GoodsSpec {get;set;}

           /// <summary>
           /// Desc:计量单位
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string GoodsUnit {get;set;}

           /// <summary>
           /// Desc:数量
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? Number {get;set;}

           /// <summary>
           /// Desc:单价
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? UnitPrice {get;set;}

           /// <summary>
           /// Desc:金额
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? TotalPrice {get;set;}

           /// <summary>
           /// Desc:税率
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? TaxRate {get;set;}

           /// <summary>
           /// Desc:折扣金额
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? DiscountPrice {get;set;}

           /// <summary>
           /// Desc:含税单价
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? TaxUnitPrice {get;set;}

           /// <summary>
           /// Desc:税额
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? TaxPrice {get;set;}

           /// <summary>
           /// Desc:含税总价
           /// Default:
           /// Nullable:True
           /// </summary>           
           public decimal? TotalTaxPrice {get;set;}

           /// <summary>
           /// Desc:备注
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Remark {get;set;}

    }
}
