using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WinformDevFramework.IRepository.System;
using WinformDevFramework.IRepository;
using WinformDevFramework.Models;

namespace WinformDevFramework.Repository
{
    public class w_BuyInWarehouseDetailRepository : BaseRepository<w_BuyInWarehouseDetail>, Iw_BuyInWarehouseDetailRepository
    {
        public w_BuyInWarehouseDetailRepository(ISqlSugarClient sqlSugar) : base(sqlSugar)
        {

        }
    }
}