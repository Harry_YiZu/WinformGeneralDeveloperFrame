using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WinformDevFramework.IRepository.System;
using WinformDevFramework.IRepository;
using WinformDevFramework.Models;

namespace WinformDevFramework.Repository
{
    public class w_BuyDetailRepository : BaseRepository<w_BuyDetail>, Iw_BuyDetailRepository
    {
        public w_BuyDetailRepository(ISqlSugarClient sqlSugar) : base(sqlSugar)
        {

        }
    }
}