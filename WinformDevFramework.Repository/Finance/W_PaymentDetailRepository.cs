using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WinformDevFramework.IRepository.System;
using WinformDevFramework.IRepository;
using WinformDevFramework.Models;

namespace WinformDevFramework.Repository
{
    public class w_PaymentDetailRepository : BaseRepository<w_PaymentDetail>, Iw_PaymentDetailRepository
    {
        public w_PaymentDetailRepository(ISqlSugarClient sqlSugar) : base(sqlSugar)
        {

        }
    }
}