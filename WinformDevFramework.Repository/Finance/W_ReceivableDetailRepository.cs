using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WinformDevFramework.IRepository.System;
using WinformDevFramework.IRepository;
using WinformDevFramework.Models;

namespace WinformDevFramework.Repository
{
    public class w_ReceivableDetailRepository : BaseRepository<w_ReceivableDetail>, Iw_ReceivableDetailRepository
    {
        public w_ReceivableDetailRepository(ISqlSugarClient sqlSugar) : base(sqlSugar)
        {

        }
    }
}