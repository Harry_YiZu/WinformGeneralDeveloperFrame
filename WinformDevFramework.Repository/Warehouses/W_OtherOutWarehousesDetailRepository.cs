using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WinformDevFramework.IRepository.System;
using WinformDevFramework.IRepository;
using WinformDevFramework.Models;

namespace WinformDevFramework.Repository
{
    public class w_OtherOutWarehouseDetailRepository : BaseRepository<w_OtherOutWarehouseDetail>, Iw_OtherOutWarehouseDetailRepository
    {
        public w_OtherOutWarehouseDetailRepository(ISqlSugarClient sqlSugar) : base(sqlSugar)
        {

        }
    }
}