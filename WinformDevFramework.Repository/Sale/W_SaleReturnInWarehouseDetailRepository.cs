using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WinformDevFramework.IRepository.System;
using WinformDevFramework.IRepository;
using WinformDevFramework.Models;

namespace WinformDevFramework.Repository
{
    public class w_SaleReturnInWarehouseDetailRepository : BaseRepository<w_SaleReturnInWarehouseDetail>, Iw_SaleReturnInWarehouseDetailRepository
    {
        public w_SaleReturnInWarehouseDetailRepository(ISqlSugarClient sqlSugar) : base(sqlSugar)
        {

        }
    }
}