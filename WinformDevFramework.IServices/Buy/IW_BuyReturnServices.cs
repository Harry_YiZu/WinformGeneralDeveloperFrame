using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WinformDevFramework.Models;


namespace WinformDevFramework.IServices
{
    public interface Iw_BuyReturnServices: IBaseServices<w_BuyReturn>
    {
        int AddBuyReturnInfo(w_BuyReturn buy, List<w_BuyReturnDetail> detail);
        bool UpdateBuyReturnInfo(w_BuyReturn buy, List<w_BuyReturnDetail> detail);
    }
}